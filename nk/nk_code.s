;--
; Source code for code segment assembly.
;--

; General-purpose bitmasks.
c_nk_bitmask_tab:
    .BYTE   $01, $02, $04, $08, $10, $20, $40, $80

; Indices for bank switching.
; See: https://wiki.nesdev.com/w/index.php/Programming_UNROM
.IF __MAPPER__ == 2 && __NUM_BANKS__ > 1
.ASSERT __BANK__ == __NUM_BANKS__-1
c_nk_bank_tab:
    .BYTE   0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
.ENDIF

;--
; Initializes nk subsystems.
;--
_nk_init:
    ; Include initialization code of modules.
    .IFDEF NK_USE_W
        _NK_W_INIT
    .ENDIF
    .IFDEF NK_USE_JOYPAD
        _NK_JOYPAD_INIT
    .ENDIF
    .IFDEF NK_USE_RANDOM
        _NK_RANDOM_INIT
    .ENDIF
    .IFDEF NK_USE_PATT
        _NK_PATT_INIT
    .ENDIF
    .IFDEF NK_USE_SKIN
        _NK_SKIN_INIT
    .ENDIF
    .IFDEF NK_USE_YALZ
        _NK_YALZ_INIT
    .ENDIF
    .IFDEF NK_USE_AUDIO
        _NK_AUDIO_INIT
    .ENDIF
    .IFDEF NK_USE_BCD
        _NK_BCD_INIT
    .ENDIF

    ; Wait at least one VBLANK for PPU to initialize.
    BIT     $2002
@wait_vblank1:
    BIT     $2002
    BPL     @wait_vblank1
@wait_vblank2:
    BIT     $2002
    BPL     @wait_vblank2

    ; Detect NTSC or PAL. Code by Blargg.
    LDX     #52
    LDY     #24
@detect_ntsc_loop:
    DEX
    BNE     @detect_ntsc_loop
    DEY
    BNE     @detect_ntsc_loop
    LDA     $2002
    AND     #$80
    STA     v_nk_ntsc_flags

    JSR     RESET

    NK_PPU_ENABLE
    NK_NMI_ENABLE

_nk_mainloop:
    LDA     v_nk_ppu_flags      ; Wait for VBLANK.
    BPL     _nk_mainloop
    AND     #%01111111
    STA     v_nk_ppu_flags      ; Clear the flag.

    JSR     UPDATE

    JMP     _nk_mainloop

_nk_nmi:
    ; Push A, X and Y. (13 cycles)
    PHA                         ; 3
    TXA                         ; 2
    PHA                         ; 3
    TYA                         ; 2
    PHA                         ; 3

    ; Check for nk NMI routine skip, aka "raw mode". (6 cycles)
    BIT     z_nk_nmi_flags      ; 3
    BMI     @skip_all           ; 3 if skipping, 2 otherwise (normal behavior)

    ; Skip every 6th frame on NTSC system.
.IF NK_NTSC_FRAMESKIP
    LDA     v_nk_ntsc_flags
    BPL     @no_ntsc_frameskip  ; We have PAL if bit 7 is clear.
    INC     v_nk_ntsc_flags
    CMP     #%10000000 + 5      ; Compares A -- not incremented flags.
    BNE     @no_ntsc_frameskip
    LDA     #%10000000
    STA     v_nk_ntsc_flags
    BMI     @end                ; Skip all, JMP optimized to BMI.
@no_ntsc_frameskip:
.ENDIF

    ; Poll PPU status register to prevent writing $2000 to cause extra NMIs.
    LDA     $2002

    LDA     v_nk_ppu_flags
    AND     #%01000000
    BEQ     @skip_ppu_set
;@ppu_set:
    LDA     v_nk_ppu_mask       ; Set PPU flags (most likely enable).
    STA     $2001
@skip_ppu_set:
.IF !NK_IRQ_UPDATE
    LDA     #%10000000          ; Indicate NMI, clear other flags.
    STA     v_nk_ppu_flags
.ENDIF

    ; DMA transfer of PPU sprites (256 bytes)
    ; TODO: does it need configuration?
.IF 0
    LDA     v_nk_fc_lo          ; Flicker somewhat.
    ASL     A
    ASL     A
    AND     #$04
.ELSE
    LDA     #0                  ; No flicker.
.ENDIF
    STA     $2003
    LDA     #>(NK_SPR_BASE)
    STA     $4014               ; Begin transfer (512 cycles)

    ; Copy palettes to PPU.
    LDA     #$3F                ; Set PPU address to palettes.
    STA     $2006
    LDX     #$00
    STX     $2006
    LDY     v_nk_pal_zero       ; Copy color zero.
    STY     $2007
@pal_copy_loop:
    LDA     v_nk_pal+1, X       ; Unrolled.
    STA     $2007
    LDA     v_nk_pal+2, X
    STA     $2007
    LDA     v_nk_pal+3, X
    STA     $2007
    ; Skip every 4th entry. DMC conflicts $2007 reads so we write instead.
    STY     $2007
    INX
    INX
    INX
    CPX     #3*4*2
    BNE     @pal_copy_loop

@skip_all:
    JSR     VBLANK              ; 6

    ; Increment 16-bit frame counter.
    INC     v_nk_fc_lo
    BNE     @skip_fc_hi
    INC     v_nk_fc_hi
@skip_fc_hi:

    LDA     $2006               ; Reset latch.
    LDA     #0                  ; Reset PPU address.
    STA     $2006
    STA     $2006
    LDA     v_nk_ppu_scroll_x
    STA     $2005               ; 1st write: X scroll reload.
    LDA     v_nk_ppu_scroll_y
    STA     $2005               ; 2nd write: Y scroll reload.
    ; TODO: make sure $2000 is not set in many places!
    LDA     v_nk_ppu_ctrl       ; Combine with PPU CTRL settings.
    STA     $2000

@end:
    PLA                         ; Pop A, X and Y.
    TAY
    PLA
    TAX
    PLA
    RTI

.IF (__MAPPER__ == 4 || __MAPPER__ == 119)
_nk_irq:
    NK_IRQ_DISABLE              ; Acknowledge interrupt.

    PHA                         ; Push A, X and Y.
    TXA
    PHA
    TYA
    PHA

    JSR     IRQ

.IF NK_IRQ_UPDATE
    LDA     #%10000000          ; Indicate NMI, clear other flags.
    STA     v_nk_ppu_flags
.ENDIF

    PLA                         ; Pop A, X and Y.
    TAY
    PLA
    TAX
    PLA
    RTI
.ENDIF

;--
; See macro.
;--
_nk_ppu_scroll_set_y:
    INX
    LDY     #0
@repeat_c0:
    SEC
@repeat_c1:
    SBC     #240
    INY
    BCS     @repeat_c1
    DEX
    BNE     @repeat_c0
    ;CLC
    ADC     #240
    STA     v_nk_ppu_scroll_y

    TYA
    LSR
    LDA     v_nk_ppu_ctrl
    BCC     @odd
;@even:
    AND     #%11111101
    STA     v_nk_ppu_ctrl
    RTS

@odd:
    ORA     #%00000010
    STA     v_nk_ppu_ctrl
    RTS

;--
; Transfer data to PPU.
; NOTE: Use NK_PPU_ORG to set destination address in PPU memory.
; Inputs:
;   t0: 0
;   Y: Source address, low byte
;   t1: Source address, high byte
;   t2: Number of bytes to copy, low byte.
;   t3: Number of bytes to copy, high byte.
; Modifies:
;   A,X,Y,t1,t2,t3
;--
nk_ppu_copy:
    INC     t3                  ; t3 += 1 for proper copy
    LDX     t2
    JMP     @enter_copy_loop

@copy_loop:
    NK_PTR_READ
    CLC
    ADC     z_nk_decode_incr    ; add increment to result
    STA     $2007
    DEX
@enter_copy_loop:
    BNE     @copy_loop
    DEC     t3
    BNE     @copy_loop
    RTS

;--
; Draw pstr to PPU.
; NOTE: Y (offset) will wrap at 256.
; Inputs:
;   (t0,t1): Pointer to pstr buffer.
;   Y: Offset in the pstr buffer.
; Outputs:
;   Y: New offset to byte after the drawn string.
; Modifies:
;   A,X,Y
;--
nk_ppu_put_pstr:
    LDA     (t0), Y
    INY
    TAX
    BEQ     @end
@pstr_loop:
    LDA     (t0), Y
    STA     $2007
    INY
    DEX
    BNE     @pstr_loop
@end:
    RTS

;--
; Gets a byte from pointer in t0,t1 and increments the pointer.
; WARNING: Assumes Y=0! Set Y=0 before use!
; Inputs:
;   t0: Source address, low byte
;   t1: Source address, high byte
; Returns:
;   A: Fetched byte
; Modifies:
;   A,t0,t1
;--
nk_getbyte:
    LDA     (t0), Y
    INC     t0
    BNE     @no_hi_incr
    INC     t1
@no_hi_incr:
    RTS

; Include code from modules.
.IFDEF NK_USE_W
    _NK_W_CODE
.ENDIF
.IFDEF NK_USE_JOYPAD
    _NK_JOYPAD_CODE
.ENDIF
.IFDEF NK_USE_RANDOM
    _NK_RANDOM_CODE
.ENDIF
.IFDEF NK_USE_PATT
    _NK_PATT_CODE
.ENDIF
.IFDEF NK_USE_SKIN
    _NK_SKIN_CODE
.ENDIF
.IFDEF NK_USE_YALZ
    _NK_YALZ_CODE
.ENDIF
.IFDEF NK_USE_AUDIO
    _NK_AUDIO_CODE
.ENDIF
.IFDEF NK_USE_BCD
    _NK_BCD_CODE
.ENDIF

_nk_code_end:

;--
; Reset IRQ, APU, PPU, zero WRAM and map two last PRG-ROM banks to $C000-$FFFF.
;
; Due to iNES mapper #64 fixed region being $E000-$FFFF vs. the usual
; $C000-$FFFF this reset is at the top of "CODE" segment, just before
; the interrupt vectors. To ensure interwork with other mappers, the default
; PRG-ROM configuration is set here to map the entire $8000-$FFFF to the two
; last 16 KB banks of the cartridge.
;
; The WRAM at $0000-$07FF is zeroed except for NK_SPR_BASE page where sprite
; Y coordinates are set $FE to hide them. The clear takes couple frames.
;--
.ORG    $FFBD                   ; !NOTE! Change to align _nk_reset with interrupt vectors below.
_nk_reset:
    SEI                         ; Disable IRQs.
    CLD                         ; Disable decimal mode.
    LDX     #$40
    STX     $4017               ; Disable APU frame IRQ.
    LDX     #$FF
    TXS                         ; Set stack pointer.
    INX                         ; X=0
    STX     $2000               ; Disable NMI etc.
    STX     $2001               ; Disable video.
    STX     $4010               ; Disable DMC IRQ.

    TXA                         ; A=X=0
@clear_loop:
    STA     $00, X              ; Clear zero page.
    STA     $0100, X            ; Clear stack
    STA     $0200, X            ; BSS
    STA     $0300, X
    STA     $0400, X
    STA     $0500, X
    STA     $0600, X
    STA     $0700, X
    INX
    BNE     @clear_loop         ; X=0 at end

    ; TODO: refactor, also used by skin routines
    LDA     #$FE                ; Hide sprites.
@reset_spr_ram:
    STA     NK_SPR_BASE, X
    INX
    INX
    INX
    INX
    BNE     @reset_spr_ram

    JMP     _nk_init

; Define interrupt vectors.
.ASSERT * == $FFFA, "_nk_reset is not aligned with interrupt vectors"
    .WORD   _nk_nmi
    .WORD   _nk_reset
.IF (__MAPPER__ == 4 || __MAPPER__ == 119)
    .WORD   _nk_irq
.ELSE
    .WORD   0
.ENDIF

.ORG    _nk_code_end            ; Restore bank ORG to one before the vectors.
