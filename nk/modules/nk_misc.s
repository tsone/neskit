
;--
; Convert byte (8 bits) to decimal.
; Inputs:
;  A: Byte to convert.
; Returns:
;  A: Decimal number 1s.
;  X: Decimal number 10s.
;  Y: Decimal number 100s.
;--
nk_byte2dec:
; Based on following algorithm (reportedly from comp.sys.cbm):
; http://codebase64.org/doku.php?id=base:tiny_.a_to_ascii_routine
    LDY     #255    ; 255=0-1
    LDX     #10
    SEC
@hundreds:
    INY
    SBC     #100
    BCS     @hundreds
@tens:
    DEX
    ADC     #10
    BMI     @tens
    ADC     #255    ; 255=0-1
    RTS


;--
; Convert word (16 bits) to decimal.
; Inputs:
;  X: Word low byte.
;  Y: Word high byte.
; Outputs:
;  t4,t3,t2,t1,t0: Ones, tens, hundreds, etc. in this order.
; Modifies:
;   C,A,X,Y,t0,t1,t2,t3,t4
nk_word2dec:
    LDA     #255    ; 255=0-1
    STA     t0
    STA     t2
    LDA     #10
    STA     t1
    STA     t3
    
    SEC    
@ten_thousands:
    INC     t0
    TXA
    SBC     #<10000
    TAX
    TYA
    SBC     #>10000
    TAY
    BCS     @ten_thousands
    
@thousands:
    DEC     t1
    TXA
    ADC     #<1000
    TAX
    TYA
    ADC     #>1000
    TAY
    BCC     @thousands
    
@hundreds:
    INC     t2
    TXA
    SBC     #100
    TAX
    TYA
    SBC     #0          ; TODO: optimize
    TAY
    BCS     @hundreds

    TXA
@tens:
    DEC     t3
    ADC     #10
    BCC     @tens

    STA     t4
    RTS
