
NK_USE_RANDOM = 1
; Other 16-bit linear feedback shift register (LFSR) magic numbers:
; http://codebase64.org/doku.php?id=base:small_fast_16-bit_prng
NK_RANDOM_MAGIC = $8117


;--
; Set seed for PRNG.
; See: nk_random
; Inputs:
;   A: Seed (high byte).
;   X: Seed (low byte).
; Modifies:
;   -
;--
.MACRO NK_RANDOM_SEED
    STA     v_nk_random_seed
    STX     v_nk_random_seed+1
.ENDMACRO

.MACRO NK_RANDOM
    JSR     nk_random
.ENDMACRO

;--
; Randomly swap a byte in an array in RAM.
; NOTE: The array size must be a power of two.
; Input:
;   addr_: WRAM address of the byte array.
;   size_: Power of two array size: 2,4,8,16,32,64,128,256
;   X: Source index for the swap.
; Modifies:
;   C,A,Y
;--
.MACRO NK_RANDOM_SWAP addr_, size_
    NK_RANDOM
.IF (size_) < 256
    AND     #(size_) - 1
.ENDIF
    TAY
    
    LDA     addr_, X            ; Swap bytes.
    PHA
    LDA     addr_, Y
    STA     addr_, X
    PLA
    STA     addr_, Y
.ENDMACRO

;--
; Shuffle an array of bytes in RAM.
; NOTE: The array size must be a power of two.
; Th algorithm is a variant of Knuth shuffle where the swap index is
; selected from the range 0..N-1. Original shuffle uses range 0..i
; because this guarantees no bias. This bias is usually not a problem.
; See also: http://en.wikipedia.org/wiki/Knuth_shuffle
; Inputs:
;   addr_: WRAM address to the bytes to shuffle.
;   size_: Power of two number of bytes to shuffle: 2,4,8,16,32,64,128,256
;--
.MACRO NK_RANDOM_SHUFFLE addr_, size_
    LDX     #<(size_)
@shuffle_loop\@:
    DEX
    NK_RANDOM_SWAP addr_, size_
    TXA
    BNE     @shuffle_loop\@
.ENDMACRO


.MACRO _NK_RANDOM_ZEROPAGE
.ENDMACRO


.MACRO _NK_RANDOM_BSS
v_nk_random_seed:        .RES 2
.ENDMACRO


.MACRO _NK_RANDOM_CODE

;--
; Get a pseudo-random byte from PRNG.
; This uses 16-bit linear feedback shift register (LFSR) algorithm.
; The pseudo-random sequence is defined by magic in NK_RANDOM_MAGIC.
; This version is based on source code by David Holz (aka White Flame):
; http://codebase64.org/doku.php?id=base:small_fast_16-bit_prng
; Inputs:
;   -
; Modifies:
;   C,A
; Output:
;   A: Pseudo-random byte.
;--
nk_random:
    LDA     v_nk_random_seed
    BEQ     @low_zero
    
    ASL     v_nk_random_seed
    LDA     v_nk_random_seed+1
    ROL     A
    BCC     @no_eor
@do_eor:
    EOR     #>NK_RANDOM_MAGIC
    STA     v_nk_random_seed+1
    LDA     v_nk_random_seed
    EOR     #<NK_RANDOM_MAGIC
    STA     v_nk_random_seed

;@combine_eor:
    EOR     v_nk_random_seed+1      ; Combine 16-bit seed to byte.
    RTS
    
@low_zero:
    LDA     v_nk_random_seed+1
    BEQ     @do_eor
    
    ASL     A
    BEQ     @no_eor
    BCS     @do_eor

@no_eor:
    STA     v_nk_random_seed+1
    
;@combine_no_eor:
    EOR     v_nk_random_seed        ; Combine 16-bit seed to byte.
    RTS

.ENDMACRO


.MACRO _NK_RANDOM_INIT
    ; By default initialize PRNG to this fixed seed.
    LDA     #$AC
    LDX     #$E1
    NK_RANDOM_SEED
.ENDMACRO
