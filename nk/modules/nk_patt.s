
NK_USE_PATT = 1
.IFNDEF NK_USE_YALZ
    .INCLUDE "modules/nk_yalz.s"
.ENDIF
.IFNDEF NK_USE_W
    .INCLUDE "modules/nk_w.s"
.ENDIF

NK_PATT_NOT_LOADED = $FF
NK_PATT_SPR_VRAM = $80


;--
; Get tile offset of loaded patt.
; Inputs:
;   patt_idx_: Patt idx.
; Outputs:
;   A: Tile offset of loaded patt or $FF if not loaded.
;--
.MACRO NK_PATT_GET_LOAD_OFFS patt_idx_
    LDA     v_nk_patt_load_offs+(patt_idx_)
.ENDMACRO


;--
; See: nk_patt_name_decode
;--
.MACRO NK_PATT_NAME_DECODE ppu_dest_, yalz_src_, patt_idx_
    LDX     #patt_idx_
    NK_PTR_SET  yalz_src_
    NK_PPU_ORG  ppu_dest_
    JSR     nk_patt_name_decode
.ENDMACRO


.MACRO _NK_PATT_ZEROPAGE
.ENDMACRO ; _NK_PATT_ZEROPAGE


.MACRO _NK_PATT_BSS
; Top tile index at VRAM: 0=BG, 1=sprite.
v_nk_patt_top:             .RES 2
; The tile offs of a loaded patt (for each patt). If not loaded, NK_PATT_NOT_LOADED.
v_nk_patt_load_offs:       .RES GEN_PATT_NUM
.ENDMACRO ; _NK_PATT_BSS


.MACRO _NK_PATT_CODE
;--
; Clear loaded patt. Reset top for both BG and sprites and clear loaded status.
; Modifies:
;   A,X
;--
nk_patt_clear:
    LDA     #0
    STA     v_nk_patt_top
    STA     v_nk_patt_top+1

    LDA     #NK_PATT_NOT_LOADED
    LDX     #GEN_PATT_NUM-1
@loop:
    STA     v_nk_patt_load_offs, X
    DEX
    BPL     @loop
    RTS

;--
; Loads a patt to VRAM and sets tile offset.
; NOTE: Patt is loaded if not loaded yet.
; Inputs:
;   A: Patt idx to load. Set bit 7 if loading to sprite VRAM.
; Outputs:
;   C: 1=Patt was/is loaded, 0=not loaded (not enough VRAM).
; Modifies:
;   A,X,Y,t0,t1,t2,t3,t4
;--
nk_patt_load:
    STA     t4
    AND     #$7F
    TAX
    LDA     #NK_PATT_NOT_LOADED
    EOR     v_nk_patt_load_offs, X
    BEQ     @start_loading
;@already_loaded:
    CLC
    RTS

@start_loading:
.IF __MAPPER__ == 2 && __NUM_BANKS__ > 1
    LDY     c_gen_patt_bank_tab, X
    NK_BANK_SWITCH_NO_SAVE
.ENDIF

    LDY     #0
    STY     t0
    BIT     t4
    BPL     @bg_ppu_ptr
;@spr_ppu_ptr:
    INY
@bg_ppu_ptr:
    STY     t3
    LDA     v_nk_patt_top, Y
    STA     t2

    ; Load patt ptr -> (Y,t1)
    LDY     c_gen_patt_ptr_hi, X
    STY     t1
    LDY     c_gen_patt_ptr_lo, X
    SEC                             ; C=1 for +1, A has patt size from nk_getbyte.
    ADC     (t0), Y
    ; Check for overflow (not enough VRAM).
    BCC     @continue
    BEQ     @continue_zero          ; Special case: 0 after overflow is OK.
;@load_failed:
@exit:
.IF __MAPPER__ == 2 && __NUM_BANKS__ > 1
    NK_BANK_RESTORE
.ENDIF
    RTS

@continue_zero:
    LDA     #$FF
@continue:
    NK_PTR_INC
    STY     t4

    LDY     t3
    STA     v_nk_patt_top, Y
    LDA     t2
    STA     v_nk_patt_load_offs, X

    ; Multiply x16 (=left-shift by 4).
    ASL
    ROL     t3
    ASL
    ROL     t3
    ASL
    ROL     t3
    ASL
    ROL     t3

    LDY     $2002                   ; Reset PPU addr latch.
    LDY     t3
    STY     $2006                   ; High 6 bits.
    STA     $2006                   ; Low 8 bits.

    LDY     t4
    JSR     nk_yalz_decode_ppu

    CLC                             ; C=0 marks load success.
    BCC     @exit

;--
; Load name table that refers to a loaded patt's tiles.
; NOTE: Patt is not loaded automatically.
; Inputs:
;   X: Index of referred patt.
;   Y,t1: Pointer to yalz to decode.
;   PPU ptr: Dest for yalz decode.
; Modifies:
;   ?
;--
nk_patt_name_decode:
    LDA     z_nk_decode_incr
    PHA
    LDA     v_nk_patt_load_offs, X
    STA     z_nk_decode_incr
    JSR     nk_yalz_decode_ppu
    PLA
    STA     z_nk_decode_incr
    RTS

.ENDMACRO ; _NK_PATT_CODE


.MACRO _NK_PATT_INIT
    JSR     nk_patt_clear
.ENDMACRO ; _NK_PATT_INIT
