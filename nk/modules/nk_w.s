
NK_USE_W = 1


;--
; Set a 16-bit value at address.
; Inputs:
;  addr: Address of 16-value to set.
;  value: The 16-value.
; Modifies:
;  A
;--
.MACRO  NK_W_SET addr, value
    LDA     #<(value)
    STA     addr+0
    LDA     #>(value)
    STA     addr+1
.ENDMACRO


;--
; Increment a 16-bit value at address.
; Inputs:
;  addr: 16-value to increment.
; Modifies:
;  A
;--
.MACRO  NK_W_INC addr
    INC     addr + 0
    BNE     @nonzero\@
    INC     addr + 1
@nonzero\@:
.ENDMACRO


;--
; Decrement a 16-bit value at address.
; Inputs:
;  addr: 16-value to decrement.
; Modifies:
;  A
;--
.MACRO  NK_W_DEC addr
    LDA     addr+0
    BNE     @nonzero\@
    DEC     addr+1
@nonzero\@:
    DEC     addr+0
.ENDMACRO

;--
; See nk_w_add
;--
.MACRO  NK_W_ADD num
    LDA     #<(num)
    LDY     #>(num)
    JSR     nk_w_add
.ENDMACRO


.MACRO _NK_W_ZEROPAGE
.ENDMACRO


.MACRO _NK_W_BSS
.ENDMACRO


.MACRO _NK_W_CODE
;--
; Word addition (16-bit).
; Inputs:
;  [X]: L-value low byte.
;  [X+1]: L-value high byte.
;  A: R-value low byte.
;  Y: R-value high byte.
; Outputs:
;  [X]: Result low byte.
;  [X+1]: Result high byte.
; Modifies:
;  A
;--
nk_w_add:
    CLC
    ADC     0, X
    STA     0, X
    TYA
    ADC     1, X
    STA     1, X
    RTS

;--
; Increment a word (16-bit).
; Inputs:
;  [X]: Low byte.
;  [X+1]: High byte.
; Outputs:
;  [X]: Result low byte.
;  [X+1]: Result high byte.
; Modifies:
;  ZN
;--
nk_w_inc:
    INC     0, X
    BNE     @nonzero
    INC     1, X
@nonzero:
    RTS

;--
; Compare 16-bit value vs. 8-bit value (low byte).
; Inputs:
;  A: L-value byte.
;  [X]: R-value low byte.
;  [X+1]: R-value high byte.
; Outputs:
;  C=1: L-value is less than R-value, C=0: L-value is greater or equal to R-value.
; Modifies:
;  A
;--
nk_w_cmp:
    CMP     0, X
    BCS     @end        ; if C=1 here, R-value is bigger
    LDA     1, X
    ; TODO: finish?
@end:
    RTS

;--
; Word subtraction (16-bit).
; Inputs:
;  [X]: L-value high byte.
;  [X+1]: L-value low byte.
;  Y: R-value high byte.
;  A: R-value low byte.
; Outputs:
;  [X]: Result high byte.
;  [X+1]: Result low byte.
; Modifies:
;  A,Y
;--
nk_w_sub:
    SEC
    SBC     1, X
    STA     1, X
    TYA
    SBC     0, X
    STA     0, X
    ;->

;--
; Word negation (16-bit).
; Inputs:
;  [X]: High byte.
;  [X+1]: Low byte.
; Outputs:
;  [X]: Result high byte.
;  [X+1]: Result low byte.
; Modifies:
;  A
;--
nk_w_neg:
    SEC
    LDA     #0
    SBC     1, X
    STA     1, X
    LDA     #0
    SBC     0, X
    STA     0, X
    RTS

;--
; Left shift word (16-bit).
; Inputs:
;  Y: Number of bits to shift.
;  [X]: High byte.
;  [X+1]: Low byte.
; Outputs:
;  [X]: Result high byte.
;  [X+1]: Result low byte.
; Modifies:
;  C,Y
;--
_pre_nk_w_lshift:
    ASL     0, X
    ROL     1, X
nk_w_lshift:
    DEY
    BPL     _pre_nk_w_lshift
    RTS
    
;--
; Right shift word (16-bit).
; Inputs:
;  Y: Number of bits to shift.
;  [X]: High byte.
;  [X+1]: Low byte.
; Outputs:
;  [X]: Result high byte.
;  [X+1]: Result low byte.
; Modifies:
;  C,Y
;--
_pre_nk_w_rshift:
    LSR     1, X
    ROR     0, X
nk_w_rshift:
    DEY
    BPL     _pre_nk_w_rshift
    RTS
.ENDMACRO


.MACRO _NK_W_INIT
.ENDMACRO
