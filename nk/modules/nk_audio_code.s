;--
; Copyright (c) 2016 Valtteri Heikkila
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the "Software"), to deal in the Software without
; restriction, including without limitation the rights to use,
; copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following
; conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
; OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
; OTHER DEALINGS IN THE SOFTWARE.
;--

NK_AUDIO_INLINE_GET_BYTE = 1
NK_AUDIO_INLINE_NIBBLE = 1
NK_AUDIO_INLINE_VOL_MUL = 1

NK_AUDIO_MAX_PERIOD = 2047      ; Max period value for sq1/2 and tri
NK_AUDIO_MAX_PIDX = (9*12*16)

NK_AUDIO_DICT_SIZE = 256        ; 256 or 128

c_nk_audio_period_table_lo:
    .BYTE   $5C, $50, $44, $38, $2B, $1F, $13, $07, $FB, $EF, $E3, $D7
    .BYTE   $CB, $C0, $B4, $A8, $9D, $91, $85, $7A, $6E, $63, $57, $4C
    .BYTE   $41, $35, $2A, $1F, $14, $08, $FD, $F2, $E7, $DC, $D1, $C6
    .BYTE   $BC, $B1, $A6, $9B, $91, $86, $7B, $71, $66, $5C, $51, $47
    .BYTE   $3C, $32, $28, $1D, $13, $09, $FF, $F4, $EA, $E0, $D6, $CC
    .BYTE   $C2, $B8, $AF, $A5, $9B, $91, $87, $7E, $74, $6A, $61, $57
    .BYTE   $4E, $44, $3B, $31, $28, $1E, $15, $0C, $02, $F9, $F0, $E7
    .BYTE   $DE, $D5, $CC, $C3, $BA, $B1, $A8, $9F, $96, $8D, $84, $7B
    .BYTE   $73, $6A, $61, $59, $50, $47, $3F, $36, $2E, $25, $1D, $14
    .BYTE   $0C, $04, $FB, $F3, $EB, $E3, $DA, $D2, $CA, $C2, $BA, $B2
    .BYTE   $AA, $A2, $9A, $92, $8A, $82, $7A, $73, $6B, $63, $5B, $54
    .BYTE   $4C, $44, $3D, $35, $2D, $26, $1E, $17, $0F, $08, $01, $F9
    .BYTE   $F2, $EB, $E3, $DC, $D5, $CD, $C6, $BF, $B8, $B1, $AA, $A3
    .BYTE   $9C, $95, $8E, $87, $80, $79, $72, $6B, $64, $5D, $57, $50
    .BYTE   $49, $42, $3C, $35, $2E, $28, $21, $1A, $14, $0D, $07, $00
    .BYTE   $FA, $F4, $ED, $E7, $E0, $DA, $D4, $CD, $C7, $C1, $BB, $B4
c_nk_audio_period_table_hi_4bit:
    .BYTE   $DD, $DD, $DD, $DD, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC
    .BYTE   $CC, $CC, $CC, $BB, $BB, $BB, $BB, $BB, $BB, $BB, $BB, $BB
    .BYTE   $BB, $BB, $BB, $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA
    .BYTE   $AA, $AA, $AA, $AA, $9A, $99, $99, $99, $99, $99, $99, $99
    .BYTE   $99, $99, $99, $99, $99, $99, $99, $88, $88, $88, $88, $88
    .BYTE   $88, $88, $88, $88, $88, $88, $88, $88, $88, $88, $88, $78
    .BYTE   $77, $77, $77, $77, $77, $77, $77, $77, $77, $77, $77, $77
    .BYTE   $77, $77, $77, $77, $77, $77, $66, $66, $66, $66, $66, $66

c_nk_audio_vol_mul_tab:
    .BYTE   $00, $00, $00, $00, $11, $11, $11, $11
    .BYTE   $00, $00, $11, $11, $11, $11, $22, $22
    .BYTE   $00, $10, $11, $11, $22, $22, $32, $33
    .BYTE   $00, $11, $11, $22, $22, $33, $33, $44
    .BYTE   $00, $11, $21, $22, $33, $43, $44, $55
    .BYTE   $00, $11, $22, $32, $43, $44, $55, $66
    .BYTE   $00, $11, $22, $33, $44, $55, $66, $77
    .BYTE   $10, $21, $32, $43, $54, $65, $76, $87
    .BYTE   $10, $21, $32, $44, $55, $76, $87, $98
    .BYTE   $10, $21, $33, $54, $65, $77, $98, $A9
    .BYTE   $10, $21, $43, $54, $76, $87, $A9, $BA
    .BYTE   $10, $22, $43, $65, $76, $98, $AA, $CB
    .BYTE   $10, $32, $43, $65, $87, $A9, $BA, $DC
    .BYTE   $10, $32, $54, $76, $87, $A9, $CB, $ED
    .BYTE   $10, $32, $54, $76, $98, $BA, $DC, $FE

c_nk_audio_vibrato_rate_hi:
    .BYTE     1,   1,   1,   2,   2,   2,   3,   3,   4,   6,   7,   9,  11,  13,  16,  19
c_nk_audio_vibrato_rate_lo:
    .BYTE   192, 200, 220,   4,  72, 172,  52, 240, 220,   4, 108,  32,  32, 120,  40,  64
c_nk_audio_vibrato_scale_hi:
    .BYTE     0,   0,   0,   0,   0,   0,   0,   0,   1,   2,   3,   4,   6,   9,  11,  14
c_nk_audio_vibrato_scale_lo:
    .BYTE     0,  47,  48,  51,  62,  87, 137, 226, 112,  68, 108, 242, 219,  35, 193, 165

c_nk_audio_penv_scale_hi:
    .BYTE     0,   0,   0,   0,   1,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5,   6
c_nk_audio_penv_scale_lo:
    .BYTE    13,  50, 109, 188,  27, 133, 249, 114, 239, 110, 236, 106, 229,  94, 213,  72

c_nk_audio_blookup_noise_rate_tab:
    .BYTE   104, 123,  73, 131, 114,  92,  34, 136, 127, 119, 110,  99,  83,  59,   0
c_nk_audio_blookup_dmc_rate_tab:
    .BYTE    99, 119,  85, 133, 109,  93,  79, 142, 127, 113, 105,  95,  89,  81,  75

;--
; In: t0=hi pidx (note, right-shifted by 4)
; Out: A=noise rate idx (for APU)
; Modifies: C, A, X, Y
;--
.MACRO _NK_AUDIO_BLOOKUP_NOISE_RATE
    LDA   #1
@loop\@:
    TAX
    LDY   c_nk_audio_blookup_noise_rate_tab-1, X
    CPY   t0
    ROL
    CMP   #16
    BCC   @loop\@
    EOR   #16
.ENDMACRO ; _NK_AUDIO_BLOOKUP_NOISE_RATE

;--
; In: t0=hi pidx (note, right-shifted by 4)
; Out: A=dmc rate idx (for APU)
; Modifies: C, A, X, Y
;--
.MACRO _NK_AUDIO_BLOOKUP_DMC_RATE
    LDA   #1
@loop\@:
    TAX
    LDY   c_nk_audio_blookup_dmc_rate_tab-1, X
    CPY   t0
    ROL
    CMP   #16
    BCC   @loop\@
    EOR   #31
.ENDMACRO ; _NK_AUDIO_BLOOKUP_DMC_RATE

;--
; Extract high/low nibble from byte.
; In: A=byte to extract from, C=1:high, 0:low nibble
; Out: A=result
; Modifies: C, A
;--
.MACRO _NK_AUDIO_NIBBLE_ACTUAL
    BCC   @low_nibble\@
    LSR
    LSR
    LSR
    LSR
@low_nibble\@:
    AND   #$0F
.ENDMACRO ; _NK_AUDIO_NIBBLE_ACTUAL

.IF !NK_AUDIO_INLINE_NIBBLE
nk_audio_nibble:
    _NK_AUDIO_NIBBLE_ACTUAL
    RTS
.ENDIF

.MACRO _NK_AUDIO_NIBBLE
.IF !NK_AUDIO_INLINE_NIBBLE
    JSR   nk_audio_nibble
.ELSE
    _NK_AUDIO_NIBBLE_ACTUAL
.ENDIF
.ENDMACRO ; _NK_AUDIO_NIBBLE

; Volume multiply (4x4 bits) using lookup (round() rounding).
; Input: A, t0 (both must be 0-15)
; Output: A
; Modifies: C, A, Y
.MACRO _NK_AUDIO_VOL_MUL_ACTUAL
    ASL
    ASL
    ASL
    ASL
    BEQ   @end\@
    ORA   t0

    LSR                         ; LSB -> C
    TAY
    LDA   c_nk_audio_vol_mul_tab-8, Y
    _NK_AUDIO_NIBBLE
@end\@:
.ENDMACRO ; _NK_AUDIO_VOL_MUL_ACTUAL

.IF !NK_AUDIO_INLINE_VOL_MUL
nk_audio_vol_mul:
    _NK_AUDIO_VOL_MUL_ACTUAL
    RTS
.ENDIF

.MACRO _NK_AUDIO_VOL_MUL
.IF !NK_AUDIO_INLINE_VOL_MUL
    JSR   nk_audio_vol_mul
.ELSE
    _NK_AUDIO_VOL_MUL_ACTUAL
.ENDIF
.ENDMACRO ; _NK_AUDIO_VOL_MUL

;--
; Minimal unsigned 8x16=16-bit multiplication by White Flame (David Holz)
; http://www.codebase64.org/doku.php?id=base:8bit_multiplication_16bit_product
; For max speed, make sure t0 is smaller number.
; In: t0: 8-bit value, t1,t2: 16-bit value (lo,hi)
; Out: A,Y: result (lo,hi), C=0, Z=1
; Modifies: C, A, X, Y, t0, t1, t2
;--
_nk_audio_mul_8_16_16:
    LDA   #0
    TAY
    BEQ   @enter_loop
@add:
    CLC
    ADC   z_nk_audio_mul2_lo
    TAX
    TYA
    ADC   z_nk_audio_mul2_hi
    TAY
    TXA
@loop:
    ASL   z_nk_audio_mul2_lo
    ROL   z_nk_audio_mul2_hi
@enter_loop:
    LSR   z_nk_audio_mul1_lo
    BCS   @add
    BNE   @loop
    RTS

;--
; Click-free sq1/2 period updates (does not restart phase).
; Based on technique by blargg: http://forums.nesdev.com/viewtopic.php?t=231
; NOTE: old periods must be stored in z_nk_audio_sq_period array
; In: A: set sq period (lo), Y:-"- (hi), X: 0/4 (sq1/sq2), C: 1=trigger,0=slide
; Maintains: X
;--
.MACRO _NK_AUDIO_SQ_SET_PERIOD
    PHA
    BCC   @not_trigger

;@trigger:
    TYA
    STA   $4003, X
    STA   z_nk_audio_sq_period, X
    BCS   @skip_hi              ; C=1 always

@not_trigger:
    TYA
    SEC
    SBC   z_nk_audio_sq_period, X
    BEQ   @skip_hi              ; Don't change hi
    STY   z_nk_audio_sq_period, X
    LDY   #$C0                  ; For $4017
    BCC   @sweep_dec
;@sweep_inc:
    STA   t0
    LDA   #$87                  ; Enable, add, shift=7
    STY   $4017
    STA   $4001, X
    LDA   #$FF
    BNE   @sweep_loop
@sweep_dec:
    EOR   #$FF                  ; Negate
    STA   t0
    INC   t0
    LDA   #$8F                  ; Enable, sub, shift=7
    STY   $4017
    STA   $4001, X
    LDA   #$00
@sweep_loop:
    STA   $4002, X
    STY   $4017
    DEC   t0
    BNE   @sweep_loop
    LDA   #$0F                  ; Disable, subtract, shift=7
    STA   $4001, X
@skip_hi:
    PLA
;@set_lo:
    STA   $4002, X
.ENDMACRO ; _NK_AUDIO_SQ_SET_PERIOD


;--
; Read value at chn inst env position.
; In: env=venv/penv, X=chn idx
; Out: A=env value at position
; Modifies: C, A, Y, t3, t4
;--
.MACRO _NK_AUDIO_ENV_READ env
    LDY   v_nk_audio_chn_inst_set, X

    LDA   c_song_inst_\1_ptr_hi, Y
    BPL   @constant\@
    STA   t4
    LDA   c_song_inst_\1_ptr_lo, Y
    STA   t3

    LDA   v_nk_audio_chn_\1_pos_hi, X
    LSR
    TAY
    LDA   (t3), Y

    _NK_AUDIO_NIBBLE

@constant\@:
.ENDMACRO ; _NK_AUDIO_ENV_READ

;--
; Step chn inst env playback position.
; In: env=venv/penv, maxpos=max env pos, X=chn idx
; Out: -
; Modifies: C, A, Y, t0
;--
.MACRO _NK_AUDIO_UPDATE_CHN_ENV env, maxpos
    LDY   v_nk_audio_chn_inst_set, X

    LDA   v_nk_audio_chn_len_hi, X
    STA   t0                    ; 1=released, 0=not released -> t0 bit7
    BPL   @phase_calc\@         ; Branch if note is playing
;@phase_2\@:
    LDA   c_song_inst_\1_spd2, Y
    BEQ   @env_stop\@           ; Branch if note is ended immediately
    BNE   @step\@

@phase_calc\@:
    LDA   v_nk_audio_chn_\1_pos_hi, X
    CMP   #16                   ; C=1: phase1, C=0: phase0
    LDA   c_song_inst_\1_spd1, Y
    BCS   @phase_done\@         ; Branch if phase1
;@phase_1\@:
    LDA   c_song_inst_\1_spd0, Y
@phase_done\@:

@step\@:
    TAY                         ; spd0/1/2 -> Y
    AND   #%11111100
    CLC
    ADC   v_nk_audio_chn_\1_pos_lo, X
    STA   v_nk_audio_chn_\1_pos_lo, X
    TYA
    AND   #%00000011
    ADC   v_nk_audio_chn_\1_pos_hi, X

    BIT   t0
    BPL   @not_released\@       ; Branch if not released

;@test_stop\@:
    CMP   #48
    BCC   @store_pos_hi\@       ; Branch if env is still playing.
@env_stop\@:
    ; pos_hi reached end, or note released and env spd2=0.
    LDA   #0                    ; Clamp to env maxpos
    STA   v_nk_audio_chn_\1_pos_lo, X
    LDA   #maxpos
    BNE   @store_pos_hi\@       ; Always jump

@not_released\@:
    CMP   #16
    BCC   @store_pos_hi\@       ; Phase0 after step

    CPY   #0                    ; Y=spd1 at this point
    BNE   @test_loop\@          ; Branch if looping

    TYA                         ; Always Y=0
    STA   v_nk_audio_chn_\1_pos_lo, X
    LDA   #16                   ; Warp to phase 1
    BNE   @store_pos_hi\@       ; Always jump

@test_loop\@:
    CMP   #32
    BCC   @no_loop\@
;@loop\@:
    ;SEC                         ; C=1 always
    SBC   #16
@no_loop\@:

@store_pos_hi\@:
    STA   v_nk_audio_chn_\1_pos_hi, X
@end\@:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_ENV

;--
; Update channel pidx.
; In: X: chn idx
; Out: t0,t1: pidx (lo,hi)
; Modifies: C, A, X, Y, t0, t1, t3, t4
;--
.MACRO _NK_AUDIO_UPDATE_CHN_PIDX
    ; Load pidx from v_chn_pidx_lo/hi (it has extra 3 bits precision)
    LDA   v_nk_audio_chn_pidx_hi, X
    BPL   @load_arp
;@clamp_negative_pidx:
    LDA   #0                    ; TODO: Is Y=0? Use instead of setting here?
    STA   t1
    BEQ   @load_pidx_epilog     ; Always jump

@load_arp:
    LDY   v_nk_audio_chn_arp_offs, X
    CLC
    ADC   c_song_arp_data, Y
    ; TODO: What if we overflow?

;@load_pidx:
    LSR
    STA   t1
    LDA   v_nk_audio_chn_pidx_lo, X
    ROR
    LSR   t1
    ROR
    LSR   t1
    ROR
    LSR   t1
    ROR
@load_pidx_epilog:
    STA   t0
;@load_pidx_done:

;@update_detune:
    LDY   v_nk_audio_chn_inst_set, X
    LDA   c_song_inst_det, Y
    ;AND   #$0F                  ; Mask out detune bits
    CLC
    ADC   t0
    STA   t0
    BCC   @detune_done
    INC   t1
@detune_done:

;@test_vibrato:
    ; TODO: Could be possible to only test vib_pos_hi bit7?
    ;LDY   v_nk_audio_chn_inst_set, X ; Already loaded (Y=inst idx)
    LDA   c_song_inst_vib, Y
    CMP   #$10
    BCC   @vibrato_done         ; Jump if no vibrato
    LDY   v_nk_audio_chn_vib_pos_hi, X
    BMI   @vibrato_done         ; Jump if vibrato delayed

;@calc_vibrato:
    ; Load vibrato scale to mul2
    LSR
    LSR
    LSR
    LSR
    TAX
    LDA   c_nk_audio_vibrato_scale_hi, X
    STA   z_nk_audio_mul2_hi
    LDA   c_nk_audio_vibrato_scale_lo, X
    STA   z_nk_audio_mul2_lo

    ; Calc vibrato table index
    TYA                         ; vib_pos_hi -> A
    STA   t3                    ; vib_pos_hi -> t3
    AND   #31
    CMP   #16
    BCC   @skip_descending
;@descending:
    EOR   #31                   ;= 31 - A, invert for descending wave
@skip_descending:
    STA   z_nk_audio_mul1_lo    ; Store vibrato value -> mul1
    JSR   _nk_audio_mul_8_16_16

    LDA   t3                    ; vib_pos_hi -> A
    CMP   #32                   ; Set C for add/sub selection
    TYA                         ; Mult result hi -> A
    LDY   #0                    ; 0 -> Y
    BCC   @vibrato_add
;@vibrato_sub:
    DEY                         ; -1 -> Y
    EOR   #$FF
    ;SEC                         ; C=1 always
@vibrato_add:
    ADC   t0                    ; C=0 always
    STA   t0
@vibrato_add_end:
    TYA
    ADC   t1
    STA   t1
@vibrato_done:

    ; Update pitch envelope
;@update_penv:
    LDX   v_nk_audio_chn_idx    ; Restore chn idx
    LDY   v_nk_audio_chn_inst_set, X ; Restore inst idx
    LDA   c_song_inst_penv_scl, Y
    TAY
    LDA   c_nk_audio_penv_scale_lo, Y
    STA   z_nk_audio_mul2_lo
    LDA   c_nk_audio_penv_scale_hi, Y
    STA   z_nk_audio_mul2_hi

    _NK_AUDIO_ENV_READ penv     ; Pitch env value -> A

    SEC
    SBC   #7                    ; Subtracted pevn value -> A
    BEQ   @penv_done
    PHA                         ; Push subtracted pevn value -> stack
    BCS   @penv_positive
;@penv_negative:
    ; Negate A
    EOR   #$FF
    ;CLC                        ; C=0 always
    ADC   #1
@penv_positive:
    STA   z_nk_audio_mul1_lo    ; Store vibrato value -> mul1

    JSR   _nk_audio_mul_8_16_16
    LDX   v_nk_audio_chn_idx    ; Restore chn idx
    STY   z_nk_audio_mul2_lo
    CMP   #$80
    ROL   z_nk_audio_mul2_lo
    ROL
    ROL   z_nk_audio_mul2_lo
    ROL
    ROL   z_nk_audio_mul2_lo
    ROL
    AND   #%00000111            ; Clear garbage bits 3..7
    STA   z_nk_audio_mul2_hi

    PLA                         ; Pop subtracted pevn value -> A
    BPL   @penv_add
;@penv_sub:
    SEC
    LDA   t0
    SBC   z_nk_audio_mul2_lo
    STA   t0
    LDA   t1
    SBC   z_nk_audio_mul2_hi
    JMP   @penv_epilog

@penv_add:
    CLC
    LDA   t0
    ADC   z_nk_audio_mul2_lo
    STA   t0
    LDA   t1
    ADC   z_nk_audio_mul2_hi
@penv_epilog:
    STA   t1
@penv_done:

@clamp_pidx:
    ; Check from approx lower bound at hi byte if we want to clamp
    LDA   t1
    CMP   #(NK_AUDIO_MAX_PIDX+255) / 256 ; Note, ceil()
    BCC   @clamp_pidx_done
    ; We clamp, decide min or max limit from mean of lower bound and 255 (max value)
    CMP   #((NK_AUDIO_MAX_PIDX+255) / 256 + 255) / 2
    BCC   @clamp_pidx_hi
;@clamp_pidx_lo:
    LDA   #0
    STA   t0
    BEQ   @clamp_pidx_epilog
@clamp_pidx_hi:
    LDA   #<(NK_AUDIO_MAX_PIDX-1)
    STA   t0
    LDA   #>(NK_AUDIO_MAX_PIDX-1)
@clamp_pidx_epilog:
    STA   t1
@clamp_pidx_done:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_PIDX

;--
; Calculate APU period
; In: t0,t1: pidx (lo,hi)
; Out: A,X: period (lo,hi)
; Modifies: C, A, X, Y, t0
;--
.MACRO _NK_AUDIO_CALC_PERIOD
    LDY   #0
    LDA   t1
    BPL   @calc_octave
;@clamp_negative_pidx_again:
    LDX   #0
    BEQ   @load_periods         ; Skip octave calculation

; in: t0,t1: pidx (lo,hi), X: chn idx
; out: Y: octave, X: ptabidx
@calc_octave:
    DEY                         ; Y=$FF
    ; TODO: Optimize inputs?
    LDA   t0
    LDX   t1
@octave_loop:
    SEC
@octave_loop_inner:
    INY
    SBC   #12*16
    BCS   @octave_loop_inner
    DEX
    BPL   @octave_loop
;@octave_done:
    ADC   #12*16                ; C=0 always
    TAX

; in: X: ptabidx, Y: octave
@load_periods:
    LDA   c_nk_audio_period_table_lo, X
    STA   t0

    TXA
    LSR                         ; Set ptabidx LSB -> C
    TAX
    LDA   c_nk_audio_period_table_hi_4bit, X

    _NK_AUDIO_NIBBLE

; in: Y: octave, t0,A: period (lo,hi)
; out: A,X: period (lo,hi)
;@shift_period:
    DEY
    BMI   @octave_shift_done
@octave_shift_loop:
    LSR
    ROR   t0
    DEY
    BPL   @octave_shift_loop
@octave_shift_done:
    TAX
    LDA   t0

; NOTE: Needed because periods of highest octave exceed NK_AUDIO_MAX_PERIOD
; (its low notes are below the limit and high notes are above).
; in: A: period (lo), X: period (hi)
; out: A: period (lo), X: period (hi)
;@clamp_period:
    CPX   #>(NK_AUDIO_MAX_PERIOD+1)
    BCC   @clamp_period_skip
    LDX   #>NK_AUDIO_MAX_PERIOD
    LDA   #<NK_AUDIO_MAX_PERIOD
@clamp_period_skip:
.ENDMACRO ; _NK_AUDIO_CALC_PERIOD

;--
; Init driver.
;--
nk_audio_init:
    ; Silence channels
    LDX   #NK_AUDIO_NUM_CHNS-1
    LDA   #48
@silence_chn_loop:
    STA   v_nk_audio_chn_venv_pos_hi, X
    DEX
    BPL   @silence_chn_loop

    LDA   #%00001111            ; Enable sq1,sq2,tri,noise (not dmc)
    STA   $4015

    ; Setup click-free sq1/2 playback
    LDA   #%00001000            ; Sweep unit negate flag
    STA   $4001                 ; sq1
    STA   $4001+4               ; sq2
    LDA   #0                    ; Timer to zero
    LDX   #%10110000            ; Toggle as silent
    STA   $4003                 ; Setup sq1
    STA   $4002
    STX   $4000
    STA   $4003+4               ; Setup sq2
    STA   $4002+4
    STX   $4000+4

    STA   $4011                 ; Set dmc level

    RTS

;--
; Play song part.
; In: X: song index, Y: 0=sfx, 1=bgm
; Modifies: C, A, X, Y, t0, t1
;--
nk_audio_play:
    LDA   c_song_part_ptr_lo, X
    STA   t0
    LDA   c_song_part_ptr_hi, X
    STA   t1

    TYA
    ORA   #v_nk_audio_part_ptrs_end-v_nk_audio_part_ptrs-2 ; bit0 is 0
    TAX
    LDY   #(v_nk_audio_part_ptrs_end-v_nk_audio_part_ptrs-2)/NK_AUDIO_NUM_PLAYERS
@copy_loop:
    LDA   (t0), Y
    STA   v_nk_audio_part_ptrs, X

    DEX
    DEX
    DEY
    BPL   @copy_loop

    BMI   _nk_audio_reset_channels ; Always jump

;--
; Stop song playback.
; In: A: 0=sfx, 1=bgm
; Modifies: C, A, X
;--
nk_audio_stop:
    ORA   #v_nk_audio_part_ptrs_end-v_nk_audio_part_ptrs-2 ; Bit0 is 0
    TAX

    LDA   #0
@clear_loop:
    STA   v_nk_audio_part_ptrs, X
    DEX
    DEX
    BPL   @clear_loop
    ; -->

; In: X: -2=sfx, -1=bgm
_nk_audio_reset_channels:
    TXA
    EOR   #-2-(NK_AUDIO_NUM_CHNS-2) ; X is either -2 or -1
    TAX

@reset_loop:
    LDA   #48                   ; Silence channel
    STA   v_nk_audio_chn_venv_pos_hi, X
    LDA   #0
    STA   v_nk_audio_chn_delay, X ; Trigger frame decode

    CPX   #NK_AUDIO_NUM_VOL_CHNS ; Skip volume for tri and dmc
    BCS   @skip_reset_vol
    STA   v_nk_audio_chn_vol_delay_hi, X ; Trigger vol frame decode
    STA   v_nk_audio_chn_vol_delay_lo, X
@skip_reset_vol:

    DEX
    DEX
    BPL   @reset_loop

    RTS

; TODO: Optimize
;--
; Update channel volume.
; In: X=chn idx
; Modifies: C, A, t0
;--
.MACRO _NK_AUDIO_UPDATE_CHN_VOL
    CPX   #NK_AUDIO_NUM_VOL_CHNS ; Skip if tri or dmc
    BCS   @end\@

    LDA   v_nk_audio_chn_vol_ptr_hi, X
    BPL   @is_constant\@        ; Branch if constant volume (address < $8000)
    STA   z_nk_audio_ptr+1      ; Load ptr
    LDY   v_nk_audio_chn_vol_ptr_lo, X

    LDA   v_nk_audio_chn_vol_delay_lo, X ; Check if to advance
    BNE   @dec_delay_lo\@
    DEC   v_nk_audio_chn_vol_delay_hi, X
    BPL   @dec_delay_lo\@

;@advance:
    ;LDA   #0                    ; Already 0
    ;STA   v_nk_audio_chn_vol_delay_lo, X ; Already 0
    STA   v_nk_audio_chn_vol_delay_hi, X

    _NK_AUDIO_PTR_PEEK
    AND   #$0F
    STA   v_nk_audio_chn_vol_set, X
    _NK_AUDIO_GET_BYTE

    CMP   #$E0
    BCS   @is_2_or_3\@
;@is_1\@:
    LSR
    LSR
    LSR
    LSR
    JMP   @store_delay_lo\@

@is_2_or_3\@:
    CMP   #$F0
@store_delay_lo_if_cc\@:
    _NK_AUDIO_GET_BYTE          ; Doesn't modify C
    BCC   @store_delay_lo\@     ; Branch if 2 or 3 bytes
;@is_3\@:
    CMP   #$80
    BCS   @is_3_loop\@
;@is_3_not_loop\@:
    STA   v_nk_audio_chn_vol_delay_hi, X
    CLC
    BCC   @store_delay_lo_if_cc\@

@is_3_loop\@:
    PHA                         ; Store offset hi
    _NK_AUDIO_PTR_PEEK          ; Read offset lo

    STY   t0                    ; Apply offset lo, Y -> t0
    CLC
    ADC   t0
    TAY                         ; Result vol_ptr_lo -> Y

    PLA                         ; Apply offset hi to vol_ptr_hi
    ADC   z_nk_audio_ptr+1
    STA   z_nk_audio_ptr+1      ; Result vol_ptr_hi -> z_nk_audio_ptr+1
    JMP   @done\@

@is_constant\@:
    STA   v_nk_audio_chn_vol_set, X
    BPL   @end\@                ; Always jump

@dec_delay_lo\@:
    SEC                         ; A=delay_lo
    SBC   #1
@store_delay_lo\@:
    STA   v_nk_audio_chn_vol_delay_lo, X

@done\@:
    TYA                         ; Store ptr
    STA   v_nk_audio_chn_vol_ptr_lo, X
    LDA   z_nk_audio_ptr+1
    STA   v_nk_audio_chn_vol_ptr_hi, X
@end\@:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_VOL

;--
; Decrement note length.
; In: X=chn idx
; Modifies: C, A
;--
.MACRO _NK_AUDIO_UPDATE_CHN_LEN
    LDA   v_nk_audio_chn_len_hi, X
    BMI   @len_end              ; Branch if released
    LDA   v_nk_audio_chn_len_lo, X
    BNE   @len_dec_lo
    DEC   v_nk_audio_chn_len_hi, X
@len_dec_lo:
    DEC   v_nk_audio_chn_len_lo, X
@len_end:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_LEN

; In: X=chn idx
; Modifies: C, A
.MACRO _NK_AUDIO_UPDATE_CHN_SLOPE
    LDA   v_nk_audio_chn_len_hi, X
    BMI   @skip_update_slope    ; Branch if released

    CLC
    LDA   v_nk_audio_chn_pidx_lo, X
    ADC   v_nk_audio_chn_slope_lo, X
    STA   v_nk_audio_chn_pidx_lo, X

    LDA   v_nk_audio_chn_pidx_hi, X
    ADC   v_nk_audio_chn_slope_hi, X
    STA   v_nk_audio_chn_pidx_hi, X
@skip_update_slope:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_SLOPE

; In: X=chn idx
; Modifies: C, A, Y
.MACRO _NK_AUDIO_UPDATE_CHN_ARP
    LDY   v_nk_audio_chn_inst_set, X

    DEC   v_nk_audio_chn_arp_delay, X
    BPL   @arp_update_end

    LDA   c_song_inst_arp_rate, Y ; Reset arp delay counter
    STA   v_nk_audio_chn_arp_delay, X

    LDY   v_nk_audio_chn_arp_offs, X ; Step arp offset
    INY
    LDA   c_song_arp_data, Y
    BNE   @arp_seek_end
@arp_seek_loop:                 ; Seek back for arp start (first 0)
    DEY
    LDA   c_song_arp_data, Y
    BNE   @arp_seek_loop
@arp_seek_end:
    TYA
    STA   v_nk_audio_chn_arp_offs, X
@arp_update_end:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_ARP


; Calc current actual channel volume (from instrument volume and venv).
; NOTE: Result contains modified from mod_vol.
; In: X=chn idx
; Out: A=resulting volume (bits 0-3), mod_vol modifier (bits 4-7)
; Modifies: C, A, Y, t0, t3, t4
.MACRO _NK_AUDIO_CHN_CALC_VOL

    _NK_AUDIO_ENV_READ venv     ; Volume env value -> A

    CPX   #NK_AUDIO_NUM_VOL_CHNS ; Skip if tri or dmc
    BCS   @skip_mul\@

    STA   t0
    LDA   v_nk_audio_chn_vol_set, X

    _NK_AUDIO_VOL_MUL

    STA   t0

    LDY   v_nk_audio_chn_inst_set, X
    LDA   v_nk_audio_chn_penv_pos_hi, X
    CMP   c_song_inst_mod_pos, Y
    LDA   c_song_inst_mod_vol, Y
    TAY
    BCC   @modifier0
;@modifier1:
    ASL
    ASL
@modifier0:
    AND   #%11000000
    STA   v_nk_audio_modifier

    TYA
    AND   #%00001111

    _NK_AUDIO_VOL_MUL

    ORA   #%00110000

@skip_mul\@:
.ENDMACRO ; _NK_AUDIO_CHN_CALC_VOL

;--
; Update APU chn sound playback.
; In: X=chn idx
;--
.MACRO _NK_AUDIO_UPDATE_APU
    ; TODO: Optimize
    TXA
    LSR
    STA   v_nk_audio_wave_idx
    BCS   @bgm_chn

;@sfx_chn:
    LDA   v_nk_audio_chn_venv_pos_hi, X
    CMP   #48
    BCC   @play_prepare
    JMP   @update_apu_end

@bgm_chn:
    LDA   v_nk_audio_chn_venv_pos_hi-1, X
    CMP   #48
    BCS   @sfx_inactive
    JMP   @update_apu_end

@sfx_inactive:
    LDA   v_nk_audio_chn_venv_pos_hi, X
    CMP   #48
    BCS   @silence

@play_prepare:
    _NK_AUDIO_CHN_CALC_VOL

    STA   t2                    ; Store vol mult result
    AND   #$0F
    BNE   @play

@silence:
    LDA   #%10110000            ; For silencing sq1/2 and noise (vol=0, duty=0)

    LDX   v_nk_audio_wave_idx
    DEX
    BMI   @silence_sq1
    BEQ   @silence_sq2
    DEX
    BEQ   @silence_noise
    DEX
    BEQ   @silence_tri

;@silence_dmc:
    LDA   #%00001111
    STA   $4015
    JMP   @update_apu_end

@silence_sq1:
    STA   $4000 ; sq1
    JMP   @update_apu_end

@silence_sq2:
    STA   $4004 ; sq2
    JMP   @update_apu_end

@silence_tri:
    ; Silence triangle (at next APU frame counter tick)
    STX   $4008
    DEX                         ; Overflow to $FF
    ; Load linear counter w/ $4008, set tri frequency to something low
    STX   $400B
    ; Trigger frame counter tick (immediately silence tri)
    STX   $4017
    JMP   @update_apu_end

@silence_noise:
    STA   $400C
    STX   $400F                 ; TODO: Needed for some reason
    JMP   @update_apu_end

@play:
    _NK_AUDIO_UPDATE_CHN_PIDX

    TXA                         ; Jump to special if noise or dmc
    EOR   #2                    ; Flip noise and tri chn idx for single compare
    CMP   #3*NK_AUDIO_NUM_PLAYERS
    BCS   @play_special

;@not_special:
    _NK_AUDIO_CALC_PERIOD

    ; TODO: Optimize this
    STX   t0                    ; Move p(hi) X -> Y
    LDY   t0

    LSR   v_nk_audio_retrig     ; Retrig bit -> C
    LDX   v_nk_audio_wave_idx
    BEQ   @play_sq1
    DEX
    BEQ   @play_sq2

;@play_tri:
    LDX   #%10000001            ; Enable tri
    STX   $4008
    STA   $400A
    STY   $400B
    ; TODO: Is it needed? Ticks frame timer immediately
    ;STX   $4017
    JMP   @update_apu_end

@play_special:
    ; Shift by 3 for blookup
    LDA   t1
    LSR
    ROR   t0
    LSR
    ROR   t0
    LSR
    ROR   t0
    CPX   #4*NK_AUDIO_NUM_PLAYERS
    BCS   @play_dmc

;@play_noise:

    _NK_AUDIO_BLOOKUP_NOISE_RATE
    ORA   v_nk_audio_modifier   ; Rate idx OR modifier (noise mode) -> A
    STA   $400F                 ; TODO: tsone: Needed for some reason
    STA   $400E                 ; Set noise rate w/ mode

    LDA   t2                    ; Volume -> A
    STA   $400C                 ; Set volume
    JMP   @update_apu_end       ; TODO: Optimize

@play_sq2:
    LDX   #4
@play_sq1:
    ;LDX   #0                    ; X=0 at this point (or X=4 for sq2)
    _NK_AUDIO_SQ_SET_PERIOD

    LDA   t2                    ; Volume -> A
    ORA   v_nk_audio_modifier   ; Volume OR modifier (duty cycle) -> A
    STA   $4000, X
    BNE   @update_apu_end       ; Z=0 always

@play_dmc:
    LDA   v_nk_audio_retrig
    BEQ   @update_apu_end

;@play_dmc_retrig:
    ; Get dmc sample index (bits 7-1) and loop flag (bit 0)
    LDY   v_nk_audio_chn_inst_set, X
    LDA   c_song_inst_mod_vol, Y
    LSR
    TAX
    PHP                         ; Push loop flag (C)
    LDA   c_song_dmc_addr, X
    STA   $4012                 ; Set sample address
    LDA   c_song_dmc_len, X
    STA   $4013                 ; Set sample length

    _NK_AUDIO_BLOOKUP_DMC_RATE

    PLP                         ; Pop loop flag (C)
    BCC   @no_loop
    ORA   #%01000000            ; Set sample to loop
@no_loop:
    STA   $4010                 ; Set rate and loop
    LDA   #%00001111            ; Trigger dmc
    STA   $4015
    LDA   #%00011111
    STA   $4015

@update_apu_end:
.ENDMACRO ; _NK_AUDIO_UPDATE_APU

;--
; Init chn envelopes from inst
; In: env=venv/penv, X=chn idx, Y=inst idx
; Out: -
; Modifies: C, A
;--
.MACRO _NK_AUDIO_CHN_RESET_ENV env
    LDA   #0
    STA   v_nk_audio_chn_\1_pos_lo, X
    CMP   c_song_inst_\1_spd0, Y
    BNE   @skip_jump\@
    LDA   #16
@skip_jump\@:
    STA   v_nk_audio_chn_\1_pos_hi, X
.ENDMACRO ; _NK_AUDIO_CHN_RESET_ENV


;--
; Peek (read) byte at pointer.
; In: z_nk_audio_ptr=0, z_nk_audio_ptr+1=pointer hi, Y=pointer lo
; Out: A=fetched byte
; Modifies: A
;--
.MACRO _NK_AUDIO_PTR_PEEK
    LDA   (z_nk_audio_ptr), Y
.ENDMACRO ; _NK_AUDIO_PTR_PEEK

;--
; Increment pointer.
; In: z_nk_audio_ptr+1=pointer hi, Y=pointer lo
; Modifies: Y, z_nk_audio_ptr+1
;--
.MACRO _NK_AUDIO_PTR_INC
    INY
    BNE   @no_hi_incr\@
    INC   z_nk_audio_ptr+1
@no_hi_incr\@:
.ENDMACRO ; _NK_AUDIO_PTR_INC

;--
; Read byte at pointer and increment (w/o changing C).
; In: z_nk_audio_ptr=0, z_nk_audio_ptr+1=pointer hi, Y=pointer lo
; Out: A=fetched byte
; Modifies: A, Y, z_nk_audio_ptr+1
;--
.MACRO _NK_AUDIO_GET_BYTE_ACTUAL
    _NK_AUDIO_PTR_PEEK
    _NK_AUDIO_PTR_INC
.ENDMACRO ; _NK_AUDIO_GET_BYTE_ACTUAL

.IF !NK_AUDIO_INLINE_GET_BYTE
nk_audio_get_byte:
    _NK_AUDIO_GET_BYTE_ACTUAL
    RTS
.ENDIF

.MACRO _NK_AUDIO_GET_BYTE
.IF !NK_AUDIO_INLINE_GET_BYTE
    JSR   nk_audio_get_byte
.ELSE
    _NK_AUDIO_GET_BYTE_ACTUAL
.ENDIF
.ENDMACRO ; _NK_AUDIO_GET_BYTE

.MACRO _NK_AUDIO_UPDATE_CHN_STREAM
    LDA   v_nk_audio_chn_delay, X
    BEQ   @frame_advance
;@not_frame_advance:
    DEC   v_nk_audio_chn_delay, X
    JMP   @update_chn_end

@frame_advance:
    LDY   v_nk_audio_chn_ptr_lo, X
    LDA   v_nk_audio_chn_ptr_hi, X
    STA   z_nk_audio_ptr+1

;--
; In: Y: MUST BE 0, X: chn idx
; Out: C: If set, don't change note.
;--
;nk_audio_chn_decode_frame:

    ; Reset some variables.
    LDA   #0
    STA   v_nk_audio_chn_slope_lo, X
    STA   v_nk_audio_chn_slope_hi, X
    STA   v_nk_audio_chn_arp_offs, X
    LDA   #$80
    STA   v_nk_audio_len_override_hi ; Bit7=1 -> no len override
    STA   t2                    ; Bit7=1 -> not dict frame

@frame_decode:

    ; Begin decode.
    _NK_AUDIO_GET_BYTE

; Dictionary command
@cmd_dict:
    LSR
    BCC   @cmd_loop

    STA   t2                    ; If t2 bit7=0, signals dict frame

    TYA
    STA   v_nk_audio_chn_ptr_lo, X
    LDA   z_nk_audio_ptr+1
    STA   v_nk_audio_chn_ptr_hi, X

    LDA   t2
.IF NK_AUDIO_DICT_SIZE == 256
    ASL                         ; Actual dict offset
    ;CLC                         ; Always C=0
.ELSE
    CLC
.ENDIF
    ADC   #<c_song_dict
    TAY
    LDA   #>c_song_dict
    ADC   #0
    STA   z_nk_audio_ptr+1
    _NK_AUDIO_GET_BYTE
    ;LSR                         ; Optimized away at export

; Loop command
@cmd_loop:
    CMP   #%01100000            ; Indicated by trigger and offset set at same time
    BCC   @cmd_instrument
    AND   #%00011111            ; Remaining bits, offset hi -> t1
    STA   t1
    _NK_AUDIO_GET_BYTE
    STA   t0                    ; Offset lo -> t0

    TXA                         ; Rewind or stop player playback
    AND   #1
    ;SEC                         ; C=1 always
    ADC   #0                    ; NK_AUDIO_SFX_NO_LOOP etc. -> A
    BIT   v_nk_audio_flags
    BEQ   @sub_rewind           ; Branch to rewind if bit=0

;@sub_stop:
    LSR                         ; 0=sfx or 1=bgm -> A
    JSR   nk_audio_stop
    JMP   _nk_audio_update_chn_loop_skip ; Stopped playing, skip everything

@sub_rewind:
    TYA
    SEC
    SBC   t0                    ; Subtract offset from ptr
    TAY
    LDA   z_nk_audio_ptr+1
    SBC   t1
    STA   z_nk_audio_ptr+1
    JMP   @frame_decode         ; We are done already!

; Instrument command
@cmd_instrument:
    LSR
    STA   t1                    ; Decoded cmd -> t1
    BCC   @cmd_note_and_slope
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_chn_inst_set, X

; Note and slope command
@cmd_note_and_slope:
    LSR   t1
    BCC   @cmd_arp
    _NK_AUDIO_GET_BYTE
    CMP   #$FF
    BEQ   @sub_slope            ; Branch if note is unset (just has slope)
;@sub_note:
    LSR
    STA   v_nk_audio_chn_note_set, X
    BCC   @sub_note_and_slope_end ; Branch if only has note
@sub_slope:
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_chn_slope_lo, X
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_chn_slope_hi, X
@sub_note_and_slope_end:
    SEC                         ; NOTE: C=1 to begin note

; Arpeggio command
@cmd_arp:
    ROR   t1                    ; NOTE: Carry to t1 to begin note
    BCC   @cmd_length
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_chn_arp_offs, X ; Set arpeggio offset
    ;SEC                         ; NOTE: C=1 always, C=1 to begin note

; Length command
@cmd_length:
    ROR   t1                    ; NOTE: Carry to t1 to begin note
    BCC   @cmd_delay
    LDA   #0
    STA   v_nk_audio_len_override_hi ; Clear hi for one_byte route
    _NK_AUDIO_GET_BYTE
    LSR
    BCC   @sub_length_one_byte  ; Bit0 chooses # of bytes
;@sub_length_two_bytes:
    STA   v_nk_audio_len_override_hi ; Store 1st byte instead as hi
    _NK_AUDIO_GET_BYTE
@sub_length_one_byte:
    STA   v_nk_audio_len_override_lo

; Delay command
@cmd_delay:
    LSR   t1
    BCC   @cmd_trigger
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_chn_delay_set, X

; Trigger command
@cmd_trigger:
    LSR   t1
    BCC   @cmd_offset
    LDA   #1
    STA   v_nk_audio_retrig
    ;SEC                        ; NOTE: C=1 always, C=1 to begin note

; Offset and loop command
@cmd_offset:
    ROR   t1                    ; NOTE: Carry to t1 to begin note
    BCC   @cmd_done
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_vib_pos_override
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_venv_pos_override
    _NK_AUDIO_GET_BYTE
    STA   v_nk_audio_penv_pos_override
    ;SEC                         ; NOTE: always C=1, C=1 signals offset (also begins note)

; Done
@cmd_done:
    ROR   t1                    ; NOTE: Carry to t1 to begin note
    BIT   t2
    BPL   @skip_store_ptr
    TYA
    STA   v_nk_audio_chn_ptr_lo, X
    LDA   z_nk_audio_ptr+1
    STA   v_nk_audio_chn_ptr_hi, X
@skip_store_ptr:

    LDA   v_nk_audio_chn_delay_set, X ; Reset delay
    STA   v_nk_audio_chn_delay, X

    LDA   t1                    ; If zero, skip note update
    BNE   @begin_note
    JMP   @frame_done

@begin_note:

    LDA   v_nk_audio_chn_note_set, X ; Set pidx from note
    STA   v_nk_audio_chn_pidx_hi, X
    LDA   #0
    STA   v_nk_audio_chn_pidx_lo, X

    TXA                         ; Load instrument length pointer -> t3,t4
    AND   #1
    TAY                         ; Pointer index: 0=sfx, 1=bgm -> Y
    LDA   v_nk_audio_inst_len_ptr_lo, Y
    STA   t3
    LDA   v_nk_audio_inst_len_ptr_hi, Y
    STA   t4

    LDY   v_nk_audio_chn_inst_set, X
    LDA   c_song_inst_arp_rate, Y ; Reset arpeggio
    STA   v_nk_audio_chn_arp_delay, X

    TYA                         ; Get instrument length
    ASL
    TAY
    LDA   (t3), Y
    STA   v_nk_audio_chn_len_lo, X
    INY
    LDA   (t3), Y
    STA   v_nk_audio_chn_len_hi, X

    BIT   t1                    ; Offset (bit7) -> N, trigger (bit6) -> V
    BVS   @skip_offset_override ; Branch if trigger=1
    BPL   @frame_done           ; Branch if offset=0 and trigger=0

;@offset_override:
    LDA   v_nk_audio_venv_pos_override
    STA   v_nk_audio_chn_venv_pos_lo, X ; NOTE: Garbage in fractional bits
    AND   #%00111111
    STA   v_nk_audio_chn_venv_pos_hi, X

    LDA   v_nk_audio_penv_pos_override
    STA   v_nk_audio_chn_penv_pos_lo, X ; NOTE: Garbage in fractional bits
    AND   #%00111111
    STA   v_nk_audio_chn_penv_pos_hi, X

    LDA   v_nk_audio_vib_pos_override
    BPL   @skip_note_release
    ; Special case to set note released (length override cmd can't do this)
    STA   v_nk_audio_chn_len_hi, X ; vib_pos override bit7=1 -> len_hi (releases note)
@skip_note_release:
    ASL
    STA   v_nk_audio_chn_vib_pos_hi, X
    LDA   #0
    STA   v_nk_audio_chn_vib_pos_lo, X
    BEQ   @frame_done           ; Always branch

@skip_offset_override:
    LDY   v_nk_audio_chn_inst_set, X
    LDA   c_song_inst_vib_dly, Y ; Reset vibrato length
    STA   v_nk_audio_chn_vib_pos_hi, X

    _NK_AUDIO_CHN_RESET_ENV venv
    _NK_AUDIO_CHN_RESET_ENV penv

@frame_done:

    LDA   v_nk_audio_len_override_hi
    BMI   @skip_len_override    ; If bit7=1 -> no len override
;@len_override:
    STA   v_nk_audio_chn_len_hi, X
    LDA   v_nk_audio_len_override_lo
    STA   v_nk_audio_chn_len_lo, X
@skip_len_override:

@update_chn_end:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_STREAM

;--
; Update chn vibrato.
; In: X=chn idx, Y=inst idx
; Out: -
; Modifies: C, A, Y
;--
.MACRO _NK_AUDIO_UPDATE_CHN_VIBRATO
    LDA   c_song_inst_vib, Y
    CMP   #$10
    BCC   @vib_end              ; Jump if no vibrato
    ; Test vibrato delay
    LDY   v_nk_audio_chn_vib_pos_hi, X
    BPL   @vib_update           ; Test bit7 (is positive)

;@vib_delay:
    INC   v_nk_audio_chn_vib_pos_hi, X
    BCS   @vib_end              ; Always jump (C=1)

@vib_update:
    AND   #$0F
    TAY                         ; Vib rate table index -> Y

    CLC
    LDA   c_nk_audio_vibrato_rate_lo, Y
    ADC   v_nk_audio_chn_vib_pos_lo, X
    STA   v_nk_audio_chn_vib_pos_lo, X
    LDA   c_nk_audio_vibrato_rate_hi, Y
    ADC   v_nk_audio_chn_vib_pos_hi, X
    AND   #63                   ; NOTE: Done to keep bit7 of pos_hi clear (is positive)
    STA   v_nk_audio_chn_vib_pos_hi, X
@vib_end:
.ENDMACRO ; _NK_AUDIO_UPDATE_CHN_VIBRATO

;--
;--
_nk_audio_update:
    LDX   #NK_AUDIO_NUM_CHNS-1
_nk_audio_update_chn_loop:
    STX   v_nk_audio_chn_idx

    TXA                         ; Test paused status
    AND   #1
    CLC
    ADC   #NK_AUDIO_BGM_PAUSE-1
    AND   #NK_AUDIO_BGM_PAUSE|NK_AUDIO_SFX_PAUSE
    BIT   v_nk_audio_flags
    BEQ   @not_paused
    ; Works due NK_AUDIO_BGM_PAUSE|NK_AUDIO_SFX_PAUSE>=48
    STA   v_nk_audio_chn_venv_pos_hi, X
    JMP   _nk_audio_update_chn_loop_skip

@not_paused:
    LDA   v_nk_audio_chn_ptr_hi, X
    BNE   @chn_active
    JMP   _nk_audio_update_chn_loop_skip

@chn_active:
    LDA   #0
    STA   v_nk_audio_retrig

    _NK_AUDIO_UPDATE_CHN_VOL

    _NK_AUDIO_UPDATE_CHN_LEN

    _NK_AUDIO_UPDATE_CHN_ENV venv, 48
    _NK_AUDIO_UPDATE_CHN_ENV penv, 47

    _NK_AUDIO_UPDATE_CHN_SLOPE
    _NK_AUDIO_UPDATE_CHN_ARP

    _NK_AUDIO_UPDATE_CHN_STREAM

    LDY   v_nk_audio_chn_inst_set, X
    _NK_AUDIO_UPDATE_CHN_VIBRATO

_nk_audio_update_chn_loop_skip:
    _NK_AUDIO_UPDATE_APU

    LDX   v_nk_audio_chn_idx
    DEX
    BMI   @chn_loop_end
    JMP   _nk_audio_update_chn_loop
@chn_loop_end:

    RTS
