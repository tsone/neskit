
NK_USE_BCD = 1
;NK_BCD_TERM = $80


.MACRO _NK_BCD_ZEROPAGE
z_nk_bcd_temp: .RES 1
.ENDMACRO ; _NK_BCD_ZEROPAGE


.MACRO _NK_BCD_BSS
.ENDMACRO ; _NK_BCD_BSS


.MACRO _NK_BCD_CODE
;--
; BCD addition. Result is stored in A operand.
; A operand is assumed to be 256 bytes long BCD,
; i.e. length is only limited by B operand.
; Inputs:
;   Y: Number of digits in B operand.
;   [X+*]: A operand zeropage address.
;   (t0,t1): B operand address.
; Outputs:
;   [X+*]: Result in zeropage.
; Modifies:
;   A,X,Y
;--
nk_bcd_add:
    DEY
    STY     z_nk_bcd_temp
    BEQ     @end            ; Quit if len=0.
    LDY     #0
    CLC
@add_loop:
    LDA     (t0), Y
@carry_loop:
    ADC     0, X
    CMP     #10
    BCC     @less_than_10
    SBC     #10             ; C=1 before and after.
@less_than_10:
    STA     0, X
    INX
    INY
    DEC     z_nk_bcd_temp
    BPL     @add_loop
    LDA     #0
    BCS     @carry_loop     ; Do carried digits if C=1.
@end:
    RTS

.IF 0
;--
; BCD addition.
; NOTE: B operand must contain NK_BCD_TERM (=terminator) in its last digit.
; Inputs:
;   [X+*]: Operand A in zeropage.
;   (t0,t1): Operand B address.
; Outputs:
;   [X+*]: Result in zeropage.
; Modifies:
;   A,X,Y
;--
nk_bcd_add_term:
    LDY     #0
    CLC
@add_loop:
    LDA     (t0), Y
    BPL     @no_term
    TAY                     ; Store NK_BCD_TERM ->Y
    AND     #~NK_BCD_TERM   ; Mask out terminator.
@no_term:
    ADC     0, X
    CMP     #10
    BCC     @less_than_10
    SBC     #10             ; C=1 before and after.
@less_than_10:
    STA     0, X
    INX
    INY
    BPL     @add_loop       ; Check term in
    LDA     #0
    BCS     @no_term        ; Do carried digits if C=1.
    RTS
.ENDIF

;--
; Compare two BCD numbers.
; The numbers must be properly aligned and have the same number of digits.
; Digits are compared until a non-equal pair is found. The resulf of this
; comparison is returned as result = A - B (same as CMP).
; Inputs:
;   Y: number of digits (bytes) to compare
;   (t0,t1): Operand A.
;   [X+*]: Operand B.
; Outputs:
;   C: 1: A>=B, 0: A<B
;   Z: 1: A==B, 0: A!=B
; Modifies:
;
; Illustration of operation:
;
;   Digit byte index: 3 2 1 0
;   -------------------------
;   A operand:        2 7 9 3
;   B operand:        2 7 4 4
;   Carry flag:       1 1 1 -
;   Zero flag:        1 1 0 -
;                         ^
;                         \- First non-equal digit.
;--
nk_bcd_cmp:
    STX     t2
    TYA
    CLC
    ADC     t2
    TAX
@equals_loop:
    DEX
    DEY
    BPL     @next_digit
;@all_digits_equal:
    INY                     ; Make sure Z=1, C=1 is last state.
    SEC
    RTS
@next_digit:
    LDA     (t0), Y
    CMP     0, X
    BEQ     @equals_loop
    RTS                     ; Digits not equal, return carry flag.

.ENDMACRO ; _NK_BCD_CODE


.MACRO _NK_BCD_INIT
.ENDMACRO ; _NK_BCD_INIT
