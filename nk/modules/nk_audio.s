
NK_USE_AUDIO = 1

NK_AUDIO_PLAYER_SFX = 0
NK_AUDIO_PLAYER_BGM = 1
NK_AUDIO_NUM_PLAYERS = 2  ; Song part players
NK_AUDIO_NUM_WAVES = 5    ; APU channels
NK_AUDIO_NUM_CHNS = NK_AUDIO_NUM_WAVES * NK_AUDIO_NUM_PLAYERS
NK_AUDIO_NUM_VOL_CHNS = (NK_AUDIO_NUM_WAVES-2) * NK_AUDIO_NUM_PLAYERS

NK_AUDIO_SFX_NO_LOOP = %00000001
NK_AUDIO_BGM_NO_LOOP = %00000010
NK_AUDIO_SFX_PAUSE = %01000000 ; Must be >= 48
NK_AUDIO_BGM_PAUSE = %10000000 ; Must be >= 48

.MACRO NK_AUDIO_SET_FLAGS flags_
    LDA   v_nk_audio_flags
    ORA   #flags_
    STA   v_nk_audio_flags
.ENDMACRO

.MACRO NK_AUDIO_UNSET_FLAGS flags_
    LDA   v_nk_audio_flags
    AND   #~flags_
    STA   v_nk_audio_flags
.ENDMACRO

.MACRO NK_AUDIO_PLAY player_, song_part_idx_
    LDX   #(song_part_idx_)
    LDY   #player_
    JSR   nk_audio_play
.ENDMACRO ; NK_AUDIO_PLAY

.MACRO NK_AUDIO_UPDATE
    JSR   _nk_audio_update
.ENDMACRO

.MACRO _NK_AUDIO_ZEROPAGE
z_nk_audio_start:
z_nk_audio_ptr:       .RES 2
; TODO: tsone: zeropage should be used only if absolutely necessary
z_nk_audio_sq_period:
z_nk_audio_sq1_pediod_hi: .RES 1 ; NOTE: must be at z_nk_audio_sq2_period_hi - 4
; TODO: tsone: module for multiply routine(s)
z_nk_audio_mul1_lo: .RES 1
z_nk_audio_mul2_lo: .RES 1
z_nk_audio_mul2_hi: .RES 1
z_nk_audio_sq2_pediod_hi: .RES 1 ; NOTE: must be at z_nk_audio_sq1_period_hi + 4
z_nk_audio_end:
.ENDMACRO ; _NK_AUDIO_ZEROPAGE


.MACRO _NK_AUDIO_BSS
v_nk_audio_start:
v_nk_audio_flags:     .RES 1    ; Bits: 0=don't loop SFX, 1=don't loop BGM
v_nk_audio_chn_idx:   .RES 1    ; Current channel index

; Delay (in frames) until next stream frame is read
v_nk_audio_chn_delay:    .RES NK_AUDIO_NUM_CHNS
; Remaining note length (in frames)
v_nk_audio_chn_len_lo:   .RES NK_AUDIO_NUM_CHNS
v_nk_audio_chn_len_hi:   .RES NK_AUDIO_NUM_CHNS ; bit7=1: Note is released

; Song part pointers
v_nk_audio_part_ptrs:
; Pointers to current stream position
v_nk_audio_chn_ptr_lo:   .RES NK_AUDIO_NUM_CHNS
v_nk_audio_chn_ptr_hi:   .RES NK_AUDIO_NUM_CHNS
; Pointers to current volume stream position
v_nk_audio_chn_vol_ptr_lo: .RES NK_AUDIO_NUM_VOL_CHNS
v_nk_audio_chn_vol_ptr_hi: .RES NK_AUDIO_NUM_VOL_CHNS
; Pointer to instrument length table
v_nk_audio_inst_len_ptr_lo: .RES NK_AUDIO_NUM_PLAYERS
v_nk_audio_inst_len_ptr_hi: .RES NK_AUDIO_NUM_PLAYERS
v_nk_audio_part_ptrs_end:

; Current note period index low: SSSSFFFF: S=subnote, F=fractional (for slopes)
v_nk_audio_chn_pidx_lo: .RES NK_AUDIO_NUM_CHNS
; Current note period index high: ONNNNNNN: O=pidx overflow bit, N=note
v_nk_audio_chn_pidx_hi: .RES NK_AUDIO_NUM_CHNS

; Vibrato position low
v_nk_audio_chn_vib_pos_lo: .RES NK_AUDIO_NUM_CHNS
; Vibrato position high when >= 0, otherwise remaining vibrato delay (in frames)
v_nk_audio_chn_vib_pos_hi: .RES NK_AUDIO_NUM_CHNS

; Volume envelope position low: FFFFFFFF: F=fractional
v_nk_audio_chn_venv_pos_lo: .RES NK_AUDIO_NUM_CHNS
; Volume envelope position high: --PPPPPP: P=position, note is stopped at >=48
v_nk_audio_chn_venv_pos_hi: .RES NK_AUDIO_NUM_CHNS

; Pitch envelope position low: FFFFFFFF: F=fractional
v_nk_audio_chn_penv_pos_lo: .RES NK_AUDIO_NUM_CHNS
; Pitch envelope position high: --PPPPPP: P=position
v_nk_audio_chn_penv_pos_hi: .RES NK_AUDIO_NUM_CHNS

; Arpeggio offset in arpeggio data
v_nk_audio_chn_arp_offs: .RES NK_AUDIO_NUM_CHNS
; Arpeggio delay in frames
v_nk_audio_chn_arp_delay: .RES NK_AUDIO_NUM_CHNS

; Set delay state (in frames) for decoding
v_nk_audio_chn_delay_set: .RES NK_AUDIO_NUM_CHNS
; Set note state (as note value, <128) for decoding
v_nk_audio_chn_note_set:  .RES NK_AUDIO_NUM_CHNS
; Set instrument (as index)
v_nk_audio_chn_inst_set:  .RES NK_AUDIO_NUM_CHNS
; Set slope low: SSSSFFFF: S=subnote, F=fractional (as period index delta)
v_nk_audio_chn_slope_lo: .RES NK_AUDIO_NUM_CHNS
; Set slope high: INNNNNNN: I=slope sign bit, N=note (as period index delta)
v_nk_audio_chn_slope_hi: .RES NK_AUDIO_NUM_CHNS

; Channel volume stream delay
v_nk_audio_chn_vol_delay_lo: .RES NK_AUDIO_NUM_VOL_CHNS
v_nk_audio_chn_vol_delay_hi: .RES NK_AUDIO_NUM_VOL_CHNS
; Current channel volume
v_nk_audio_chn_vol_set:    .RES NK_AUDIO_NUM_VOL_CHNS

; Set note length override (for this frame only)
v_nk_audio_wave_idx:
v_nk_audio_len_override_lo: .RES 1 ; TODO: tsone: temp, can be probably optimized out
v_nk_audio_modifier:
v_nk_audio_len_override_hi: .RES 1
v_nk_audio_vib_pos_override: .RES 1 ; TODO: tsone: temp, can be probably optimized out
v_nk_audio_venv_pos_override: .RES 1
v_nk_audio_penv_pos_override: .RES 1
; Note retrig flag for current channel: $01=trig, $00=no trig
v_nk_audio_retrig:       .RES 1 ; TODO: tsone: temp, can be probably optimized out
v_nk_audio_end:
.ENDMACRO ; _NK_AUDIO_BSS


.MACRO _NK_AUDIO_CODE
.STAT nk_audio
.INCLUDE    "modules/nk_audio_code.s"
.ENDSTAT
.ENDMACRO ; _NK_AUDIO_CODE


.MACRO _NK_AUDIO_INIT
    JSR     nk_audio_init
.ENDMACRO ; _NK_AUDIO_INIT
