
NK_USE_SKIN = 1
.IFNDEF NK_USE_PATT
    .INCLUDE "modules/nk_patt.s"
.ENDIF

; Number of visible skins on screen.
NK_SKIN_NUM            = 8
; Counter is stored in bits 2..7.
NK_SKIN_FC_MASK        = %11111110
; Constant for decrementing frame counter.
NK_SKIN_FC_DECR        = %00000010
; Skin invisible bit.
NK_SKIN_FC_INVISIBLE   = %00000001
; Stop animation is the counter maximum.
NK_SKIN_FC_STOP        = NK_SKIN_FC_MASK
NK_SKIN_COORD_BITS     = 6
NK_SKIN_CHANGE_FORCE   = $80    ; Flag to force anim change.
; Special anim offs indicating an empty frame without sprite(s).
NK_SKIN_NO_SPRITES_IN_FRAME = $00

;--
; Pause skin animation.
; Inputs:
;   X: Skin index.
; Modifies:
;   A
;--
.MACRO NK_SKIN_ANIM_PAUSE
    LDA     v_nk_skin_fc, X
    ORA     #NK_SKIN_FC_STOP
    STA     v_nk_skin_fc, X
.ENDMACRO

;--
; Reset and hold the first animation frame for some video frames.
; Skin is set visible if it's invisible.
; Inputs:
;   num_frames_: Number of frames to hold.
;   X: Skin index.
; Modifies:
;   A
;--
.MACRO NK_SKIN_ANIM_HOLD num_frames_
    LDA     #num_frames_<<1
    STA     v_nk_skin_fc, X
    LDA     #0
    STA     v_nk_skin_anim_offs, X
.ENDMACRO


.MACRO _NK_SKIN_ZEROPAGE
.ENDMACRO ; _NK_SKIN_ZEROPAGE


.MACRO _NK_SKIN_BSS
;--
; Skin is an animated screen entity. It composes of multiple PPU sprites
; that are sorted by Y coordinate and drawn.
;--
v_nk_skin_sort_idx:        .RES NK_SKIN_NUM    ; Sorted skin idxs for correct drawing.
v_nk_skin_x:               .RES NK_SKIN_NUM    ; Skin x-coordinate on screen.
v_nk_skin_y:               .RES NK_SKIN_NUM    ; Skin y-coordinate on screen.
v_nk_skin_fc:              .RES NK_SKIN_NUM    ; Skin frame counter.
v_nk_skin_patt:            .RES NK_SKIN_NUM    ; Skin patt or skin index (in skin table).
v_nk_skin_anim:            .RES NK_SKIN_NUM    ; Anim index in skin's anim table.
v_nk_skin_anim_offs:       .RES NK_SKIN_NUM    ; Frame (byte) offset in the anim data.
v_nk_skin_count:           .RES 1              ; Number of skins on screen right now.

; TODO: move to elsewhere? animation specific
v_nk_spr_idx_top_prev:     .RES 1  ; Sprite index top from prev frame.
v_nk_spr_idx_top_curr:     .RES 1  ; Current sprite index top.
.ENDMACRO ; _NK_SKIN_BSS


.MACRO _NK_SKIN_CODE
;--
; Sorts all skins. Sorts v_nk_skin_sort_idx in decrementing order by skin y.
; Uses bubble sort with one iteration per frame (=NK_SKIN_NUM-1 cmps/swaps).
; This means correct ordering takes at max NK_SKIN_NUM-1 frames.
; Inputs:
;   -
; Modifies:
;   A,X,Y
;--
nk_skin_sort_all:
    LDY     #NK_SKIN_NUM - 2
@sort_loop:
    LDX     v_nk_skin_sort_idx, Y     ; if (y[sort_idx[Y]] < y[sort_idx[Y+1]]) {
    LDA     v_nk_skin_y, X            ;   swap(Y,Y+1);
    LDX     v_nk_skin_sort_idx+1, Y   ; }
    CMP     v_nk_skin_y, X
    BCS     @skip_swap
;@swap:
    LDA     v_nk_skin_sort_idx, Y      ; Swap the indexes.
    STA     v_nk_skin_sort_idx+1, Y
    TXA
    STA     v_nk_skin_sort_idx, Y
@skip_swap:
    DEY
    BPL     @sort_loop
    RTS

;--
; Update all skins.
; Modifies:
;   A,X,Y,t0,t1,t2,t3,t4,t5
;--
nk_skin_update_all:
    LDY     #0
@update_loop:
    TYA
    PHA
    LDX     v_nk_skin_sort_idx, Y
    JSR     nk_skin_update
    PLA
    TAY
    INY
    CPY     #NK_SKIN_NUM - 1
    BNE     @update_loop
    RTS

;--
; Description of animation data layout.
; An animation is composed of multiple frames where
; each is composed of multiple sprites (8x16 px). There is a frame
; header table storing an offset to instead of full pointer.
;
; Frame header data (16 bits, 2 bytes):
;   OOOOOOOO QDDDDDDD
;   O: Offset to first sprite in frame.
;   D: Frame delay (in frames) divided by 2.
;   Q: Last frame in anim?
;
; Frame sprite(s) data (24 bits, 3 bytes, 8x16 px):
;   VHYYYYYY XXXXXXPP QIIIIIII
;   V: Vertical flip? (1 bit)
;   H: Horizontal flip? (1 bit)
;   Y: Sprite Y coordinate. (6 bits)
;   X: Sprite X coordinate. (6 bits)
;   P: Sprite palette index. (2 bits)
;   Q: Terminator bit, is last sprite? (1 bit)
;   I: Chr index (based on VRAM $0100) (7 bits)
;--
;--
; Update skin.
; Inputs:
;   X: Index of skin to update.
; Returns:
;   X: New PPU sprite index (byte offset).
; Modifies:
;   A,X,Y,t0,t1,t2,t3
;--
nk_skin_update:
    LDA     v_nk_skin_x, X          ; Load skin x and y to temps.
    STA     t0
    LDA     v_nk_skin_y, X
    STA     t1
    BNE     @continue               ; Exit if skin y == 0.
@exit:
    RTS
@continue:

    ; Load PPU tile offs per skin patt idx.
    LDY     v_nk_skin_patt, X
    LDA     v_nk_patt_load_offs, Y
    LSR     A
    STA     t3
    TYA
    ; Load skin ptr to t4,t5.
    ASL     A
    TAY
    LDA     c_gen_patt_skin_tab, Y
    STA     t4
    LDA     c_gen_patt_skin_tab+1, Y
    STA     t4+1
    ; Load anim ptr to t4,t5.
    LDA     v_nk_skin_anim, X
    ASL     A
    TAY
    LDA     (t4), Y
    INY
    PHA
    LDA     (t4), Y
    STA     t4+1
    PLA
    STA     t4

    LDA     v_nk_skin_fc, X
    LDY     v_nk_skin_anim_offs, X
    JSR     nk_anim_update
    STA     v_nk_skin_fc, X
    TYA
    STA     v_nk_skin_anim_offs, X

    LDA     #NK_SKIN_FC_INVISIBLE   ; Don't draw if skin is invisible.
    AND     v_nk_skin_fc, X
    BNE     @exit                   ; Exit if invisible.

    LDA     (t4), Y                 ; Load anim sprite(s) offset.
    BEQ     @exit                   ; Frame has no sprite(s) if offs == $00
    TAY
    ;->

;--
; Draw an animation frame.
; Inputs:
;   t0,t1: (x,y) coordinate where to draw frame.
;   t3: Offset for sprite character tile.
;   t4,t5: Pointer to animation to use.
;   Y: Byte offset of anim frame sprite(s) (from anim pointer).
; Modifies:
;   A,X,Y,t0,t1,t2
;--
nk_anim_frame_draw:
    ; Calculate offseted skin pos: (t0,t1) = (x,y) - offset.
    LDX     #1
@init_coords_loop:
    LDA     t0, X
    SEC
    SBC     #(1 << NK_SKIN_COORD_BITS) / 2 ; =min coord value.
    STA     t0, X
    DEX
    BPL     @init_coords_loop

    LDX     v_nk_spr_idx_top_curr      ; Load current sprite idx top ->X

@block_loop:
    LDA     (t4), Y                 ; (nk_skin_frame_ptr), Y
    INY
    PHA                             ; Push param with y-coord.
    AND     #%11000000
    STA     t2                      ; Masked sprite attributes ->t2
    PLA                             ; Pop param with y-coord (b0..5).
    AND     #%00111111
    CLC                             ; C=?
    ADC     t1                      ; Calculate sprite y-coord.
    STA     NK_SPR_BASE+0, X        ; Store sprite y-coord.

    LDA     (t4), Y                 ; (nk_skin_frame_ptr), Y
    INY
    PHA                             ; Push param with x-coord.
    AND     #%00000011
    ORA     t2                      ; Combine sprite attributes.
    STA     NK_SPR_BASE+2, X        ; Store sprite attributes.
    PLA                             ; Pop param with x-coord (b2..7).
    LSR     A
    LSR     A
    CLC                             ; C=?
    ADC     t0                      ; Calculate sprite x-coord.
    STA     NK_SPR_BASE+3, X        ; Store sprite x-coord.

    LDA     (t4), Y                 ; (nk_skin_frame_ptr), Y
    INY
    CLC                             ; C=?
    ADC     t3                      ; Calculate sprite character index.
    ASL     A                       ; b7 is end bit => end loop if C=1.
    ORA     #%00000001              ; Set bit 0 to address VRAM $0100.
    STA     NK_SPR_BASE+1, X        ; Store sprite character index.

    INX
    INX
    INX
    INX
    BCC     @block_loop

    STX     v_nk_spr_idx_top_curr      ; store spr idx for next draw

    RTS

;--
; Update animation frame.
; Inputs:
;   A: Animation frame counter.
;   t4,t5: Pointer to animation.
;   Y: Current frame offset.
; Returns:
;   A: New animation frame counter.
;   Y: New frame offset.
; Modifies:
;   A,Y
;--
nk_anim_update:
    CMP     #NK_SKIN_FC_STOP - 1    ; Check if animation is stopped.
    BCC     @continue_not_stopped
@exit:
    RTS                             ; Animation was stopped.

@continue_not_stopped:
    SEC                             ; Decrement fc by NK_SKIN_FC_DECR.
    SBC     #NK_SKIN_FC_DECR        ; SBC is used to return new value for fc.
    BCS     @exit                   ; Decremented but frame didn't change.

    INY                             ; Change frame to the next.
    LDA     (t4), Y                 ; Bit 7 == 1 (in fc data) if frame is last.
    BPL     @not_last
;@last_frame:
    LDY     #$FF                    ; Is last frame. Reset by loading -1 to Y.
@not_last:
    INY                             ; Inrement to next frame (fc data).
    INY
    LDA     (t4), Y                 ; Load new fc data.
    DEY                             ; Correct frame data offset (-1) ->Y
; TODO: Hacky reference to fc here (not otherwise in the routine).
    LSR     v_nk_skin_fc, X         ; Maintain fc invisiblity bit.
    ROL     A                       ; Clear MSB, roll in visibility bit.
    RTS

;--
; Hide extra sprites not used during current frame/NMI. Call at end of NMI.
; Modifies:
;   A,X,t0
;--
nk_anim_hide_unused:
    LDA     v_nk_spr_idx_top_prev
    STA     t0                      ; t0 = prev
    LDX     v_nk_spr_idx_top_curr      ; X = curr
    STX     v_nk_spr_idx_top_prev      ; prev = curr
    LDA     #0
    STA     v_nk_spr_idx_top_curr      ; curr = 0

    LDA     #$FE                    ; Y-coordinate for hiding sprites.
@hide_sprites_loop:
    CPX     t0                      ; Compare: curr - prev
    BCS     @end                    ; End if curr >= prev.
    STA     NK_SPR_BASE, X           ; Set y-coordinate of sprite to hide.
    INX
    INX
    INX
    INX
    BNE     @hide_sprites_loop      ; Bxx instead of JMP (smaller).

@end:
    RTS

;--
; Change skin animation.
; Inputs:
;   A: Anim to change to. Use NK_SKIN_CHANGE_FORCE to force.
;   X: Index of skin.
; Modifies:
;   C,A
;--
nk_skin_change_anim:
    CMP     v_nk_skin_anim, X
    BEQ     @end                    ; Don't change if param is different.
    AND     #~NK_SKIN_CHANGE_FORCE  ; Clear force bit (bit 7).
    STA     v_nk_skin_anim, X
    LDA     #0                      ; reset anim offset and frame counter
    STA     v_nk_skin_anim_offs, X

    LDA     v_nk_skin_fc, X         ; Clear bits except invisible bit.
    AND     #NK_SKIN_FC_INVISIBLE
    STA     v_nk_skin_fc, X
@end:
    RTS

.ENDMACRO ; _NK_SKIN_CODE


.MACRO _NK_SKIN_INIT
    LDX     #NK_SKIN_NUM - 1
@init_loop:
    LDA     #0
    STA     v_nk_skin_y, X
    STA     v_nk_skin_anim_offs, X
    LDA     #NK_SKIN_FC_STOP
    STA     v_nk_skin_fc, X
    TXA
    STA     v_nk_skin_sort_idx, X
    DEX
    BPL     @init_loop

    ; TODO: these init anim subsystem
    LDA     #0
    STA     v_nk_spr_idx_top_curr
    STA     v_nk_spr_idx_top_prev
.ENDMACRO ; _NK_SKIN_INIT
