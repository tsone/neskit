
NK_USE_YALZ = 1


;--
; Decode YALZ to PPU. Sets address at t0,t1.
; Modifies:
;   A,X,Y,t0,t1,t2,t3,t4
;--
.MACRO NK_YALZ_DECODE   dst_ppu, src_addr
    NK_PTR_SET  src_addr
    NK_YALZ_DECODE_CONTINUE dst_ppu
.ENDMACRO


;--
; Continue decode YALZ to PPU from address in t0,t1.
; NOTE! You must set address in Y,t1.
; Modifies:
;   A,X,Y,t0,t1,t2,t3,t4
;--
.MACRO NK_YALZ_DECODE_CONTINUE  dst_ppu
    NK_PPU_ORG  dst_ppu
    JSR     nk_yalz_decode_ppu
.ENDMACRO


.MACRO _NK_YALZ_ZEROPAGE
.ENDMACRO


.MACRO _NK_YALZ_BSS
; State for nk_yalz_decode_start and nk_yalz_decode_continue:
v_nk_yalz_flags:        .RES 1
v_nk_yalz_lookback_idx: .RES 1
v_nk_yalz_copy_idx:     .RES 1
v_nk_yalz_copy_end:     .RES 1
v_nk_yalz_in_literal:   .RES 1 ; 1=in literal, 0=in copy (loop).
.ENDMACRO

.MACRO _NK_YALZ_PUT_BYTE_PPU
    STA     $2007
    STA     NK_YALZ_PAGE, X
    INX
.ENDMACRO

.MACRO _NK_YALZ_CODE
;--
; Decode YALZ-compressed data, port for NES / Famicom (CA65 assembler).
; Data is decoded directly to Picture Processing Unit (PPU) memory.
; NOTE: Use NK_PPU_ORG to set destination address in PPU memory.
; Inputs:
;   Y: Source address, low byte
;   t1: Source address, high byte
; Modifies:
;   A,X,Y,t0,t1,t2,t3,t4
;--
nk_yalz_decode_ppu:
    LDX     #0                  ; X: lookback buffer index
    STX     t0                  ; 0 -> t0

    ; peek first flags byte to see if data is yalz or raw
    NK_PTR_PEEK
    LSR     A                   ; bit0 == 1 means raw
    BCC     @flags_loop

    ; prepare for raw ppu_copy
    STA     t3                  ; data size, high byte
    NK_PTR_INC                  ; skip first byte (already read)
    NK_PTR_READ
    STA     t2                  ; data size, low byte
    JMP     nk_ppu_copy

@flags_loop:
    NK_PTR_READ
    SEC                         ; Flags and terminator bit 7 -> t4
    ROR
    STA     t4
    BCS     @copy               ; Copy or literal?

@literal:
    NK_PTR_READ
    ;CLC                        ; C=0 always
    ADC     z_nk_decode_incr    ; Add decode increment to result
    _NK_YALZ_PUT_BYTE_PPU

@flags_loop_check:
    LSR     t4                  ; Frame, copy or literal?
    BEQ     @flags_loop
    BCC     @literal

@copy:
    NK_PTR_READ                 ; get end_offs
    STA     t2                  ; end_offs -> t2
    NK_PTR_READ                 ; start_offs -> A
    CMP     t2                  ; if start_offs == end_offs, we're done
    BEQ     @end                ; yes, exit decode

;@copy_start:
    STY     t3                  ; store ptr lo, Y -> t3
    TAY                         ; start_offs -> Y
@copy_loop:
    LDA     NK_YALZ_PAGE, Y     ; get byte from previously decoded
    INY
    _NK_YALZ_PUT_BYTE_PPU
    CPY     t2                  ; Is Y == end_offs?
    BNE     @copy_loop          ; If not, continue copying.
;@copy_end:
    LDY     t3                  ; If yes, restore ptr lo in t3 -> Y
    BCS     @flags_loop_check   ; JMP optimization (C=1 always due CPY above)

@end:
    RTS

;--
; Decode given number of bytes of YALZ-compressed data to NK_YALZ_PAGE.
; Important differences to nk_yalz_decode_ppu:
; - Source data MUST be compressed (can't be raw data).
; - Bytes are decoded to NK_YALZ_PAGE where they can be read after.
; - Decoded bytes are not offset by z_nk_decode_incr.
; Inputs:
;   A: Desired number bytes to decode (0 means 256 bytes)
;   Y: Source address, low byte
;   t1: Source address, high byte
; Outputs:
;   Y: Advanced source address, low byte.
;   t1: Advanced source address, high byte.
;   C: 0=no more data, 1=more data.
; Modifies:
;   A,X,Y,t0,t1,t3,t4
nk_yalz_decode_start:
    STA     t4
    LDX     #0
    STX     t0                  ; X, 0 -> t0

_nk_yalz_dec_flags_loop:
    NK_PTR_READ
    SEC                         ; Flags and terminator bit 7 -> t4
    ROR
    STA     v_nk_yalz_flags
    BCS     _nk_yalz_dec_copy   ; Copy or literal?

_nk_yalz_dec_literal:
    NK_PTR_READ
    STA     NK_YALZ_PAGE, X
    INX
    CPX     t4
    BEQ     _nk_yalz_dec_stop_in_literal

_nk_yalz_dec_flags_loop_end:
    LSR     v_nk_yalz_flags     ; Frame, copy or literal?
    BEQ     _nk_yalz_dec_flags_loop
    BCC     _nk_yalz_dec_literal

_nk_yalz_dec_copy:
    NK_PTR_READ                 ; get end_offs
    STA     v_nk_yalz_copy_end
    NK_PTR_READ                 ; start_offs -> A
    CMP     v_nk_yalz_copy_end  ; if start_offs == end_offs, we're done
    BEQ     _nk_yalz_dec_end    ; yes, exit decode

;_nk_yalz_dec_copy_start:
    STY     t3                  ; store ptr lo, Y -> t3
    TAY                         ; start_offs -> Y
_nk_yalz_dec_copy_loop:
    LDA     NK_YALZ_PAGE, Y     ; get byte from previously decoded
    INY
    STA     NK_YALZ_PAGE, X
    INX
    CPX     t4
    BEQ     _nk_yalz_dec_stop_in_copy
_nk_yalz_dec_copy_continue:
    CPY     v_nk_yalz_copy_end  ; is Y == end_offs?
    BNE     _nk_yalz_dec_copy_loop ; no, continue copying
;_nk_yalz_dec_copy_end:
    LDY     t3                  ; yes, restore ptr lo, t3 -> Y
    BCS     _nk_yalz_dec_flags_loop_end ; JMP optimization (C=1 always due CPY above)

_nk_yalz_dec_stop_in_copy:
    STY     v_nk_yalz_copy_idx  ; current idx -> copy_idx
    LDY     t3                  ; Restore ptr lo -> Y
    CLC

_nk_yalz_dec_stop_in_literal:
    ROL     v_nk_yalz_in_literal ; Mark literal (C=1 always due CMP earlier)
    STX     v_nk_yalz_lookback_idx

    SEC
    RTS

_nk_yalz_dec_end:
    CLC
    RTS

nk_yalz_decode_continue:
    CLC
    ADC     v_nk_yalz_lookback_idx
    STA     t4
    LDA     #0
    STA     t0
    LDX     v_nk_yalz_lookback_idx

    LSR     v_nk_yalz_in_literal
    BCS     _nk_yalz_dec_flags_loop_end
;@continue_copy:
    STY     t3                  ; ptr lo -> t3
    LDY     v_nk_yalz_copy_idx  ; current idx -> Y
    JMP     _nk_yalz_dec_copy_continue

.ENDMACRO


.MACRO _NK_YALZ_INIT
.ENDMACRO
