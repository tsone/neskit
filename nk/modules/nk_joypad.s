
NK_USE_JOYPAD = 1

NK_JOYPAD_A         = %10000000
NK_JOYPAD_B         = %01000000
NK_JOYPAD_SELECT    = %00100000
NK_JOYPAD_START     = %00010000
NK_JOYPAD_UP        = %00001000
NK_JOYPAD_DOWN      = %00000100
NK_JOYPAD_LEFT      = %00000010
NK_JOYPAD_RIGHT     = %00000001

NK_DIR_UP       = 0
NK_DIR_DOWN     = 1
NK_DIR_LEFT     = 2
NK_DIR_RIGHT    = 3
NK_DIR_NONE     = $FF
NK_DIR_HORZ_BIT = %00000010


;--
; Check if direction is vertical (=along the Y axis).
; Will return C=1 for DIR_UP and DIR_DOWN.
; Inputs:
;   A: Direction.
; Modifies:
;   C
;--
.MACRO  NK_DIR_IS_VERTICAL
    CMP     #NK_DIR_LEFT
.ENDMACRO

;--
; Check if direction is positive along the X or Y axis.
; NOTE: Modifies contents of A.
; Will return C=1 for DIR_DOWN and DIR_RIGHT.
; Inputs:
;   A: Direction.
; Modifies:
;   C,A
;--
.MACRO  NK_DIR_IS_POSITIVE
    LSR     A
.ENDMACRO

;--
; Read input from single joypad. The routine is reasonably DPCM-safe.
; The routine is adapted from code by Blargg:
; http://forums.nesdev.com/viewtopic.php?t=4124
; Inputs:
;   joypad_idx_: 0=joypad1, 1=joypad2.
; Modifies:
;   C,A,X,Y,t0,t1,t2
;--
.MACRO NK_JOYPAD_READ joypad_idx_
    LDA     v_nk_joypad_input+joypad_idx_ ; Store previous state.
    STA     v_nk_joypad_previnput+joypad_idx_

    LDY     #1                      ; Init for strobe and loops.
    LDX     #0

    STY     $4016                   ; Strobe joypad (1st time).
    STX     $4016
    STY     t0                      ; Set bit0 sentinel.
@_joypad_read_bits1\@:              ; Read joypad bits.
    LDA     $4016+joypad_idx_
    AND     #%00000011              ; Merge bits for FC.
    CMP     #1
    ROL     t0
    BCC     @_joypad_read_bits1\@

    STY     $4016                   ; Strobe joypad (2nd time).
    STX     $4016
    STY     t1
@_joypad_read_bits2_\@:
    LDA     $4016+joypad_idx_
    AND     #%00000011
    CMP     #1
    ROL     t1
    BCC     @_joypad_read_bits2_\@

    STY     $4016                   ; Strobe joypad (3rd time).
    STX     $4016
    STY     t2
@_joypad_read_bits3_\@:
    LDA     $4016+joypad_idx_
    AND     #%00000011
    CMP     #1
    ROL     t2
    BCC     @_joypad_read_bits3_\@
    
    ; Compare read input bits. Key: X=correct input bits, -=incorrect.
    LDA     t0
    CMP     t2          ;X-X
    BEQ     @_joypad_match\@
    LDA     t1          ;XX-, -XX, -X-
@_joypad_match\@:
    STA     v_nk_joypad_input+joypad_idx_   ; Store result as new input.
    
    EOR     v_nk_joypad_previnput+joypad_idx_ ; Get pressed buttons.
    PHA
    AND     v_nk_joypad_input+joypad_idx_
    STA     v_nk_joypad_pressed+joypad_idx_
    
    PLA                                     ; Get released buttons.
    AND     v_nk_joypad_previnput+joypad_idx_
    STA     v_nk_joypad_released+joypad_idx_
.ENDMACRO


; TODO: Do something with this unsafe read? DPCM conflicts with this.
.IF 0
;--
; Reads input from two joypads.
; Modifies:
;   C,A,X,Y
;--
.MACRO NK_JOYPAD_READ
    LDY     #1                      ; Strobe joypads.
    STY     $4016
    LDX     #0
    STX     $4016
    INX                             ; X=1, Y=1
@_joypad_read\@:
    LDA     v_nk_joypad_input, X     ; Store previous state.
    STA     v_nk_joypad_previnput, X

    TYA                             ; Prepare for loop (set bit0).
    STA     v_nk_joypad_input, X
@_joypad_read_buttons\@:            ; Read loop.
    LDA     $4016, X
    LSR     A
    ROL     v_nk_joypad_input, X
    BCC     @_joypad_read_buttons\@
    
    LDA     v_nk_joypad_input, X     ; Get pressed buttons.
    EOR     v_nk_joypad_previnput, X
    PHA
    AND     v_nk_joypad_input, X
    STA     v_nk_joypad_pressed, X
    
    PLA                             ; Get released buttons.
    AND     v_nk_joypad_previnput, X
    STA     v_nk_joypad_released, X
    
    DEX                             ; Repeat for joypad 1.
    BPL     @_joypad_read\@
.ENDMACRO
.ENDIF


.MACRO _NK_JOYPAD_ZEROPAGE
.ENDMACRO


.MACRO _NK_JOYPAD_BSS
v_nk_joypad_input:       .RES 2 ; Current joypads state.
v_nk_joypad_previnput:   .RES 2 ; Joypad state at previous read.
v_nk_joypad_pressed:     .RES 2 ; Pressed buttons since previous read.
v_nk_joypad_released:    .RES 2 ; Released buttons since previous read.
.ENDMACRO


.MACRO _NK_JOYPAD_CODE
;--
; Extract dpad direction from input state bits.
; Inputs:
;   A: Joypad state bits (bits 0-3 contain the direction).
; Outputs:
;   X: The direction, or NK_DIR_NONE if none found (N=1).
; Modifies:
;   C,A,X
;--
nk_joypad_get_dir:
    LDX     #NK_DIR_RIGHT
@shift_loop:
    LSR     A                   ; Shift joypad bits right and check carry.
    BCS     @found              ; Terminate this loop if set bit is found.
    DEX
    BPL     @shift_loop
@found:
    RTS
.ENDMACRO


.MACRO _NK_JOYPAD_INIT
.ENDMACRO
