
# nk

nk is a homebrew game programming framework for NES/FC, written in 6502
assembly. It provides many advanced features for developers (see
the MODULES section). nk's module-based design allows to only include the
necessary functionality for a specific game.

Please note the following before using nk:

- **nk framework is a work-in-progress or beta. There be dragons.**
- Supports iNES mappers:
  - #0 NROM.
  - #2 UxROM.
  - #4 MMC3.
  - #119 TxROM (MMC3 with CHR-RAM and CHR-ROM).
- Only supports nkasm assembler. May build on ca65 assembler with
  some modifications.
- Not tested on FC and famiclone hardware.


## Features

nk offers the following basic features before including any modules:

- Provides helpful entrypoints to game code: RESET, UPDATE and VBLANK.
- Proper background scrolling routines.
- Automatic palette updates at VBLANK (can be used for palette effects).
- Pascal-style strings drawing.
- PPU state/register management.
- Automatic sprite DMA at VBLANK.

Modules add advanced funtionality on top of these basic features. These are
described in MODULES section.


## Entrypoints

nk provides convenient entrypoint to your game code, handling the
hardware interrupts behind the scenes. In order to use nk you must define
the following three entrypoints (as labels) in you program:

**NOTE:** All of the entrypoints must end with RTS instruction.

### RESET

Called after a HW RESET interrupt has been handled by nk. Perform the game
initialization here. PPU and NMI are turned off while RESET is called, so
that the initialization can take any time necessary. By default, UPDATE and
VBLANK entrypoints will not be called until RESET returns.

### VBLANK

Called at VBLANK period after nk has performed internal NMI/VBLANK
update tasks. User should use VBLANK callback to perform custom PPU
manipulation, usually background tile manipulation, animations or other
graphics updates.

### UPDATE

Called after all VBLANK code has been fully handled. Add all game logic here
(input handling, sound effects, sprite/skin changes etc.)
**NOTE:** If the UPDATE code takes longer than one frame (i.e. ~20 ms on PAL
or ~16.7 ms on NTSC), it will be interrupted by a call to VBLANK entrypoint.
In this case nk stores registers to resume UPDATE after VBLANK has returned.
One can think VBLANK and UPDATE as two threads/tasks where VBLANK has the
higher priority.

### IRQ

Required with MMC3 mapper #4. Called at MMC3 scanline interrupt (IRQ).
For accurate synchronization, use NK_IRQ_START in VBLANK to set scanline
count after which IRQ will be called. **NOTE:** Rendering must be enabled
for the IRQ to trigger (NK_MASK_CONFIG must have NK_MASK_SPR_SHOW and/or
NK_MASK_BG_SHOW). Also pay attention that
[scanline counter behaves unpredictably with 8x16 sprites][IRQ].


## Modules

Modules add advanced functionality in forms of routines, macros and
variables. Modules are added with the .INCLUDE command (see USAGE section).
Modules typically occupy some PRG-ROM and memory (due to introduced
subroutines and variables). The amount varies for each module.

Here is a list of modules and their description:

### bcd

Routines for binary coded decimal operations.

### joypad

Routines for reading joypad input. Get pressed and released buttons
since previous input read. Convert joypad button state to directions.

### misc

Miscellaneous routines that do not match other categories.

### patt

In nk, a patt (short for pattern) is a multi-tile graphics resource.
The patt module handles management and loading of patts. Patts can be loaded
intelligently to free pattern memory in the PPU, allowing tile graphics to be
dynamically located. The module also provides tile offset of loaded patterns
for name table loading and skin animation.

### random

Pseudo-random number generation routines.

### skin

In nk, skin is a collection of multi-sprite animations resource. For example,
a all game character graphics and animations can be stored as a single skin.
The skins can be also sorted by y-coordinate when this is desired (for depth
effect). The skin module contains routines for displaying and managing skins.

### audio

Play background music (BGM) or sound effects (SFX) created and exported in
[nemus][nm]. Basic workflow is to export a song as assembly in nemus, and
include it in the game code. (All game BGM and SFX song parts are intended to
be in a single nemus song file exported to assembly.)

**NOTE:** Including nk audio without also including exported song data will
cause an assembler error. This is because nk audio hooks directly with the song
data at source code level, i.e. assembly labels to the data must exist.

### w

16-bit word number routines.

### yalz

Decompressor for a custom YALZ Lempel-Ziv compression format. The format
is quite similar to [LZSS][LZSS].


## Usage

First add modules .INCLUDE commands and then the main nk include file (nk.s):

    .INCLUDE "modules/nk_joypad.s"
    .INCLUDE "nk.s"

Then add your entrypoints:

    RESET:
        ; Called after power or reset button has been pressed.
        ; Reset the game state here.
        RTS

    UPDATE:
        ; Called after VBLANK period has been handled.
        ; Update the game logic here.
        RTS

    VBLANK:
        ; Called at the start of VBLANK (after nk internal VBLANK code).
        ; Modify contents of VRAM/PPU memory here (if needed).
        RTS

    ; Following IRQ is only required with iNES mappers #4, $119.
    .IF (__MAPPER__ == 4 || __MAPPER__ == 119)
    IRQ:
        ; Called at MMC3 scanline counter interrupt (IRQ).
        ; Perform synchronized scrolling or similar.
        RTS
    .ENDIF

Some modules may need you to specify configuration constants. These should be
set before any .INCLUDE commands to nk files. You can see these in the
beginning of nk.s file under "Configuration" section. Look for ".IFNDEF XYZ".
Here XYZ is a guard for the configuration constants you should set.


[LZSS]: http://en.wikipedia.org/wiki/LZSS
[nm]: https://bitbucket.org/tsone/nemus/
[IRQ]: https://wiki.nesdev.com/w/index.php/MMC3#IRQ_Specifics


## License

nk is released under MIT license.


```
nk - neskit framework
---------------------

Copyright (c) 2013 Valtteri Heikkila

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
