
;--
; Configuration.
; Note: Do not modify (unless you know what you are doing)!
;--
.IFNDEF NK_CODE_BASE
NK_CODE_BASE = $C000        ; Default code origin in final 16 KB bank.
.ENDIF
.IFNDEF NK_NTSC_FRAMESKIP
NK_NTSC_FRAMESKIP = 0       ; Skip every 6th frame on NTSC system.
.ENDIF
.IFNDEF NK_IRQ_UPDATE
NK_IRQ_UPDATE = 0           ; Call UPDATE at IRQ. Default: at NMI.
.ENDIF
NK_BSS_BASE = $0200         ; Base address of BSS segment.
NK_SPR_BASE = $0700         ; Sprite RAM, align: 256, size: 256.
NK_YALZ_PAGE = $0600        ; YALZ module RAM, align: 256, size: 256.

;--
; Config PPU control (for $2000 register aka PPUCTRL).
;--
NK_CTRL_NMI         = (1<<7)
NK_CTRL_8x16_SPR    = (1<<5)
NK_CTRL_BG_HIGH     = (1<<4)
NK_CTRL_SPR_HIGH    = (1<<3)
NK_CTRL_NONE        = 0
.MACRO NK_CTRL_CONFIG flags
    LDA     #(flags)
    STA     v_nk_ppu_ctrl
.ENDMACRO

;--
; Set PPU display flags (for $2001 register aka PPUMASK).
;--
NK_MASK_TINT_BLUE    = (1<<7)
NK_MASK_TINT_GREEN   = (1<<6)
NK_MASK_TINT_RED     = (1<<5)
NK_MASK_SPR_SHOW     = (1<<4)
NK_MASK_BG_SHOW      = (1<<3)
NK_MASK_SPR_NOCLIP   = (1<<2)
NK_MASK_BG_NOCLIP    = (1<<1)
NK_MASK_MONOCHROME   = (1<<0)
NK_MASK_NONE         = 0
.MACRO NK_MASK_CONFIG flags
    LDA     #(flags)
    STA     v_nk_ppu_mask
.ENDMACRO

; TODO: remove or keep?
.MACRO NEG addr
    LDA     addr
    EOR     #$FF
    STA     addr
    INC     addr
.ENDMACRO

;--
; Negate value of A.
; Inputs:
;   A: Value to negate.
; Outputs:
;   A: Negated value.
; Modifies:
;   C,A
;--
.MACRO NK_NEG
    EOR     #$FF
    CLC
    ADC     #1
.ENDMACRO

;--
; Absolute value of A.
; NOTE: Sing bit (N) must be set for A.
; Inputs:
;   A: To get the absolute.
; Outputs:
;   A: The absolute value.
; Modifies:
;   C,A
;--
.MACRO NK_ABS
    BPL     @skip\@
    NK_NEG
@skip\@:
.ENDMACRO

.MACRO NK_PPU_ORG addr
    LDA     $2002           ; read PPU status to reset the high/low latch
    LDA     #>(addr)
    STA     $2006           ; PPU write address, high 6 bits
    LDA     #<(addr)
    STA     $2006           ; PPU write address, low 8 bits
.ENDMACRO

.MACRO NK_PTR_SET addr
    LDA     #>(addr)
    STA     t1
    LDY     #<(addr)
.ENDMACRO

.MACRO NK_PTR_SET2 addr
    LDA     #>(addr)
    STA     t1
    LDA     #<(addr)
    STA     t0
.ENDMACRO

.MACRO NK_COUNT_SET count
    LDA     #<(count)
    STA     t2
    LDA     #>(count)
    STA     t3
.ENDMACRO

.MACRO NK_PTR_PEEK
    LDA     (t0), Y
.ENDMACRO

.MACRO NK_PTR_INC
    INY
    BNE     @no_hi_inc\@
    INC     t1
@no_hi_inc\@:
.ENDMACRO

.MACRO NK_PTR_READ
    NK_PTR_PEEK
    NK_PTR_INC
.ENDMACRO


;--
; Disables all PPU output immediately.
; When PPU is disabled, one can safely write to PPU VRAM (outside VBLANK).
; The screen will be turned off and background color is displayed.
; NOTE: Also reportedly SPR RAM contents may be lost after some time.
;--
.MACRO NK_PPU_DISABLE
    LDA     #0
    STA     $2001
.ENDMACRO

;--
; Enables PPU with user settings at next VBLANK (not immediately).
;--
.MACRO NK_PPU_ENABLE
    LDA     v_nk_ppu_flags
    ORA     #%01000000      ; Set bit to indicate we want PPU to be enabled.
    STA     v_nk_ppu_flags
.ENDMACRO

;--
; Load both BG and sprite palettes. Input data is (in order):
;   1 byte for zero color,
;   12 bytes for BG palette, and
;   12 bytes for sprite palette.
; Inputs:
;  addr_: Address to palettes.
; Modifies:
;  C,A,X,Y,t0,t1
;--
.MACRO NK_PAL_LOAD addr_
    LDX     #0
@loop\@:
    LDA     addr_, X
    STA     v_nk_pal, X
    INX
    CPX     #1+2*4*3
    BNE     @loop\@
.ENDMACRO

;--
; Set screen scroll X coordinate.
; Inputs:
;  A: Scroll low byte.
;  X: Scroll high byte.
; Modifies:
;  A
;--
.MACRO NK_PPU_SCROLL_SET_X
    STA     v_nk_ppu_scroll_x
    LSR     v_nk_ppu_ctrl
    TXA
    LSR
    ROL     v_nk_ppu_ctrl
.ENDMACRO

;--
; Set screen scroll Y coordinate.
; Inputs:
;  A: Scroll low byte.
;  X: Scroll high byte.
; Modifies:
;  A
;--
.MACRO NK_PPU_SCROLL_SET_Y
    JSR _nk_ppu_scroll_set_y
.ENDMACRO

;--
; Disable NMI. Note that audio routines etc. will not be called when NMI
; is disabled.
;--
.MACRO NK_NMI_DISABLE
    LDA     v_nk_ppu_ctrl
    AND     #$7F
    STA     $2000
.ENDMACRO

;--
; Enable NMI (if enabled in user settings).
;--
.MACRO NK_NMI_ENABLE
    LDA     v_nk_ppu_ctrl
    STA     $2000
.ENDMACRO

;--
; Disable/acknowledge IRQ.
; Modifies:
;   -
;--
.MACRO NK_IRQ_DISABLE
.ASSERT __MAPPER__ != 0 && __MAPPER__ != 2, "IRQ is not supported for iNES mappers #0, #2"
    STA     $E000
.ENDMACRO

;--
; Enable/acknowledge IRQ.
; Modifies:
;   -
;--
.MACRO NK_IRQ_ENABLE
.ASSERT __MAPPER__ != 0 && __MAPPER__ != 2, "IRQ is not supported for iNES mappers #0, #2"
    STA     $E001
.ENDMACRO

;--
; Start IRQ.
; Inputs:
;   A: Scanline count.
; Modifies:
;   -
;--
.MACRO NK_IRQ_START
.ASSERT __MAPPER__ != 0 && __MAPPER__ != 2, "IRQ is not supported for iNES mappers #0, #2"
    NK_IRQ_DISABLE
    STA     $C000           ; Set count (latch).
    STA     $C001           ; Reload count.
    NK_IRQ_ENABLE
.ENDMACRO

;--
; Reset the global frame counter.
; Modifies:
;   A
;--
.MACRO NK_FRAME_COUNTER_RESET
    LDA     #0
    STA     v_nk_fc_lo
    STA     v_nk_fc_hi
.ENDMACRO

;--
; Switch current bank directly (don't track current bank).
; NOTE: Currently only supports iNES mapper #2.
; Inputs:
;   Y: Bank index (0-14).
; Modifies:
;   A
;--
.MACRO NK_BANK_SWITCH_NO_SAVE
.ASSERT __MAPPER__ == 2, "PRG-ROM bank switching with save currently only supported with iNES mapper #2"
.ASSERT __NUM_BANKS__ > 1, "PRG-ROM bank switching requires at least two PRG-ROM banks"
    TYA
    STA     c_nk_bank_tab, Y
.ENDMACRO

;--
; Switch current bank.
; Inputs:
;   slot_idx_: Slot index, indicating the address range for the iNES mapper used:
;     #2:       0=$8000..$BFFF
;     #4, #119: 0=$8000..$9FFF 1=$A000..$BFFF
;   bank_idx_: Bank index (range depends on mapper used).
; Modifies:
;   A
;--
.MACRO NK_BANK_SWITCH slot_idx_, bank_idx_
.ASSERT __MAPPER__ != 0, "PRG-ROM bank switching is not supported by iNES mapper #0"
.ASSERT __NUM_BANKS__ > 0, "PRG-ROM bank switching requires at least two PRG-ROM banks"
.IF __MAPPER__ == 2
.ASSERT slot_idx_ == 0, "PRG-ROM bank switching slot index must be 0 for mapper #2"
    LDA     #bank_idx_
    STA     v_nk_curr_bank
    STA     c_nk_bank_tab+bank_idx_
.ELSE
.ASSERT slot_idx_ >= 0 && slot_idx_ < 2, "PRG-ROM bank switching slot index must be 0,1 for mappers #4, #119"
.IF slot_idx_ < 2
    LDA     #%00000110+slot_idx_
.ELSE
    LDA     #%00001111
.ENDIF
    STA     $8000
    LDA     #bank_idx_
    STA     $8001
.ENDIF
.ENDMACRO

;--
; Restore previous bank.
; NOTE: Currently only supports iNES mapper #2.
; Modifies:
;   A,Y
;--
.MACRO NK_BANK_RESTORE
.ASSERT __NUM_BANKS__ > 1, "PRG-ROM bank switching requires at least two PRG-ROM banks"
.ASSERT __MAPPER__ == 2, "PRG-ROM bank restore is currently only supported by iNES mapper #2"
    LDY     v_nk_curr_bank
    NK_BANK_SWITCH_NO_SAVE
.ENDMACRO

;--
; Set CHR-ROM bank.
; Also switches from CHR-RAM to CHR-ROM on mapper #119.
; NOTE: Requires iNES mapper #4 or #119.
; Inputs:
;   Y: Bank index to set (0-5):
;      0: Select 2 KB CHR bank at PPU $0000-$07FF (or $1000-$17FF).
;      1: Select 2 KB CHR bank at PPU $0800-$0FFF (or $1800-$1FFF).
;      2: Select 1 KB CHR bank at PPU $1000-$13FF (or $0000-$03FF).
;      3: Select 1 KB CHR bank at PPU $1400-$17FF (or $0400-$07FF).
;      4: Select 1 KB CHR bank at PPU $1800-$1BFF (or $0800-$0BFF).
;      5: Select 1 KB CHR bank at PPU $1C00-$1FFF (or $0C00-$0FFF).
;   A: Bank offset as a 1 KB multiple.
;      NOTE: Bank indices 0-1 require an even multiple: 0,2,4...
;--
.MACRO NK_CHR_BANK_SET
.ASSERT __MAPPER__ == 4 || __MAPPER__ == 119, "CHR-ROM bank switching requires iNES mapper #4 or #119"
.ASSERT __NUM_CHR_BANKS__ > 0, "CHR-ROM bank switching requires at least one CHR-ROM bank"
    STY     $8000
    STA     $8001
.ENDMACRO

;--
; Switch from CHR-ROM to CHR-RAM.
; NOTE: Requires iNES mapper #119.
; Modifies:
;   A
;--
.MACRO NK_CHR_RAM_ENABLE
.ASSERT __MAPPER__ == 119, "Switching from CHR-ROM to CHR-RAM requires iNES mapper #119"
    LDA     #$80
    STA     $8001
.ENDMACRO

;--
; Perform RTS jump. Jumps to a subroutine in a call table.
; Inputs:
;   Y: Offset in jump table (=2x SUBROUTINE INDEX!)
;   table_: Jump table to subroutines.
; Modifies:
;   A,*
;--
.MACRO NK_RTS_JUMP table_
    LDA     table_+1, Y
    PHA
    LDA     table_+0, Y
    PHA
    RTS
.ENDMACRO

;--
; Enable PPU palette tint. For debugging code timing.
; Modifies:
;   A
;--
.MACRO NK_DEBUG_TINT_START
    LDA     v_nk_ppu_mask
    ORA     #NK_MASK_TINT_BLUE
    STA     $2001
.ENDMACRO

;--
; Disable PPU palette tint. For debugging code timings
; Modifies:
;   A
;--
.MACRO NK_DEBUG_TINT_END
    LDA     v_nk_ppu_mask
    STA     $2001
.ENDMACRO


; Include zeropage assembly.
.SEGMENT    "ZEROPAGE"
.ORG        $0000           ; Zeropage segment is always at $0000-$00FF.
.INCLUDE    "nk_zeropage.s"

; Include BSS assembly.
.SEGMENT    "BSS"
.ORG        NK_BSS_BASE
.INCLUDE    "nk_bss.s"

; Include code assembly.
.SEGMENT    "CODE"
.BANK       __NUM_BANKS__-1 ; Default PRG-ROM bank is the last bank.
.ORG        NK_CODE_BASE    ; Default origin for nk code.
.INCLUDE    "nk_code.s"
