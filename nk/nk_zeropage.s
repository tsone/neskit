;--
; Source code for zeropage segment assembly.
;--

t0:                 .RES 1
t1:                 .RES 1
t2:                 .RES 1
t3:                 .RES 1
t4:                 .RES 1
t5:                 .RES 1

; Increment to each byte nk_yalz_decode_ppu and nk_ppu_copy.
z_nk_decode_incr:   .RES 1

; Control nk NMI routine behavior. Bits:
;   7: 1="raw mode", i.e. run user VBLANK only. 0=default.
z_nk_nmi_flags:     .RES 1

; Include ZEROPAGE variables from modules.
.IFDEF NK_USE_W
    _NK_W_ZEROPAGE
.ENDIF
.IFDEF NK_USE_JOYPAD
    _NK_JOYPAD_ZEROPAGE
.ENDIF
.IFDEF NK_USE_RANDOM
    _NK_RANDOM_ZEROPAGE
.ENDIF
.IFDEF NK_USE_PATT
    _NK_PATT_ZEROPAGE
.ENDIF
.IFDEF NK_USE_SKIN
    _NK_SKIN_ZEROPAGE
.ENDIF
.IFDEF NK_USE_YALZ
    _NK_YALZ_ZEROPAGE
.ENDIF
.IFDEF NK_USE_AUDIO
    _NK_AUDIO_ZEROPAGE
.ENDIF
.IFDEF NK_USE_BCD
    _NK_BCD_ZEROPAGE
.ENDIF
