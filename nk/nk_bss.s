;--
; Source code for BSS segment assembly.
;--

v_nk_ppu_ctrl:      .RES 1      ; Contents of $2000 register.
v_nk_ppu_mask:      .RES 1      ; Contents of $2001 register.
v_nk_ppu_flags:     .RES 1      ; Misc flags (related to $2001 register).
v_nk_ppu_scroll_x:  .RES 1
v_nk_ppu_scroll_y:  .RES 1
v_nk_pal:                       ; Label for all palettes.
v_nk_pal_zero:      .RES 1      ; Zero background color.
v_nk_pal_bg:        .RES 4*3    ; Background palette.
v_nk_pal_spr:       .RES 4*3    ; Sprite palette.
v_nk_fc_lo:         .RES 1      ; Global frame counter (16-bit).
v_nk_fc_hi:         .RES 1      ; (Wraps in ~18 min on NTSC, ~22 min on PAL.)
v_nk_ntsc_flags:    .RES 1      ; bit7: 1=NTSC, 0=PAL; bit0-2: frame counter
.IF __MAPPER__ == 2 && __NUM_BANKS__ > 1
v_nk_curr_bank:     .RES 1      ; Current bank number.
.ENDIF

; Include BSS variables from modules.
.IFDEF NK_USE_W
    _NK_W_BSS
.ENDIF
.IFDEF NK_USE_JOYPAD
    _NK_JOYPAD_BSS
.ENDIF
.IFDEF NK_USE_RANDOM
    _NK_RANDOM_BSS
.ENDIF
.IFDEF NK_USE_PATT
    _NK_PATT_BSS
.ENDIF
.IFDEF NK_USE_SKIN
    _NK_SKIN_BSS
.ENDIF
.IFDEF NK_USE_YALZ
    _NK_YALZ_BSS
.ENDIF
.IFDEF NK_USE_AUDIO
    _NK_AUDIO_BSS
.ENDIF
.IFDEF NK_USE_BCD
    _NK_BCD_BSS
.ENDIF
