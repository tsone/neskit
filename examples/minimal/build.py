#!/usr/bin/env python3
import os, subprocess
os.chdir(os.path.dirname(os.path.abspath(__file__)))
def sh(line):
    subprocess.check_call(line, shell=True)
try:
    os.mkdir("generated")
except:
    pass

sh("python3 ../../nkasm -I ../../nk -v -d -b 1 -o minimal.nes -l minimal.lst minimal.s")
