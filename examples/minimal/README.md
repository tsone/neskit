
# Minimal example

This example is based on the [NintendoAge][NA] forum
[Nerdy Nights week 3][NN] tutorial. One can use the example as
a starting point for a game using neskit.

The example enables background blue "tint" bit in PPU mask register ($2001).
Please refer to the [original article][NN] for more information.

To build, run `build.py`.

[NA]: http://www.nintendoage.com/
[NN]: http://www.nintendoage.com/forum/messageview.cfm?catid=22&threadid=4440


## License

Minimal example is released under MIT license.

```
minimal - neskit example
---------------------------

Copyright (c) 2013 Valtteri Heikkila

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
