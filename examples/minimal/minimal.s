;--
; nk module includes.
;--
.INCLUDE    "nk.s"

;--
; Note, we do not define any variables since none is needed by this program.
; We start defining our PRG-ROM code directly.
;--
.SEGMENT    "CODE"

;--
; Nk library jumps to RESET label at console reset interrupt (when reset or
; power button is pressed). Jump happens after nk has done all mandatory
; system initialization.
;--
RESET:
    ; Enables NMI interrupt.
    NK_CTRL_CONFIG  NK_CTRL_NMI

    ; NOTE: The "tint" bits may work differently in various emulators.
    ; For FCEUX, use the 'Old PPU' option to see the effect. 'New PPU'
    ; option in FCEUX v2.2.1 (at least) does not show color tint correctly.
    NK_MASK_CONFIG  NK_MASK_TINT_BLUE
    RTS

;--
; UPDATE is called for every frame after VBLANK. This is where game logic is
; done to update game state (ex. do AI calculation, move sprites, change
; level). If UPDATE is longer than full screen redraw by PPU, it will be
; interrupted at some point by VBLANK. Nk library will handle the NMI
; interrupt correctly by pushing and restoring the contents of registers.
;--
UPDATE:
    RTS

;--
; Nk library jumps to label VBLANK during PPU VBLANK period. We must modify
; PPU memory here because NES/FC hardware doesn't allow PPU memory access
; outside VBLANK period.
;--
VBLANK:
    RTS
