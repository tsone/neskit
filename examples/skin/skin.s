.INCLUDE    "modules/nk_skin.s"
.INCLUDE    "modules/nk_joypad.s"
.INCLUDE    "nk.s"

.SEGMENT    "CODE"

PLAYER_SKIN_IDX = 0

RESET:
    NK_CTRL_CONFIG  NK_CTRL_NMI | NK_CTRL_8x16_SPR | NK_CTRL_SPR_HIGH
    NK_MASK_CONFIG  NK_MASK_BG_SHOW | NK_MASK_SPR_SHOW | NK_MASK_BG_NOCLIP | NK_MASK_SPR_NOCLIP

    NK_PAL_LOAD palette

    LDA     #GEN_PATT_SKIN_FLAPPY|NK_PATT_SPR_VRAM
    JSR     nk_patt_load

    LDA     #GEN_PATT_SKIN_FLAPPY
    STA     v_nk_skin_patt

    LDA     #GEN_SKIN_FLAPPY_DOWN|NK_SKIN_CHANGE_FORCE
    LDX     #PLAYER_SKIN_IDX
    JSR     nk_skin_change_anim

    LDA     #128
    STA     v_nk_skin_x+PLAYER_SKIN_IDX
    LDA     #112
    STA     v_nk_skin_y+PLAYER_SKIN_IDX

    RTS

UPDATE:
    NK_JOYPAD_READ  0           ; Read joypad 1 state.

    LDX     #PLAYER_SKIN_IDX
    LDA     v_nk_joypad_input ; Load joypad pressed button bits in A.
;@test_right:
    LSR
    BCC     @test_left
    INC     v_nk_skin_x+PLAYER_SKIN_IDX
    LDA     #GEN_SKIN_FLAPPY_RIGHT
    BCS     @has_input
@test_left:
    LSR
    BCC     @test_down
    DEC     v_nk_skin_x+PLAYER_SKIN_IDX
    LDA     #GEN_SKIN_FLAPPY_LEFT
    BCS     @has_input
@test_down:
    LSR
    BCC     @test_up
    INC     v_nk_skin_y+PLAYER_SKIN_IDX
    LDA     #GEN_SKIN_FLAPPY_DOWN
    BCS     @has_input
@test_up:
    LSR
    BCC     @no_input
    DEC     v_nk_skin_y+PLAYER_SKIN_IDX
    LDA     #GEN_SKIN_FLAPPY_UP
    BCS     @has_input
@no_input:
    NK_SKIN_ANIM_HOLD 1
    BCC     @update_end
@has_input:
    JSR     nk_skin_change_anim

@update_end:
    JSR     nk_skin_sort_all
    JSR     nk_skin_update_all
    JSR     nk_anim_hide_unused
    RTS

VBLANK:
    RTS

.INCLUDE "flappy.s"
.INCLUDE "patt.s"

palette:
.INCBIN "flappy.pale"
