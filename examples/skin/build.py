#!/usr/bin/env python3
import os, subprocess
os.chdir(os.path.dirname(os.path.abspath(__file__)))
def sh(line):
    subprocess.check_call(line, shell=True)
try:
    os.mkdir("generated")
except:
    pass

sh("python3 ../../tools/skintool.py res/flappy.json generated/flappy.s generated/skin_flappy.patt.yalz")
sh("python3 ../../tools/imgtool.py -p generated/flappy.pale res/flappy.png")
sh("python3 ../../tools/patttool.py generated/ generated/patt.s")
sh("python3 ../../nkasm -v -I ../../nk -I generated -b 1 -o skin.nes -d -l skin.lst skin.s")
