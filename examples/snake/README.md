
# Snake example

This example demonstrates the use of nk and nkasm in a simple snake game.
The assembler source is thoroughly documented, but still requires fairly
good understanding of NES and 6502 assembly programming. It in doubt, see
[Nerdy Nights][NN] and other tutorials at the [NintendoAge][NA] forum,
and [Everynes NES Specs][EN] by Martin Korth.

The example uses nk framework joypad and random modules, where random module
is used to generate the food sprite location. Snake direction is changed by
reading the joypad low nibble bits. Start button resets the game at any time.

The graphics are generated in code to keep things simple and to demonstrate
manipulation of CHR-RAM with iNES mapper #2. The background and sprites use
same tile graphics (one black and three white tiles).

The snake tiles are written in the name table in PPU memory directly.
The snake head collision test is also done by reading directly a tile from
the PPU memory. Because accessing the PPU memory is safe only during the
VBLANK (when PPU is enabled), most of the game logic takes place during VBLANK.

To build, run `build.py`.

[NA]: http://www.nintendoage.com/
[NN]: http://www.nintendoage.com/forum/messageview.cfm?catid=22&threadid=7155
[EN]: http://nocash.emubase.de/everynes.htm


## License

Snake example is released under MIT license.

```
snake - neskit example
----------------------

Copyright (c) 2013 Valtteri Heikkila

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
