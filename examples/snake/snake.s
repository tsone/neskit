;--
; nk module includes.
;--
NK_NTSC_FRAMESKIP = 1           ; Skip every 6th frame on NTSC system.
.INCLUDE    "modules/nk_joypad.s" ; Joypad routines.
.INCLUDE    "modules/nk_random.s" ; Random number routines.
.INCLUDE    "nk.s"              ; Main nk include. Must be the last include.


;--
; Game constants.
;--
; A point data structure. This is one way to define a structure in
; 6502 assembly. This method defines struct size (here PNT_STRUCT)
; and the offsets to each member (PNT_X, PNT_Y).
PNT_X           = 0             ; Offset to X coordinate.
PNT_Y           = PNT_X+1       ; Offset to Y coordinate.
PNT_STRUCT      = PNT_Y+1       ; Size of struct.

; Similar data structure for both snake ends.
ENDS_POSITION   = 0             ; Position (in tiles, PNT_STRUCT).
ENDS_COUNTER    = ENDS_POSITION+PNT_STRUCT ; A counter for delaying move.
ENDS_DIRECTION  = ENDS_COUNTER+1 ; Direction of movement.
ENDS_RESET      = ENDS_DIRECTION+1 ; Reset value for counter.
ENDS_STRUCT     = ENDS_RESET+1  ; Size of struct.

; Other constants.
TILE_EMPTY = 4                  ; Tile/character index an empty square.
EAT_FOOD_GROW = 6               ; Amount to grow snake when food is eaten.


;--
; Zeropage variables.
;--
; This defines what game variables are stored in zeropage memory segment.
; Zeropage segment is common for all 6502 processors. It is 256 bytes long
; (i.e. one page is 256 bytes in 6502) and address range is $0000-$00FF.
; Since this game uses very little memory, all variables can be in zeropage.
; Larger games must additionally store variables to BSS segment $0200-$07FF.
; We use z_ prefix to indicate label is in zeropage.
.SEGMENT    "ZEROPAGE"
; NOTE: Do not set .ORG in zeropage. nk handles this for you.
z_head:          .RES ENDS_STRUCT   ; Snake head and tail ends.
z_tail:          .RES ENDS_STRUCT
z_food_pos:      .RES PNT_STRUCT    ; Food sprite position (x,y) (in tiles).
z_food_count:    .RES 1             ; Number of food eaten since reset.
z_prev_head_dir: .RES 1             ; Previous direction for input handling.


;--
; Code (PRG-ROM).
;--
; "Code" segment is where all program code is defined, i.e. PRG-ROM.
; The NES/FC maps 32KB address area for PRG-ROM ($8000-$FFFF). To cover
; this area, we need (at least) two 16KB banks. Because nk and nkasm use
; a bank-switchable iNES mapper 2 (UxROM), we need to place always-present
; routines (for example UPDATE, VBLANK, RESET) to a fixed, non-switchable
; PRG-ROM bank. Mapper #2 has this in the last PRG-ROM bank. It is mapped to
; address area $C000-$FFFF.
.SEGMENT    "CODE"
; NOTE: Again, there is no need to set .ORG. nk handles this for you.

;--
; nk jumps to RESET label at console reset interrupt (when reset or power
; button is pressed). Jump to here happens after nk has done all mandatory
; subsystem initializations.
;--
RESET:
    ; Enable NMI interrupt. Sets both sprite and background tile to PPU
    ; address $0000.
    NK_CTRL_CONFIG  NK_CTRL_NMI

    ; Enable background and sprites. Additionally remove black clip border
    ; for background. (Here sprites doesn't need it removed since food
    ; sprite is always near the middle of the screen.)
    NK_MASK_CONFIG  NK_MASK_BG_SHOW|NK_MASK_SPR_SHOW|NK_MASK_BG_NOCLIP

    ; Jump to our game reset routine. Since reset_game is a subroutine
    ; (ends with RTS) there is no need to JSR.
    JMP     reset_game          ; RESET done.

;--
; UPDATE is called for every frame after VBLANK. This is where game logic is
; done to update game state (ex. do AI calculation, move sprites, change
; level). If UPDATE is longer than full screen redraw by PPU, it will be
; interrupted at some point by VBLANK. nk will handle the NMI interrupt
; correctly by pushing and restoring the contents of registers.
;--
UPDATE:
    NK_JOYPAD_READ  0           ; Reads joypad 1 state to v_nk_joypad_input.

    ; Translate joypad input state to direction (NK_DIR_LEFT, NK_DIR_RIGHT,
    ; etc.) The proper direction value will be in 0..3.
    LDA     v_nk_joypad_input
    JSR     nk_joypad_get_dir
    BMI     @none_found
;@found:
    ; Check direction is valid by comparing to previous one. Direction must
    ; be horizontal if previous direction was vertical (and vice versa).
    ; For this check NK_DIR_HORZ_BIT. If set, direction is horizontal.
    ; Otherwise it's vertical.
    TXA                         ; X tells the new direction number.
    EOR     z_prev_head_dir     ; XOR with previous to test difference.
    AND     #NK_DIR_HORZ_BIT    ; If HORZ_BIT differs, then direction changed
    BEQ     @none_found         ; from vertical to horizontal or vice versa.
    STX     z_head+ENDS_DIRECTION
@none_found:

    JSR     food_maybe_eat      ; Check if head is at food, and if so, eat.

    ; Randomize new food position if z_food_pos.y == 0.
    LDA     z_food_pos+PNT_Y
    BNE     @skip_food_randomize
    JSR     food_randomize
@skip_food_randomize:

    ; Reset game if start button is pressed.
    LDA     #NK_JOYPAD_START
    BIT     v_nk_joypad_input
    BEQ     @skip_reset_game
@do_reset_game:
    JSR     reset_game
@skip_reset_game:

    RTS                         ; UPDATE done.

;--
; nk jumps to label VBLANK during PPU VBLANK period. We must modify tile
; map (aka name table) here because NES/FC hardware doesn't allow PPU memory
; access outside VBLANK period when PPU is enabled. Large part of game logic
; is also done here. While this works in this case, it is generally a bad
; idea to have game logic in VBLANK. It should be reserved for PPU
; manipulation.
;--
VBLANK:

    ; Ensure that the tile under food sprite is empty. If not, food will
    ; be re-randomized. This check needs to be done before head is
    ; drawn. Otherwise the food would be incorrectly randomized just
    ; at the moment when the head would eat it.
    LDX     #<z_food_pos        ; Set X to point to food position.
    JSR     set_ppu_addr        ; Calculate/set address for PPU read/write.
    ; The load from register $2007 always returns the _previous_ result, so
    ; we need to load twice.
    LDA     $2007
    LDA     $2007               ; Actual tile is returned here.
    CMP     #TILE_EMPTY
    BEQ     @food_valid
    ; Set z_food_pos.y = 0 to indicate food position needs to be randomized.
    LDA     #0
    STA     z_food_pos+PNT_Y
@food_valid:

    ; Draw head tile. The tile is drawn every frame.
    LDX     #<z_head+ENDS_POSITION ; Set X to point to head position.
    JSR     set_ppu_addr
    LDA     z_head+ENDS_DIRECTION
    STA     $2007               ; Write tile in PPU via register $2007.

    ; Update head state. This will move the head when counter is zero.
    ; If so, collision checks need to be performed and tail needs to be
    ; also updated.
    LDX     #z_head             ; Set X to point to head struct.
    JSR     ends_update
    BCC     @skip_move_tail     ; Nothing else to do if carry is cleared.

    LDA     z_head+ENDS_DIRECTION ; Store previous head direction.
    STA     z_prev_head_dir

    JSR     check_collision     ; Check if head collides.

    ; Update tail end. If tail is moved the new tail direction needs to be
    ; read and stored (for tail to follow snake body correctly). The
    ; direction is stored in the PPU tile directly, i.e. the graphic tells
    ; the direction.
    LDX     #z_tail             ; Again, set X to point to tail struct.
    JSR     ends_update
    BCC     @skip_move_tail

    ; Read tail direction from PPU tile.
    LDX     #<z_tail+ENDS_POSITION
    JSR     set_ppu_addr
    LDA     $2007               ; Again, need to read twice...
    LDA     $2007               ; Actual tile (direction) is returned here.
    STA     z_tail+ENDS_DIRECTION

    ; Draw tail tile. Note: we have same PPU address here as before.
    ; Unfortunately however loads from $2007 will incremented PPU address
    ; pointer. Because of this, we need to set the PPU address again.
    LDX     #<z_tail+ENDS_POSITION
    JSR     set_ppu_addr
    LDA     #TILE_EMPTY
    STA     $2007               ; Write tile.
@skip_move_tail:

    RTS                         ; VBLANK done.

;--
; Reset game state. Sets all game variables to default state.
;--
reset_game:
    ; Disable PPU to that we can modify PPU memory (VRAM) freely.
    NK_PPU_DISABLE

    LDX     #0
    STX     z_food_pos+PNT_Y    ; Set food to be randomized at first frame.
    STX     z_food_count        ; Reset food eat counter.
    INX                         ; X=1
    STX     z_tail+ENDS_RESET   ; Set tail counter reset counter to 1.

    ; Set initial direction (right).
    LDA     #3
    STA     z_head+ENDS_DIRECTION
    STA     z_tail+ENDS_DIRECTION
    STA     z_prev_head_dir

    ; Set initial length for snake.
    LDA     #EAT_FOOD_GROW
    STA     z_tail+ENDS_COUNTER

    ; Set initial speed for head.
    LDA     #10                 ; Move head every 10 frames.
    STA     z_head+ENDS_RESET
    STA     z_head+ENDS_COUNTER

    ; Set initial X position for head and tail.
    LDA     #(256/2)/8
    STA     z_head+ENDS_POSITION+PNT_X
    STA     z_tail+ENDS_POSITION+PNT_X
    DEC     z_tail+ENDS_POSITION+PNT_X

    ; Set initial Y position for head and tail.
    LDA     #(240/2)/8
    STA     z_head+ENDS_POSITION+PNT_Y
    STA     z_tail+ENDS_POSITION+PNT_Y

    ; Sets background and sprite palettes in v_nk_pal_bg. nk copies
    ; the palette from this array to PPU every frame (which allows
    ; palette effects). Both background and sprite palettes are 12 colors,
    ; so nk stores only these in addition to the zero-color.
    LDA     #$0F                ; Black ($0F) for zero color.
    STA     v_nk_pal_zero
    LDA     #$30                ; White ($30) for other colors.
    LDX     #3*4-1              ; Set 12 distinct colors in BG and sprite pal.
@reset_pal_loop:
    STA     v_nk_pal_bg, X
    STA     v_nk_pal_spr, X
    DEX
    BPL     @reset_pal_loop

    ; Set PPU address pointer to $0000 and generate tile graphics in CHR-RAM
    ; on the cartridge (as we're using iNES mapper #2).
    ; We generate TILE_EMPTY (=4) white tiles, for each snake
    ; direction), then one black tile for black (empty) background.
    ; So TILE_EMPTY (=4) will be the first empty (black) pattern table tile.
    NK_PPU_ORG $0000
    ; BG sub-palette color 3 is always white, so set 4x color 3 => X=$FF
    LDX     #$FF
    ; Every tile graphic is 16 bytes => TILE_EMPTY*16.
    LDY     #TILE_EMPTY*16
@reset_tiles_loop:
    STX     $2007
    DEY
    BNE     @reset_tiles_loop
    ; Set X,Y for a single black tile to reuse @reset_tiles_loop.
    ; Incrementing X will overflow it to 0 (and black is color 0 in
    ; our palette).
    LDY     #1*16
    INX                         ; X=0, equals to 4x color 0.
    BEQ     @reset_tiles_loop   ; X=0 at first branch, X=1 at second.

    ; Clear name and attribute tables by setting 4*256 bytes (=1KB) of
    ; the memory to TILE_EMPTY (=4, character/pattern table index of our
    ; empty tile). The attributes don't matter as all 4 our background
    ; palettes have same colors.
    NK_PPU_ORG $2000            ; Initialize PPU pointer.
    LDA     #TILE_EMPTY
    LDY     #4                  ; Outer loop executed 4 times.
    LDX     #0                  ; Inner loop executed 256 times.
@clear_bg_loop:
    STA     $2007
    DEX
    BNE     @clear_bg_loop
    DEY
    BNE     @clear_bg_loop

    ; Done with game reset. Enable PPU.
    NK_PPU_ENABLE
    RTS

;--
; Calculate PPU address from tile coordinates. Tile coordinates
; should be in range X=0..31, Y=0..29.
; Inputs:
;  X: Index to PNT_STRUCT in zeropage.
;--
set_ppu_addr:
    LDA     $2002               ; Reset PPU latch for setting address.
    ; Calculate PPU address high byte. Keep 2 MSB of Y coordinate.
    LDA     PNT_Y, X
    LSR     A
    LSR     A
    LSR     A
    ORA     #$20
    STA     $2006               ; Set address high byte.
    ; Calculate PPU address low byte. Take 3 LSB of Y coordinate and
    ; bitwise OR with 5 bits X coordinate.
    LDA     PNT_Y, X
    ASL     A
    ASL     A
    ASL     A
    ASL     A
    ASL     A
    ORA     PNT_X, X
    STA     $2006               ; Set address low byte.
    RTS

;--
; Update snake ends (head or tail). The counter in struct is decremented,
; and if count is zero, end is moved towards the current direction.
; Inputs:
;  X: Zeropage index to ENDS_STRUCT in zeropage.
; Returns:
;  C=0 if we didn't move, C=1 if moved.
;--
ends_update:
    CLC                         ; Clear carry for default return value.
    DEC     ENDS_COUNTER, X     ; Decrement counter.
    BNE     @exit               ; Exit if counter is not zero.

    ; Now counter is zero. This reset the counter to a new value.
    LDA     ENDS_RESET, X
    STA     ENDS_COUNTER, X
    SEC

    ; Performs move based on direction. There are 4 directions, and the
    ; horizontal and vertical movement are similar; only the offset to
    ; X or Y coordinate differs. Following code decrements the direction
    ; value and checks the correct jump for that direction. It also adjusts
    ; offset in X register in case direction is on horizontal axis.
    LDY     ENDS_DIRECTION, X
    BEQ     @decr
    DEY
    BEQ     @incr
    DEX                         ; Decrement X for horizontal axis.
    DEY
    BEQ     @decr
    ; By default, the following code moves along vertical axis. By adjusting
    ; offset in X register, this will perform also horizontal move.
@incr:
    INC     ENDS_POSITION+PNT_Y, X
    RTS
@decr:
    DEC     ENDS_POSITION+PNT_Y, X
    ;->
@exit:
    RTS

;--
; Randomize food position. The food sprite will appear at a 16x16 area in
; the middle of the screen.
;--
food_randomize:
    ; Randomize X and Y coordinate with food_randomize_one.
    LDA     #8                  ; Offset for randomization.
    LDX     #<z_food_pos+PNT_X  ; Set X register for the result.
    JSR     food_randomize_one
    LDA     #6
    LDX     #<z_food_pos+PNT_Y
    JSR     food_randomize_one

    ; Convert X and Y tile coordinates to sprite (pixel) coordinates.
    LDA     z_food_pos+PNT_X    ; For X coordinate.
    ASL     A                   ; Multiply by 8 (shift left by 3).
    ASL     A
    ASL     A
    STA     NK_SPR_BASE+3
    LDA     z_food_pos+PNT_Y    ; For Y coordinate.
    ASL     A                   ; Multiply by 8 (shift left by 3).
    ASL     A
    ASL     A
    SBC     #0                  ; Subtract 1 for Y coordinate. C=0 due ASL.
    STA     NK_SPR_BASE+0
    RTS

;--
; Randomize a single food coordinate. Value will be in 0..15 incremented by
; the contents of A register.
;--
food_randomize_one:
    STA     0, X
    NK_RANDOM                   ; Call nk library random routine.
    AND     #15                 ; "Clamp" down to 0..15 range.
    CLC
    ADC     0, X
    STA     0, X                ; Store result after adding increment.
    RTS

;--
; Check if food is at head position. If so, eat the food.
;--
food_maybe_eat:
    ; Compare food position against head position.
    LDA     z_food_pos+PNT_X    ; Compare X coordinate.
    CMP     z_head+ENDS_POSITION+PNT_X
    BNE     @exit
    LDA     z_food_pos+PNT_Y    ; Compare Y coordinate.
    CMP     z_head+ENDS_POSITION+PNT_Y
    BNE     @exit
    ; Head is at food, so eat the food.
    LDA     #0                  ; Set to be randomized on next frame.
    STA     z_food_pos+PNT_Y
    LDA     #EAT_FOOD_GROW      ; Increase snake length by setting counter.
    CLC
    ADC     z_tail+ENDS_COUNTER
    STA     z_tail+ENDS_COUNTER
    INC     z_food_count        ; Increment "score" or food count.
    ; Next check if speed will be increased. Speed is increased for every 4
    ; foods eaten. This check is done with BIT instruction.
    LDA     #3
    BIT     z_food_count        ; Check if bitmask is zero.
    BNE     @exit
    DEC     z_head+ENDS_RESET   ; Increase speed by decrementing head reset.
@exit:
    RTS

;--
; Check if head collides with playfield limits or with itself. If it
; collides, game state will be reset immediately.
;--
check_collision:
    ; Check for horizontal limits (X coordinate).
    LDA     z_head+ENDS_POSITION+PNT_X
    BMI     @head_collides      ; X < 0?
    CMP     #32
    BEQ     @head_collides      ; X = 32?
    ; Check for vertical limits (Y coordinate).
    LDA     z_head+ENDS_POSITION+PNT_Y
    BEQ     @head_collides      ; Y = 0?
    CMP     #32-3
    BEQ     @head_collides      ; Y == 32-3?
    ; Check for collision with snake body.
    LDX     #<z_head+ENDS_POSITION
    JSR     set_ppu_addr        ; Calculate PPU address for head position.
    LDA     $2007
    LDA     $2007               ; Read tile at head.
    CMP     #TILE_EMPTY
    BNE     @head_collides      ; Collide if tile is not TILE_EMPTY.
    RTS
@head_collides:
    JMP     reset_game          ; JMP is used since reset_game is subroutine.
