#!/usr/bin/env python3
import os, subprocess
os.chdir(os.path.dirname(os.path.abspath(__file__)))
def sh(line):
    subprocess.check_call(line, shell=True)

for subdir in next(os.walk('.'))[1]:
    print(f"-- Building {subdir}")
    sh("python3 " +subdir + "/build.py")
