;--
; nk module includes.
;--
.INCLUDE    "modules/nk_audio.s" ; Include audio module.
.INCLUDE    "modules/nk_joypad.s" ; Include joypad module.
.INCLUDE    "nk.s"              ; Include main module. (Must be last.)


;--
; Zeropage variables.
;--
.SEGMENT "ZEROPAGE"
z_song_part:  .RES 1            ; Index of current song part.


;--
; Code (PRG-ROM).
;--
.SEGMENT    "CODE"

;--
; RESET or initialize our app state.
; Called by nk after module initializations after system reset or power on.
;--
RESET:
    ; Enable NMI interrupt.
    NK_CTRL_CONFIG  NK_CTRL_NMI

    NK_MASK_CONFIG  NK_MASK_MONOCHROME|NK_MASK_BG_SHOW

    ; Disable looping on sound effects (SFX) player.
    NK_AUDIO_SET_FLAGS NK_AUDIO_SFX_NO_LOOP

    ; Play song 0 on background music (BGM) player.
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_BGM, 0

    RTS                         ; RESET done.

;--
; Update the nk audio. Additionally visualize audio update duration by tinting
; pixels blue. Every blue scanline (pixel row) is equal 113 2/3 CPU cycles
; on a NTSC system.
;--
VBLANK:
    ; Delay some CPU cycles to clearly show the start of the blue tint.
    LDY     #200
@wait:
    LDX     #2
@wait_sub:
    DEX
    BNE     @wait_sub
    DEY
    BNE     @wait

    NK_DEBUG_TINT_START         ; Enable blue tint.

    ; Update audio. This manipulates the APU registers for audio playback.
    ; You must call this every frame. For best playback result (timing), call
    ; this at consistently at same frequency, for example in NMI/VBLANK
    ; (as we do here).
    NK_AUDIO_UPDATE

    NK_DEBUG_TINT_END           ; Disable blue tint.

    RTS                         ; VBLANK done.

;--
; Handle joypad input and update audio.
; Called by nk every frame after calling VBLANK.
;--
UPDATE:

    NK_JOYPAD_READ  0           ; Read joypad 1 state.

    LDA     v_nk_joypad_pressed ; Load joypad pressed button bits in A.
;@joy_right:
    LSR                         ; Test NK_JOYPAD_RIGHT (bit 0).
    BCC     @joypad_left
    LDA     z_song_part
    CMP     #SONG_PARTS_NUM-1   ; Increment current part and play.
    BCS     @joypad_exit
    ADC     #1 ; C=1
@store_and_play_current_part:
    STA     z_song_part
@play_current_part:
    TAX                         ; Select song part in A.
    LDY     #1                  ; Select BGM player
    JSR     nk_audio_play       ; Start playback
    JMP     @joypad_exit

@joypad_left:
    LSR                         ; Test NK_JOYPAD_LEFT (bit 1).
    BCC     @joypad_down
    LDA     z_song_part         ; Decrement current part and play.
    BEQ     @joypad_exit
    SBC     #1 ; C=1
    BPL     @store_and_play_current_part

@joypad_down:
    LSR                         ; Test NK_JOYPAD_DOWN (bit 2).
    BCC     @joypad_up
    LDA     #1
    JSR     nk_audio_stop       ; Stop BGM playback
    JMP     @joypad_exit

@joypad_up:
    LSR                         ; Test NK_JOYPAD_UP (bit 3).
    BCC     @joypad_start
    LDA     z_song_part
    BCS     @play_current_part  ; Play current part

@joypad_start:
    LSR                         ; Test NK_JOYPAD_START (bit 4).
    BCC     @joypad_exit
    LDX     #SONG_PARTS_NUM-1   ; Select last song part
    LDY     #NK_AUDIO_PLAYER_SFX ; Select SFX player
    JSR     nk_audio_play       ; Start playback
    JMP     @joypad_exit

@joypad_exit:

    RTS                         ; UPDATE done.

.STAT audio_song_s
.INCLUDE    "song.s"
.ENDSTAT
