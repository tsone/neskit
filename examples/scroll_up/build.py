#!/usr/bin/env python3
import os, subprocess
os.chdir(os.path.dirname(os.path.abspath(__file__)))
def sh(line):
    subprocess.check_call(line, shell=True)
try:
    os.mkdir("generated")
except:
    pass

sh("python3 ../../tools/tiledtool.py -y -s up -p generated/bg_tileset.pale -bn generated/bg_map.name -bp generated/bg_tileset.patt res/bg_map.json")
sh("python3 ../../tools/tiledtool.py -ba generated/bg_map.attr res/bg_map.json")
sh("python3 ../../tools/patttool.py generated/ generated/patt.s")
sh("python3 ../../nkasm -v -I ../../nk -I generated -b 1 -d -o scroll_up.nes -l scroll_up.lst scroll_up.s")
