.INCLUDE    "modules/nk_patt.s"
.INCLUDE    "modules/nk_joypad.s"
.INCLUDE    "nk.s"

MAP_HEIGHT = 4*240

.SEGMENT    "ZEROPAGE"
z_vblank_command: .RES 1

z_scroll_x_lo:  .RES 1
z_scroll_x_hi:  .RES 1
z_scroll_y_lo:  .RES 1
z_scroll_y_hi:  .RES 1
z_scroll_y_lo_prev: .RES 1

z_ppu_ptr_base_hi: .RES 1 ; z_ppu_ptr_base_lo = $00

z_name_ptr_lo:      .RES 1
z_name_ptr_hi:      .RES 1
z_attr_ptr_lo:      .RES 1
z_attr_ptr_hi:      .RES 1

z_row:        .RES 1

.SEGMENT    "CODE"

RESET:
    NK_CTRL_CONFIG  NK_CTRL_NMI
    NK_MASK_CONFIG  NK_MASK_BG_SHOW|NK_MASK_BG_NOCLIP

    LDA     #>(bg_map_attr)
    STA     z_attr_ptr_hi
    LDA     #<(bg_map_attr)
    STA     z_attr_ptr_lo

    LDA     #$28
    STA     z_ppu_ptr_base_hi
    LDA     #29
    STA     z_row

    LDA     #<(MAP_HEIGHT-240)
    STA     z_scroll_y_lo
    LDA     #>(MAP_HEIGHT-240)
    STA     z_scroll_y_hi

    NK_PPU_DISABLE

    NK_PAL_LOAD palette

    LDA     #GEN_PATT_BG_TILESET
    JSR     nk_patt_load

    ; Start name table yalz decode.
    ; NOTE: Be careful not to do other yalz decodes as those write to NK_YALZ_PAGE.
    LDY     #<bg_map_name
    LDA     #>bg_map_name
    STA     t1
    LDA     #32
    JSR     nk_yalz_decode_start
    STY     z_name_ptr_lo
    LDA     t1
    STA     z_name_ptr_hi

    JSR     row_copy
@init_name_loop:
    JSR     row_advance
    JSR     row_copy
    LDA     #$20
    CMP     z_ppu_ptr_base_hi
    BNE     @init_name_loop

    LDA     #0
    STA     z_vblank_command

    NK_PPU_ENABLE

    RTS

row_advance:
    ; Advance name table yalz decode
    LDY     z_name_ptr_lo
    LDA     z_name_ptr_hi
    STA     t1
    LDA     #32
    JSR     nk_yalz_decode_continue
    STY     z_name_ptr_lo
    LDA     t1
    STA     z_name_ptr_hi

    DEC     z_row
    BPL     @exit

;@swap_name_table:
    LDA     z_ppu_ptr_base_hi
    EOR     #$08
    STA     z_ppu_ptr_base_hi
    LDA     #29
    STA     z_row

@exit:
    RTS

UPDATE:
    NK_JOYPAD_READ  0

    LDA     #%00001000 ; Joypad up mask
    BIT     v_nk_joypad_input ; Joypad pressed bits -> A
    BEQ     @input_end
;@up_pressed:
    NK_W_DEC z_scroll_y_lo
    LDA     z_scroll_y_hi
    BPL     @input_end
    LDA     #0
    STA     z_scroll_y_lo
    STA     z_scroll_y_hi
@input_end:

    LDA     z_scroll_x_lo
    LDX     z_scroll_x_hi
    NK_PPU_SCROLL_SET_X
    LDA     z_scroll_y_lo
    LDX     z_scroll_y_hi
    NK_PPU_SCROLL_SET_Y

    LDA     z_scroll_y_lo
    TAY
    EOR     z_scroll_y_lo_prev
    STY     z_scroll_y_lo_prev
    AND     #8
    BEQ     @skip_advance
    STA     z_vblank_command
    JSR     row_advance
@skip_advance:

    RTS

VBLANK:
    LDA     z_vblank_command
    BEQ     @exit
    LDA     #0
    STA     z_vblank_command
    BEQ     row_copy ; RTS trick
@exit:
    RTS

row_copy:
    ; Name
    LDY     #0
    STY     t0
    LDA     $2002
    LDA     z_row
    LSR
    ROR     t0
    LSR
    ROR     t0
    LSR
    ROR     t0
    ;CLC
    ADC     z_ppu_ptr_base_hi
    STA     $2006
    LDA     t0
    STA     $2006

    ; Copy decoded name table bytes from NK_YALZ_PAGE
    LDA     v_nk_yalz_lookback_idx
    SEC
    SBC     #32
    TAY
    LDX     #32
@name_row_loop:
    LDA     NK_YALZ_PAGE, Y
    STA     $2007
    INY
    DEX
    BNE     @name_row_loop

    ; Attr
    LDA     z_row
    CMP     #29 ; Special case
    BEQ     @attr_copy
    AND     #3
    CMP     #3 ; First row of section of 4 tile rows
    BNE     @skip_attr_copy

@attr_copy:
    CLC
    LDA     $2002
    LDA     z_ppu_ptr_base_hi
    ADC     #>(32*30)
    STA     $2006
    LDA     z_row
    ASL
    AND     #$F8
    ;CLC
    ADC     #<(32*30)
    STA     $2006

    LDY     #0
@attr_row_loop:
    LDA     (z_attr_ptr_lo), Y
    STA     $2007
    INY
    CPY     #8
    BNE     @attr_row_loop

    ; Advance attr ptr
    LDA     z_attr_ptr_lo
    CLC
    ADC     #8
    STA     z_attr_ptr_lo
    BCC     @skip_attr_hi
    INC     z_attr_ptr_hi
@skip_attr_hi:

@skip_attr_copy:

    RTS

.INCLUDE "patt.s"

palette:
.INCBIN "bg_tileset.pale"

bg_map_name:
.INCBIN "bg_map.name.yalz"

bg_map_attr:
.INCBIN "bg_map.attr"