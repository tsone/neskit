#!/usr/bin/env python3
import os, subprocess
os.chdir(os.path.dirname(os.path.abspath(__file__)))
def sh(line):
    subprocess.check_call(line, shell=True)
try:
    os.mkdir("generated")
except:
    pass

sh("python3 ../../tools/skintool.py res/all.json generated/all.gen.s generated/skin_all.patt.yalz")
sh("python3 ../../tools/imgtool.py -y -bp generated/grid.patt -bn generated/grid.name -ba generated/grid.attr res/grid.png")
sh("python3 ../../tools/imgtool.py -p generated/night.pale res/night.png")
sh("python3 ../../tools/imgtool.py -y -n -bp generated/tiles.patt res/tiles.png")
sh("python3 ../../tools/imgtool.py -y -n -bp generated/font.patt res/font.png")
sh("python3 ../../tools/imgtool.py -y -bp generated/title.patt -bn generated/title.name -ba generated/title.attr res/title.png")
sh("python3 ../../tools/patttool.py generated/ generated/patt.s")
sh("python3 ../../nkasm -v -I ../../nk -I generated -b 1 -o 2048.nes -d -l 2048.lst 2048.s")
