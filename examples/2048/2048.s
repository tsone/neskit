;--
; nk module includes.
;--
; Configure DPCM samples origin.
.INCLUDE    "modules/nk_joypad.s"
.INCLUDE    "modules/nk_yalz.s"
.INCLUDE    "modules/nk_random.s"
.INCLUDE    "modules/nk_w.s"
.INCLUDE    "modules/nk_bcd.s"
.INCLUDE    "modules/nk_skin.s"
.INCLUDE    "modules/nk_audio.s"
.INCLUDE    "nk.s"

;--
; Game constants.
;--
TILE_EMPTY      = $00

; Game states.
STATE_TITLE     = 0*2
STATE_MOVE      = 1*2
STATE_COMBINE   = 2*2
STATE_INPUT     = 3*2 ; =Main state.

; Field iteration modes.
ITER_MOVE        = 0
ITER_COMBINE     = 1

NUM_SCORE_DIGITS = 6 ; Number of digits in score counters.
MAX_TILE = 13 ; Maximum tile to get in the game, 8192.

; Name table addresses (PPU).
GRID_NAME_BASE      = $2800
SCORE_STR_BASE      = GRID_NAME_BASE+4*32+4
SCORE_DEC_BASE      = SCORE_STR_BASE+6
HIGH_STR_BASE       = GRID_NAME_BASE+4*32+17
HIGH_DEC_BASE       = HIGH_STR_BASE+5
SCORE_ADD_DEC_BASE  = SCORE_DEC_BASE+1*32

GRID_BASE = GRID_NAME_BASE+8*32+8
GRID_TILES_ATTR_BASE = GRID_NAME_BASE+$3C0+2*8+2

; Title screen constants.
TITLE_SCROLL = 0 ; Y-scroll for title.
MENU_X              = ((32/2)-4)*8
MENU_Y              = 17*8 ; NOTE: Must not be multiple of CURSOR_MOVE!
CURSOR_MOVE         = 2*8 ; in px
TITLE_NAME_BASE     = $2000
NEW_GAME_STR_BASE   = TITLE_NAME_BASE+(MENU_Y/8+0)*32+MENU_X/8
CONTINUE_STR_BASE   = TITLE_NAME_BASE+(MENU_Y/8+2)*32+MENU_X/8
CREDITS_STR_BASE    = TITLE_NAME_BASE+23*32

; Gamefield constants.
FIELD_SCROLL = 240+8 ; Y-scroll for field.
FIELD_SIZE = 4*4
; Delay for hiding the white tile (for combine and new tile add).
BLINK_HIDE_DELAY        = 4     ; in frames
; Delay for hiding score add string.
SCORE_ADD_HIDE_DELAY    = 80    ; in frames

; nk skin indexes to use for which skin.
CURSOR_SKIN_IDX     = 0
BLINK_SKIN_IDX      = 0
COMBINE_SKIN_IDX    = 1 ; NOTE! MUST BE THIS INDEX (4 skins).
POPUP_SKIN_IDX      = 1+4
NUM_SKIN            = 1+4+1

; Special msg indexes.
START_MSG_IDX   = 5
OOM_MSG_IDX     = 6

; Song part IDs.
THEME_SONG_ID   = 0
OOM_SONG_ID     = 1
MOVE_SFX_ID     = 2
COMBINE_SFX_ID  = 3
PAUSE_SFX_ID    = 4
CONTINUE_SFX_ID = 5
CURSOR_SFX_ID   = 6
POPUP_SFX_ID    = 7
NEW_GAME_SFX_ID = POPUP_SFX_ID
OOM_SFX_ID      = 8


; Convert 6 BCD to decimals.
.MACRO BCD_TO_DEC6 bcd_addr_, dec_addr_
    CLC
    LDX     #0
    LDA     bcd_addr_+5
    BNE     @to_dec6
    LDA     bcd_addr_+4
    BNE     @to_dec5
    LDA     bcd_addr_+3
    BNE     @to_dec4
    LDA     bcd_addr_+2
    BNE     @to_dec3
    LDA     bcd_addr_+1
    BNE     @to_dec2
    LDA     bcd_addr_+0
    BCC     @to_dec1    ; JMP optimization.
@to_dec6:
    ADC     #DECIMAL_BASE
    STA     dec_addr_, X
    INX
    LDA     bcd_addr_+4
@to_dec5:
    ADC     #DECIMAL_BASE
    STA     dec_addr_, X
    INX
    LDA     bcd_addr_+3
@to_dec4:
    ADC     #DECIMAL_BASE
    STA     dec_addr_, X
    INX
    LDA     bcd_addr_+2
@to_dec3:
    ADC     #DECIMAL_BASE
    STA     dec_addr_, X
    INX
    LDA     bcd_addr_+1
@to_dec2:
    ADC     #DECIMAL_BASE
    STA     dec_addr_, X
    INX
    LDA     bcd_addr_+0
@to_dec1:
    ADC     #DECIMAL_BASE
    STA     dec_addr_, X
.ENDMACRO

; PPU address setter without PPU address latch reset.
.MACRO PPU_ORG addr
    LDA     #>(addr)
    STA     $2006           ; PPU write address, high 6 bits
    LDA     #<(addr)
    STA     $2006           ; PPU write address, low 8 bits
.ENDMACRO

; Draw one grid attribute row.
.MACRO DRAW_ATTR_ROW row
    PPU_ORG  GRID_TILES_ATTR_BASE+(8*row)
    LDA     z_attr+(4*row)+0
    STA     $2007
    LDA     z_attr+(4*row)+1
    STA     $2007
    LDA     z_attr+(4*row)+2
    STA     $2007
    LDA     z_attr+(4*row)+3
    STA     $2007
.ENDMACRO

; Draw decimal number of 6 digits.
.MACRO DRAW_DEC6 dec_addr_
    LDA     dec_addr_+0
    STA     $2007
    LDA     dec_addr_+1
    STA     $2007
    LDA     dec_addr_+2
    STA     $2007
    LDA     dec_addr_+3
    STA     $2007
    LDA     dec_addr_+4
    STA     $2007
    LDA     dec_addr_+5
    STA     $2007
.ENDMACRO

; Charmap to be used.
.CHARMAP    " !\"#$%&'()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[\\]^_"
.CHARBASE   0
DECIMAL_BASE = (48-32)

;--
; Zeropage variables.
;--
.SEGMENT    "ZEROPAGE"
; Tiles and attrs for drawing tiles quick to VRAM.
z_tiles:            .RES 4*4*2*2
z_attr:             .RES 4*4
z_score_bcd:        .RES NUM_SCORE_DIGITS
z_score_add_bcd:    .RES NUM_SCORE_DIGITS
z_iter_inner_incr:  .RES 1  ; Interate inner loop increment.
z_iter_outer_incr:  .RES 1  ; Interate outer loop increment.
z_draw_selector:    .RES 1  ; Select what to draw on VBLANK.
z_msg_ptr:          .RES 2  ; Pointer to current msg char.
z_score_dec:        .RES NUM_SCORE_DIGITS
z_score_high_dec:   .RES NUM_SCORE_DIGITS
z_score_add_dec:    .RES NUM_SCORE_DIGITS+1 ; +1 for "+" character

;--
; WRAM variables.
;--
.SEGMENT    "BSS"
v_field:            .RES FIELD_SIZE
v_move_dir:         .RES 1  ; Current move direction.

v_iter_mode:            .RES 1  ; Mode for field_iterate
; Inner loop state variable, reset to 0 for every outer loop.
v_iter_inner_state:     .RES 1
v_iter_move_count:      .RES 1  ; Number of move ops done.
v_iter_combine_count:   .RES 1  ; Number of combine ops done.

v_state:            .RES 1  ; Current state.
v_state_prev:       .RES 1  ; Previous state.

v_field_highest:    .RES 1  ; Highest tile number in field.

v_high_bcd:         .RES NUM_SCORE_DIGITS

v_blink_fc:         .RES 1
v_score_add_fc:     .RES 1

v_msg_fc:           .RES 1  ; Frame counter for delaying msg.
v_msg_idx:          .RES 1  ; Char index to draw on screen.

v_find_idx_tab:     .RES FIELD_SIZE ; Shuffled field tile indexes.


;--
; Code (PRG-ROM).
;--
.SEGMENT    "CODE"

RESET:
    NK_CTRL_CONFIG  NK_CTRL_NMI | NK_CTRL_8x16_SPR | NK_CTRL_SPR_HIGH
    NK_MASK_CONFIG  NK_MASK_BG_SHOW | NK_MASK_SPR_SHOW | NK_MASK_BG_NOCLIP | NK_MASK_SPR_NOCLIP

    ; Init shuffled index tab.
    LDX     #FIELD_SIZE-1
@find_tab_init_loop:
    TXA
    STA     v_find_idx_tab, X
    DEX
    BPL     @find_tab_init_loop

    ; Set palette.
    NK_PAL_LOAD grid_pale

    ; Decode patts.
    LDA     #GEN_PATT_FONT
    JSR     nk_patt_load
    LDA     #GEN_PATT_GRID
    JSR     nk_patt_load
    LDA     #GEN_PATT_TITLE
    JSR     nk_patt_load
    LDA     #GEN_PATT_TILES
    JSR     nk_patt_load
    LDA     #GEN_PATT_SKIN_ALL|NK_PATT_SPR_VRAM
    JSR     nk_patt_load

    ; Decode BG name & attr.
    NK_PTR_SET  grid_name
    NK_PPU_ORG  GRID_NAME_BASE
    LDX     #GEN_PATT_GRID
    JSR     nk_patt_name_decode
    NK_YALZ_DECODE_CONTINUE    GRID_NAME_BASE+$3C0      ; grid attr

    ; Decode title name & attr.
    LDX     #GEN_PATT_TITLE
    NK_PPU_ORG  TITLE_NAME_BASE
    JSR     nk_patt_name_decode
    NK_YALZ_DECODE_CONTINUE    TITLE_NAME_BASE+$3C0     ; title attr

    ; Set vertical scroll.
    LDA     #TITLE_SCROLL
    LDX     #0
    NK_PPU_SCROLL_SET_Y

    ; Set patt for skins.
    LDX     #NUM_SKIN-1
    LDA     #GEN_PATT_SKIN_ALL
@skin_patt_loop:
    STA     v_nk_skin_patt, X
    DEX
    BPL     @skin_patt_loop

    ; Init cursor skin.
    LDA     #MENU_X
    STA     v_nk_skin_x+CURSOR_SKIN_IDX
    LDA     #MENU_Y
    STA     v_nk_skin_y+CURSOR_SKIN_IDX
    LDA     #GEN_SKIN_ALL_CURSOR
    STA     v_nk_skin_anim+CURSOR_SKIN_IDX
    LDA     #0
 	STA     v_nk_skin_fc+CURSOR_SKIN_IDX

 	; Init banner skin.
 	LDA     #(256/2)-1
 	STA     v_nk_skin_x+POPUP_SKIN_IDX
 	LDA     #0
 	STA     v_nk_skin_y+POPUP_SKIN_IDX
    LDA     #GEN_SKIN_ALL_SUPER
    STA     v_nk_skin_anim+POPUP_SKIN_IDX
    LDA     #0
 	STA     v_nk_skin_fc+POPUP_SKIN_IDX

    ; Draw score strings.
    NK_PTR_SET2  c_score_pstr
    LDY     #0
    NK_PPU_ORG  SCORE_STR_BASE
    JSR     nk_ppu_put_pstr
    NK_PPU_ORG  HIGH_STR_BASE
    JSR     nk_ppu_put_pstr

    ; Draw title strings.
    NK_PTR_SET2  c_new_game_pstr
    LDY     #0
    NK_PPU_ORG  NEW_GAME_STR_BASE
    JSR     nk_ppu_put_pstr

    NK_PTR_SET2  c_credits_pstr
    LDY     #0
    NK_PPU_ORG  CREDITS_STR_BASE
    JSR     nk_ppu_put_pstr

    NK_AUDIO_SET_FLAGS  NK_AUDIO_SFX_NO_LOOP

state_change_nop:
    RTS

UPDATE:
    NK_AUDIO_UPDATE

    NK_JOYPAD_READ  0

    ; Shuffle tile index table.
    NK_RANDOM_SHUFFLE   v_find_idx_tab, FIELD_SIZE

    ; Animate popup.
    LDA     v_nk_skin_y+POPUP_SKIN_IDX
    CMP     #240
    BCS     @skip_popup_anim
    PHA
    SEC
    SBC     #240/2
    NK_ABS
    LSR     A
    LSR     A
    LSR     A
    LSR     A
    TAX
    PLA
    SEC
    SBC     c_popup_speed_tab, X
    STA     v_nk_skin_y+POPUP_SKIN_IDX
@skip_popup_anim:

    LDA     #STATE_TITLE
    CMP     v_state
    BEQ     @skip_hide_blink

    ; Handle blink fc.
    LDA     v_blink_fc
    DEC     v_blink_fc
    BNE     @skip_hide_blink
    LDA     #0
@hide_blink:
    STA     v_nk_skin_y+BLINK_SKIN_IDX
    STA     v_nk_skin_y+COMBINE_SKIN_IDX
    STA     v_nk_skin_y+COMBINE_SKIN_IDX+1
    STA     v_nk_skin_y+COMBINE_SKIN_IDX+2
    STA     v_nk_skin_y+COMBINE_SKIN_IDX+3
@skip_hide_blink:

    ; Handle score_add fc.
    LDA     v_score_add_fc
    BEQ     @clear_score_add
    DEC     v_score_add_fc
    BNE     @skip_clear_score_add
@clear_score_add:
    LDA     #0
    LDX     #NUM_SCORE_DIGITS     ; Clear score add dec.
@clear_score_add_loop:
    STA     z_score_add_dec, X
    DEX
    BPL     @clear_score_add_loop
@skip_clear_score_add:

    JSR     state_update
    JSR     convert_score
    JSR     convert_field_to_tiles
    JSR     nk_skin_sort_all    ; TODO: move to nk?
    JSR     nk_skin_update_all  ; TODO: move to nk?
    JMP     nk_anim_hide_unused ; TODO: move to nk?

;--
; Change the state.
; Stores also previous state.
; Inputs:
;   A: New state.
; Modifies:
;   ?
;--
state_update:
    LDY     v_state
    NK_RTS_JUMP c_state_update_jsr_tab

;--
; Change the state.
; Stores also previous state.
; Inputs:
;   A: New state.
; Modifies:
;   ?
;--
state_change:
    PHA
    LDA     v_state
    STA     v_state_prev
    PLA
    STA     v_state
    TAY
    NK_RTS_JUMP c_state_change_jsr_tab

c_state_update_jsr_tab:
    .WORD   state_update_title-1    ; STATE_TITLE
    .WORD   state_update_move-1     ; STATE_MOVE
    .WORD   state_update_combine-1  ; STATE_COMBINE
    .WORD   state_update_input-1    ; STATE_INPUT
c_state_change_jsr_tab:
    .WORD   state_change_title-1    ; STATE_TITLE
    .WORD   state_change_nop-1      ; STATE_MOVE
    .WORD   state_change_nop-1      ; STATE_COMBINE
    .WORD   state_change_input-1    ; STATE_INPUT


state_change_title:
    NK_AUDIO_SET_FLAGS  NK_AUDIO_BGM_PAUSE
    NK_AUDIO_PLAY       NK_AUDIO_PLAYER_SFX, PAUSE_SFX_ID

    ; Show cursor.
    LDA     #MENU_X
    STA     v_nk_skin_x+CURSOR_SKIN_IDX
    LDA     #MENU_Y+CURSOR_MOVE
    STA     v_nk_skin_y+CURSOR_SKIN_IDX
    LDX     #CURSOR_SKIN_IDX
    LDA     #GEN_SKIN_ALL_CURSOR|NK_SKIN_CHANGE_FORCE
    JSR     nk_skin_change_anim

    ; Hide other skins.
    LDA     #0
    STA     v_nk_skin_y+COMBINE_SKIN_IDX
    STA     v_nk_skin_y+COMBINE_SKIN_IDX+1
    STA     v_nk_skin_y+COMBINE_SKIN_IDX+2
    STA     v_nk_skin_y+COMBINE_SKIN_IDX+3
 	STA     v_nk_skin_y+POPUP_SKIN_IDX
    RTS

state_update_title:
    LDA     #TITLE_SCROLL
    LDX     #0
    NK_PPU_SCROLL_SET_Y

    LDA     v_nk_joypad_pressed
    CMP     #NK_JOYPAD_START
    BNE     @skip_start_field
;@start_field:
    LDA     #CURSOR_MOVE
    BIT     v_nk_skin_y+CURSOR_SKIN_IDX
    BNE     @select_continue

;@select_new_game:
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_SFX, NEW_GAME_SFX_ID
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_BGM, THEME_SONG_ID
    NK_AUDIO_UNSET_FLAGS NK_AUDIO_BGM_PAUSE
    JSR     field_init
    LDA     #STATE_INPUT
    JMP     state_change

@select_continue:
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_SFX, CONTINUE_SFX_ID
    NK_AUDIO_UNSET_FLAGS NK_AUDIO_BGM_PAUSE
    LDA     #STATE_INPUT
    JMP     state_change
@skip_start_field:

    LDA     v_state_prev
    CMP     #STATE_TITLE
    BEQ     @skip_move_cursor

    LDA     v_nk_joypad_pressed
    CMP     #NK_JOYPAD_SELECT
    BNE     @skip_move_cursor
;@move_cursor:
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_SFX, CURSOR_SFX_ID

    LDA     v_nk_skin_y+CURSOR_SKIN_IDX
    EOR     #CURSOR_MOVE
    STA     v_nk_skin_y+CURSOR_SKIN_IDX
@skip_move_cursor:
    RTS


state_update_move:
    LDA     v_iter_move_count   ; Remember previous count.
    PHA
    LDA     #ITER_MOVE
    STA     v_iter_mode
    JSR     field_iterate
    PLA                         ; Compare with previous count.
    CMP     v_iter_move_count
    BEQ     @no_change
;@change:
    RTS                         ; Field changed, repeat move state.

@no_change:
    LDA     #STATE_COMBINE
    CMP     v_state_prev
    BNE     @set_to_combine
;@set_to_input:
    LDA     #STATE_INPUT
@set_to_combine:
    JMP     state_change


state_update_combine:
    LDA     #ITER_COMBINE
    STA     v_iter_mode
    JSR     field_iterate
    LDA     #STATE_MOVE
    JMP     state_change

state_change_input:
    LDA     #STATE_TITLE
    CMP     v_state_prev
    BNE     @skip_hide_cursor
    LDA     #0                  ; Hide cursor/blink.
    STA     v_nk_skin_y+CURSOR_SKIN_IDX
@skip_hide_cursor:

    LDA     v_iter_combine_count
    BNE     @made_combines          ; Player has combined tiles.
    LDA     v_iter_move_count
    BEQ     @no_changes             ; Player made no moves or combines.
;@made_moves:
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_SFX, MOVE_SFX_ID

@epilog:
    JSR     field_new_tile          ; Add new tile and reset counters.
    LDA     #0
    STA     v_iter_move_count
    STA     v_iter_combine_count

    JSR     field_check_oom         ; Check for OOM after move or combine.
    BEQ     @skip_oom
;@oom:
    LDX     #OOM_MSG_IDX            ; Show OOM popup/msg.
    JSR     popup_msg_begin
    NK_AUDIO_PLAY  NK_AUDIO_PLAYER_BGM, OOM_SONG_ID ; Play OOM song and SFX.
    NK_AUDIO_PLAY  NK_AUDIO_PLAYER_SFX, OOM_SFX_ID
@skip_oom:

@no_changes:
    RTS

@made_combines:
    ; Play combine SFX if popup is not just started.
    LDA     v_nk_skin_y+POPUP_SKIN_IDX
; TODO: constant for popup start y
    CMP     #240-129    ; This works by overflowing subtraction.
    BPL     @skip_play_combine_sfx  ; If N=1 then play combine SFX.
;@play_combine_sfx:
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_SFX, COMBINE_SFX_ID
@skip_play_combine_sfx:

    NK_PTR_SET2 z_score_add_bcd  ; Accumulate score from score_add.
    LDX     #z_score_bcd
    LDY     #NUM_SCORE_DIGITS
    JSR     nk_bcd_add

    LDA     #$0B                ; Set "+" char
    STA     z_score_add_dec
    LDA     #0                  ; Clear add dec.
    LDX     #NUM_SCORE_DIGITS-1
@clear_add_dec_loop:
    STA     z_score_add_dec+1, X
    DEX
    BPL     @clear_add_dec_loop

    BCD_TO_DEC6 z_score_add_bcd, z_score_add_dec+1

    LDA     #0                  ; Clear add BCD.
    LDX     #NUM_SCORE_DIGITS-1
@clear_add_bcd_loop:
    STA     z_score_add_bcd, X
    DEX
    BPL     @clear_add_bcd_loop

    LDA     #SCORE_ADD_HIDE_DELAY ; Set score hide delay fc.
    STA     v_score_add_fc
    JMP     @epilog             ; TODO: JMP optimization?


state_update_input:
    NK_PPU_ENABLE
    LDA     #FIELD_SCROLL       ; Set field scroll
    LDX     #0
    NK_PPU_SCROLL_SET_Y

    LDA     v_nk_joypad_pressed ; Get move direction
    JSR     nk_joypad_get_dir
    BMI     @no_move

;@move:
    STX     v_move_dir          ; Store move direction.
    LDA     #STATE_MOVE         ; Change to MOVE state.
    JSR     state_change
    BPL     @end

@no_move:
    LDA     v_nk_joypad_pressed
    CMP     #NK_JOYPAD_START
    BNE     @skip_change_to_title
;@change_to_title:
    LDA     #STATE_TITLE
    JSR     state_change
    BPL     @end
@skip_change_to_title:
@end:
    RTS


;--
; Initialize the game.
; Clear the field, add two tiles and reset score.
; Modifies:
;   A,X
;--
field_init:
    ; Reset variables.
    LDA     #1
    STA     v_msg_fc
    LDA     #0
    STA     v_score_add_fc
    STA     v_field_highest

    ; Zero digits.
    LDX     #NUM_SCORE_DIGITS-1
@reset_loop:
    STA     z_score_bcd, X
    STA     z_score_add_bcd, X
    STA     z_score_dec, X      ; NOTE: Charcode 0 must be space (blank)!
    DEX
    BPL     @reset_loop

    LDX     #START_MSG_IDX      ; Begin start popup/msg.
    JSR     popup_msg_begin

    LDX     #FIELD_SIZE-1
    LDA     #TILE_EMPTY
@clear_loop:
    STA     v_field, X
    DEX
    BPL     @clear_loop
    JSR     field_new_tile      ; Add two tiles.
    JSR     field_new_tile

    NK_PPU_DISABLE
    NK_PPU_ORG  $2B40           ; TODO: constant for msg PPU addr
    LDX     #32
    LDA     #0
@msg_clear_loop:
    STA     $2007
    DEX
    BNE     @msg_clear_loop

    RTS


;--
; Move tiles in field by one increment (see below).
; Inputs:
;   z_iter_inner_incr: Inner increment (=offset per move).
;   z_iter_outer_incr: Outer increment (=offset per row/column).
;   X: Initial dst index.
; Modifies:
;   C,A,X,Y,t2,t3
;--
field_iterate_one:
    TXA                 ; Init the src index.
    CLC
    ADC     z_iter_inner_incr
    TAY

    LDA     #4          ; Init outer counter ->t2
    STA     t2
@outer_loop:
    LDA     #3          ; Init inner counter ->t3
    STA     t3
    LDA     #0          ; Reset inner state to 0.
    STA     v_iter_inner_state

@inner_loop:
    ; Branch to inner broutine depending on mode.
    LDA     v_iter_mode
    BEQ     @inner_move
    BNE     @inner_combine

@inner_done:
    TYA                 ; Inner increment src
    CLC
    ADC     z_iter_inner_incr
    TAY
    TXA                 ; Inner increment dst
    CLC
    ADC     z_iter_inner_incr
    TAX

    DEC     t3
    BNE     @inner_loop

    TYA                 ; Outer increment src
    CLC
    ADC     z_iter_outer_incr
    TAY
    TXA                 ; Outer increment dst
    CLC
    ADC     z_iter_outer_incr
    TAX

    DEC     t2
    BNE     @outer_loop
    RTS

@inner_move:
    LDA     v_field, X
    BNE     @inner_done ; No move if dst tile is occupied (=non-zero).
    LDA     v_field, Y
    BEQ     @inner_done ; No move if both tiles are empty (=zero).
;@move:
    INC     v_iter_move_count
    STA     v_field, X
    LDA     #0
    STA     v_field, Y
    BEQ     @inner_done ; JMP optimization.

@inner_combine:
    LDA     v_iter_inner_state
    BNE     @inner_done ; No combine if did it was already done for this row/column.
    LDA     v_field, X
    BEQ     @inner_done ; No combine if dst is empty (=zero).
    CMP     v_field, Y
    BNE     @inner_done ; No combine if src != dst.
    CMP     #MAX_TILE
    BCS     @inner_done ; No combine if dst or src >MAX_TILE
;@combine:
    STY     t4          ; Store Y
    STX     t5          ; Store X

    INC     v_iter_inner_state
    INC     v_iter_combine_count
    ADC     #1          ; Increase tile number (C=0).
    STA     v_field, X
    PHA                 ; Store new tile for later.
    CMP     v_field_highest
    BEQ     @skip_new_highest_tile
    BCC     @skip_new_highest_tile
;@new_higheset_tile:
    STA     v_field_highest         ; Save the new highest tile.
    SEC                             ; Check if we show msg and popup.
    SBC     #9                      ; We do this at "512" and after.
    BCC     @skip_new_highest_tile
;@show_msg_and_popup:
    TAX                             ; Tile number = msg index ->X
    JSR     popup_msg_begin         ; Show new highes tile popup/msg.
    NK_AUDIO_PLAY   NK_AUDIO_PLAYER_SFX, POPUP_SFX_ID ; Play sound effect.

@skip_new_highest_tile:
    LDA     #0          ; Clear other tile.
    LDY     t4          ; Restore Y (other tile idx)
    STA     v_field, Y

    ; Calculate score table ptr and add the score.
    TAX                 ; $00 ->X
    STA     t1          ; $00 ->t1
    PLA
    SEC
    SBC     #2          ; A = A - 2
    ASL     A
    ASL     A
    STA     t0          ; Offset in table ->t0
    NK_W_ADD c_score_tab
    LDX     #z_score_add_bcd
    LDY     #4          ; Table has 4-digit values.
    JSR     nk_bcd_add

    ; Set skin animation for combine (blink).
    LDY     t5
    LDX     v_iter_combine_count ; Skin idx 1..4
    JSR     set_blink_pos
    LDA     #GEN_SKIN_ALL_BLINK|NK_SKIN_CHANGE_FORCE
    JSR     nk_skin_change_anim

    LDY     t4          ; Restore Y
    LDX     t5          ; Restore X
    JMP     @inner_done ; TODO: optimize JMP?

;--
; Show popup and begin displaying msg.
; Inputs:
;   X: Msg/popup index.
; Modifies:
;   A,X,Y
;--
popup_msg_begin:
    LDA     c_msg_lo_tab, X         ; Set the pointer.
    STA     z_msg_ptr+0
    LDA     c_msg_hi_tab, X
    STA     z_msg_ptr+1
    LDY     #0                      ; Reset char idx.
    STY     v_msg_idx
    INY                             ; Reset message fc (=1).
    STY     v_msg_fc

    LDA     c_popup_anim_tab, X     ; Set popup skin animation.
    LDX     #POPUP_SKIN_IDX
    JSR     nk_skin_change_anim
    LDA     #239                    ; Start to animate the popup.
    STA     v_nk_skin_y+POPUP_SKIN_IDX
    RTS

;--
; Iterate field with set mode.
; Modifies:
;   C,A,X,Y,t0,t1,t2,t3,t4,t5
;--
field_iterate:
    LDY     v_move_dir
    LDA     c_iter_inner_tab, Y ; Setup move variables.
    STA     z_iter_inner_incr
    LDA     c_iter_outer_tab, Y
    STA     z_iter_outer_incr
    LDX     c_iter_origin_tab, Y
    JMP     field_iterate_one

;--
; Find empty tile in field and return its index.
; Uses the shuffled idx table to find empty tile in at most 16 steps.
; Outputs:
;   Y: Found empty tile index.
;   C: 1=empty tile found, 0=not found (field is full).
; Modifies:
;   C,A,X,Y
;--
field_find_empty:
    SEC                         ; C=1, empty found (default).
    LDX     #FIELD_SIZE-1
@search_loop:
    LDY     v_find_idx_tab, X   ; Test idx ->Y
    LDA     v_field, Y          ; End if empty tile found.
    BEQ     @found
    DEX
    BPL     @search_loop
    CLC                         ; C=0, no empty tiles, field is full.
@found:
    RTS

;--
; Add a new tile randomly on any empty tile.
; Modifies:
;   C,A,X,Y,t0
;--
field_new_tile:
    JSR     field_find_empty
    BCC     @exit               ; C=0, should never happen...
    LDX     #1                  ; 90% for "2" tile, 10% for "4".
    NK_RANDOM                   ; Get uniform random byte.
    CMP     #255/10             ; 10% * 255 = 255 / 10.
    BCS     @add_2
;@add_4:
    LDX     #2                  ; Tile "4".
@add_2:
    TXA
    STA     v_field, Y

    ; Set blink animation.
    LDX     #BLINK_SKIN_IDX
    JSR     set_blink_pos
    LDA     #GEN_SKIN_ALL_BLINK|NK_SKIN_CHANGE_FORCE
    JSR     nk_skin_change_anim

    ; Set blink hide delay to fc.
    LDA     #BLINK_HIDE_DELAY
    STA     v_blink_fc
@exit:
    RTS

;--
; Check out of moves (OOM) condition: no empty or equal adjacent tiles.
; Outputs:
;   Z: 0=OOM, 1=not OOM.
;--
field_check_oom:
    LDX     #FIELD_SIZE-1
@loop:
    LDA     v_field, X
    BEQ     @not_oom

    CPX     #4
    BCC     @skip_vertical
    CMP     v_field-4, X
    BEQ     @not_oom
@skip_vertical:

    TXA
    AND     #$03
    BEQ     @skip_horizontal
    LDA     v_field, X
    CMP     v_field-1, X
    BEQ     @not_oom
@skip_horizontal:

    DEX
    BPL     @loop
@not_oom:
    RTS

;--
; Calculate and set a blink tile position from tile index.
; Inputs:
;   X: Skin idx.
;   Y: Tile idx.
;--
set_blink_pos:
    TYA
    AND     #$03
    ASL     A
    ASL     A
    ASL     A
    ASL     A
    ASL     A
    ADC     #8*8+16-4
    STA     v_nk_skin_x, X

    TYA
    AND     #$0C
    ASL     A
    ASL     A
    ASL     A
    ADC     #5*8+2*8-1+16-4
    STA     v_nk_skin_y, X
    RTS

;--
; Convert score and high score digits from BCD to decimal.
; Updates z_score_dec and z_score_high_dec.
;--
convert_score:
    BCD_TO_DEC6 z_score_bcd, z_score_dec

    ; Update high score if needed.
    NK_PTR_SET2 v_high_bcd
    LDX     #z_score_bcd
    LDY     #NUM_SCORE_DIGITS
    JSR     nk_bcd_cmp
    BEQ     @convert_high_score ; In case of a new game.
    BCC     @convert_high_score
    RTS

@convert_high_score:
    LDX     #NUM_SCORE_DIGITS-1
@high_loop:
    LDA     z_score_bcd, X
    STA     v_high_bcd, X
    LDA     z_score_dec, X
    STA     z_score_high_dec, X
    DEX
    BPL     @high_loop
    RTS


;--
; Convert field state to tile graphics.
; This updates the z_tiles and z_attr.
; Modifies:
;   C,A,X,Y,t0
;--
convert_field_to_tiles:
    NK_PATT_GET_LOAD_OFFS   GEN_PATT_TILES ; Tiles load offs ->t0
    STA     t0
    LDX     #FIELD_SIZE-1
@tiles_loop:
    LDA     v_field, X
    TAY                     ; Store for later ->Y

    ASL     A               ; A*8+base.
    ASL     A
    ASL     A
    ADC     t0              ; Add tiles load offs.
    ; Set the tile (upper and lower starting idx for the tile).
    STA     z_tiles, X
    ADC     #4
    STA     z_tiles+FIELD_SIZE, X

    TYA                     ; Get field attr.
    AND     #3
    TAY
    LDA     c_field_to_attr_tab, Y
    STA     z_attr, X

    DEX
    BPL     @tiles_loop

    ; Copy tile top/bottom part.
    LDX     #FIELD_SIZE-1
    CLC
    LDA     t0
    ADC     #4*2*(MAX_TILE+1)
    STA     t0
@loop:
    LDA     v_field, X
    BEQ     @skip
    LDA     #6
@skip:
    ADC     t0
    STA     z_tiles+2*FIELD_SIZE, X
    ADC     #3
    STA     z_tiles+3*FIELD_SIZE, X
    DEX
    BPL     @loop
    RTS

copy_row_vram:
    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007

    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007

    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007

    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007
    INX
    STX     $2007

    RTS

copy_row_vram_alt:
    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    STX     $2007
    INX
    STX     $2007

    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    STX     $2007
    INX
    STX     $2007

    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    STX     $2007
    INX
    STX     $2007

    LDX     z_tiles, Y
    INY
    STX     $2007
    INX
    STX     $2007
    STX     $2007
    INX
    STX     $2007

    RTS


VBLANK:
    LDA     #STATE_TITLE
    CMP     v_state
    BNE     @vblank_field

;@vblank_title:
    CMP     v_state_prev
    BEQ     @skip_draw_continue
;@draw_continue:
    NK_PTR_SET2  c_continue_pstr
    LDY     #0
    NK_PPU_ORG  CONTINUE_STR_BASE
    JSR     nk_ppu_put_pstr
@skip_draw_continue:
    RTS

@vblank_field:
    LDA     z_draw_selector
    EOR     #$80
    STA     z_draw_selector
    BMI     @draw_top_half
    JMP     @draw_bottom_half

@draw_top_half:
    ; Draw score.
    PPU_ORG SCORE_DEC_BASE
    DRAW_DEC6 z_score_dec
    ; Draw score add.
    PPU_ORG SCORE_ADD_DEC_BASE
    DRAW_DEC6 z_score_add_dec

    ; Copy field top half.
    DRAW_ATTR_ROW 0
    DRAW_ATTR_ROW 1
    LDY     #0*FIELD_SIZE
    PPU_ORG  GRID_BASE+1*32
    JSR     copy_row_vram
    PPU_ORG  GRID_BASE+5*32
    JSR     copy_row_vram
    LDY     #1*FIELD_SIZE
    PPU_ORG  GRID_BASE+2*32
    JSR     copy_row_vram
    PPU_ORG  GRID_BASE+6*32
    JSR     copy_row_vram
    LDY     #2*FIELD_SIZE
    PPU_ORG  GRID_BASE+0*32
    JSR     copy_row_vram_alt
    PPU_ORG  GRID_BASE+4*32
    JSR     copy_row_vram_alt
    LDY     #3*FIELD_SIZE
    PPU_ORG  GRID_BASE+3*32
    JSR     copy_row_vram_alt
    PPU_ORG  GRID_BASE+7*32
    JMP     copy_row_vram_alt

@draw_bottom_half:
    ; Draw high score.
    PPU_ORG HIGH_DEC_BASE
    DRAW_DEC6 z_score_high_dec

    JSR     draw_msg_char

    ; Copy field bottom half.
    DRAW_ATTR_ROW 2
    DRAW_ATTR_ROW 3
    LDY     #0*FIELD_SIZE+FIELD_SIZE/2
    PPU_ORG  GRID_BASE+9*32
    JSR     copy_row_vram
    PPU_ORG  GRID_BASE+13*32
    JSR     copy_row_vram
    LDY     #1*FIELD_SIZE+FIELD_SIZE/2
    PPU_ORG  GRID_BASE+10*32
    JSR     copy_row_vram
    PPU_ORG  GRID_BASE+14*32
    JSR     copy_row_vram
    LDY     #2*FIELD_SIZE+FIELD_SIZE/2
    PPU_ORG  GRID_BASE+8*32
    JSR     copy_row_vram_alt
    PPU_ORG  GRID_BASE+12*32
    JSR     copy_row_vram_alt
    LDY     #3*FIELD_SIZE+FIELD_SIZE/2
    PPU_ORG  GRID_BASE+11*32
    JSR     copy_row_vram_alt
    PPU_ORG  GRID_BASE+15*32
    JMP     copy_row_vram_alt

draw_msg_char:
    LDY     #0
    LDA     (z_msg_ptr), Y
    TAX                         ; Char ->X
    LDA     v_msg_idx           ; Char index ->A
    AND     #31
    BNE     @draw_char
    DEC     v_msg_fc
    BNE     @end

@draw_char:
    LDY     #$2B                ; TODO: constant for base addr high
    STY     $2006
    ORA     #$40                ; TODO: constant for base addr low
    STA     $2006
    TXA
    CLC
    BPL     @not_emptying
;@emptying:
    ASL     A                   ; Shift out sentinel bit ->C
@not_emptying:
    STA     $2007               ; Write the char.
    INC     v_msg_idx
    BCS     @end                ; If sentinel (C=1), skip ptr increment.
    NK_W_INC    z_msg_ptr
@end:
    RTS

c_iter_inner_tab:
    .BYTE   4, 256-4, 1, 256-1
c_iter_outer_tab:
    .BYTE   256-3*4+1, 3*4-1, 1, 256-1
c_iter_origin_tab:
    .BYTE   0, FIELD_SIZE-1, 0, FIELD_SIZE-1

c_field_to_attr_tab:
    .BYTE   $00, $55, $AA, $FF

c_popup_speed_tab:
    .BYTE   1,1,1,2,3,4,6,8

c_score_tab:
    .BYTE   4,0,0,0
    .BYTE   8,0,0,0
    .BYTE   6,1,0,0
    .BYTE   2,3,0,0
    .BYTE   4,6,0,0
    .BYTE   8,2,1,0
    .BYTE   6,5,2,0
    .BYTE   2,1,5,0
    .BYTE   4,2,0,1
    .BYTE   8,4,0,2
    .BYTE   6,9,0,4
    .BYTE   2,9,1,8

c_popup_anim_tab:
    .BYTE   GEN_SKIN_ALL_GOOD|NK_SKIN_CHANGE_FORCE  ; 512
    .BYTE   GEN_SKIN_ALL_GREAT|NK_SKIN_CHANGE_FORCE ; 1024
    .BYTE   GEN_SKIN_ALL_SUPER|NK_SKIN_CHANGE_FORCE ; 2048
    .BYTE   GEN_SKIN_ALL_SUPER|NK_SKIN_CHANGE_FORCE ; 4096
    .BYTE   GEN_SKIN_ALL_SUPER|NK_SKIN_CHANGE_FORCE ; 8192
    .BYTE   GEN_SKIN_ALL_START|NK_SKIN_CHANGE_FORCE ; start
    .BYTE   GEN_SKIN_ALL_STUCK|NK_SKIN_CHANGE_FORCE ; oom

; NOTE: DO NOT CHANGE THE ORDER!
c_score_pstr:
    .PSTR   "score"
    .PSTR   "high"
c_new_game_pstr:
    .PSTR   "new game"
c_continue_pstr:
    .PSTR   "continue"
; Long fake PSTR for title screen lower part.
c_credits_pstr:
    .BYTE   4*32
    .STR    "         (c) 2014 tsone         "
    .STR    "                                "
    .STR    "  original by gabriele cirulli  "
    .STR    "   font and tiles by oerg866    "

; NOTE: Message strings must be aligned to 32 bytes.
; NOTE: Reset needs 2 bytes so last string is capped.
; Reset sentinel is $80.
c_msg_start:
    .STR    "        try to get 2048.        "
    .STR    "         you can do it.         "
    .BYTE   $80
c_msg_512:
    .STR    "          you got 512!          "
    .BYTE   $80
c_msg_1024:
    .STR    "       nice! you got 1024.      "
    .STR    "       continue for 2048.       "
    .BYTE   $80
c_msg_2048:
    .STR    "     awesome! you got 2048!     "
    .STR    "        congratulations!        "
    .STR    "      you've beat the game.     "
    .STR    "    continue for high score.    "
    .BYTE   $80
c_msg_4096:
    .STR    "       impressive!! 4096!       "
    .STR    "    ...but can you get 8192?    "
    .BYTE   $80
c_msg_8192:
    .STR    "  8192!!! you must be kidding.  "
    .STR    "   8192 is the maximum block.   "
    .STR    "    continue for more points.   "
    .BYTE   $80
c_msg_oom:
    .STR    "      sorry! out of moves!      "
    .STR    "    press start for new game.   "
    .BYTE   $80

c_msg_hi_tab:
    .BYTE   >c_msg_512, >c_msg_1024, >c_msg_2048, >c_msg_4096, >c_msg_8192
    .BYTE   >c_msg_start, >c_msg_oom
c_msg_lo_tab:
    .BYTE   <c_msg_512, <c_msg_1024, <c_msg_2048, <c_msg_4096, <c_msg_8192
    .BYTE   <c_msg_start, <c_msg_oom

.STAT audio_data
.INCLUDE "audio.s"
.ENDSTAT
.INCLUDE "all.gen.s"

.INCLUDE "patt.s"

;--
; Bank 0 stores game resources (music, sounds, graphics, etc.)
;--
.BANK 0

; NOTE: DO NOT CHANGE THE ORDER!
grid_pale:
    .INCBIN "night.pale"
grid_name:
    .INCBIN "grid.name.yalz"
grid_attr:
    .INCBIN "grid.attr.yalz"
title_name:
    .INCBIN "title.name.yalz"
title_attr:
    .INCBIN "title.attr.yalz"