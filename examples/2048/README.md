
# 2048 example

This example is based on the the popular [2048 puzzle game][1].
Rather than being a simple example, this is a full and complete game.
This example uses nk framework and tools.

Following nk modules are used: skin (for sprites), patt (for graphics
management), yalz (for graphics compression), random and bcd (for
displaying score in decimals).

All of the game code is in `2048.s` file, except for the generated
assembly in the `*.gen.s` files, and audio (music and sound effects)
data in `audio.s`.

Game resources are located in the `res/` directory. The audio is in
`res/2048.nms` which is made with [nemus][3]. Game graphics are in
`res/*.png`. neskit tools convert these to game data files and
assembly source. Data files will be generated in the `data/` directory.

To build, run `build.py`.


# Credits

[2048][1] was designed by Gabriele Cirulli. Font and tile graphics are
borrowed from [2048 Sega Megadrive][2] version of by oerg866.
Many thanks to you.


[1]: http://gabrielecirulli.github.io/2048/
[2]: http://titandemo.org/
[3]: https://bitbucket.org/tsone/nemus/


## License

2048 example is released under MIT license.

```
2048 - neskit example
---------------------

Copyright (c) 2013 Valtteri Heikkila

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
