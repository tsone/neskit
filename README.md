
# neskit

*neskit* is a homebrew game development kit for NES/FC. It contains a
framework library *nk*, a python-based assembler *nkasm*, and a set of
useful graphics conversion tools and examples.

**NOTE: All parts of neskit are work-in-progress. There be dragons.**

With netkit, you write games with 6502 assembly. The nkasm assembler builds
.nes ROM files directly, offering syntax similar to ca65. Please refer to
README.md files in each of neskit subdirectories.

Thank you for your interest in neskit and NES/FC homebrew in general.


## Setup

nkasm and the graphics conversion tools are python3 programs. You need
python and Pillow to use them.

1. **(This step is for Windows only)** Install python3
    - Download installer from: http://www.python.org/download/
    - Set Windows environment %PATH% to point to C:\Python3
      (or wherever python was installed)
2. Install pip (if not already installed)
    - See instructions here: https://pip.pypa.io/en/latest/installing.html
3. Install Pillow
    - Run on command prompt: `python3 -m pip install Pillow`


## License

neskit is released under MIT license.

```
neskit - NES/Famicom game development kit
-----------------------------------------

Copyright (c) 2013 Valtteri Heikkila

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
