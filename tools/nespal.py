"""neskit NES palette color mappings."""
__version__ = "0.9.1"
__copyright__ = 'Copyright (c) 2014 Valtteri Heikkila'
__license__ = """
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""

# NES colors RGB triplets ordered by their corresponding index.
# Don't remember where I got this data..
NES_COLORS = (
    (   124,    124,    124  ), # 0x00 (0)
    (   0,      0,      252  ), # 0x01 (1)
    (   0,      0,      188  ), # 0x02 (2)
    (   68,     40,     188  ), # 0x03 (3)
    (   148,    0,      132  ), # 0x04 (4)
    (   168,    0,      32   ), # 0x05 (5)
    (   168,    16,     0    ), # 0x06 (6)
    (   136,    20,     0    ), # 0x07 (7)
    (   80,     48,     0    ), # 0x08 (8)
    (   0,      120,    0    ), # 0x09 (9)
    (   0,      104,    0    ), # 0x0A (10)
    (   0,      88,     0    ), # 0x0B (11)
    (   0,      64,     88   ), # 0x0C (12)
    (   0,      0,      0    ), # 0x0D (13)
    (   0,      0,      0    ), # 0x0E (14)
    (   0,      0,      0    ), # 0x0F (15)
    (   188,    188,    188  ), # 0x10 (16)
    (   0,      120,    248  ), # 0x11 (17)
    (   0,      88,     248  ), # 0x12 (18)
    (   104,    68,     252  ), # 0x13 (19)
    (   216,    0,      204  ), # 0x14 (20)
    (   228,    0,      88   ), # 0x15 (21)
    (   248,    56,     0    ), # 0x16 (22)
    (   228,    92,     16   ), # 0x17 (23)
    (   172,    124,    0    ), # 0x18 (24)
    (   0,      184,    0    ), # 0x19 (25)
    (   0,      168,    0    ), # 0x1A (26)
    (   0,      168,    68   ), # 0x1B (27)
    (   0,      136,    136  ), # 0x1C (28)
    (   8,      8,      8    ), # 0x1D (29)
    (   0,      0,      0    ), # 0x1E (30)
    (   0,      0,      0    ), # 0x1F (31)
    (   248,    248,    248  ), # 0x20 (32)
    (   60,     188,    252  ), # 0x21 (33)
    (   104,    136,    252  ), # 0x22 (34)
    (   152,    120,    248  ), # 0x23 (35)
    (   248,    120,    248  ), # 0x24 (36)
    (   248,    88,     152  ), # 0x25 (37)
    (   248,    120,    88   ), # 0x26 (38)
    (   252,    160,    68   ), # 0x27 (39)
    (   248,    184,    0    ), # 0x28 (40)
    (   184,    248,    24   ), # 0x29 (41)
    (   88,     216,    84   ), # 0x2A (42)
    (   88,     248,    152  ), # 0x2B (43)
    (   0,      232,    216  ), # 0x2C (44)
    (   120,    120,    120  ), # 0x2D (45)
    (   0,      0,      0    ), # 0x2E (46)
    (   0,      0,      0    ), # 0x2F (47)
    (   252,    252,    252  ), # 0x30 (48)
    (   164,    228,    252  ), # 0x31 (49)
    (   184,    184,    248  ), # 0x32 (50)
    (   216,    184,    248  ), # 0x33 (51)
    (   248,    184,    248  ), # 0x34 (52)
    (   248,    164,    192  ), # 0x35 (53)
    (   240,    208,    176  ), # 0x36 (54)
    (   252,    224,    168  ), # 0x37 (55)
    (   248,    216,    120  ), # 0x38 (56)
    (   216,    248,    120  ), # 0x39 (57)
    (   184,    248,    184  ), # 0x3A (58)
    (   184,    248,    216  ), # 0x3B (59)
    (   0,      252,    252  ), # 0x3C (60)
    (   216,    216,    216  ), # 0x3D (61)
    (   0,      0,      0    ), # 0x3E (62)
    (   0,      0,      0    ), # 0x3F (63)
)


def _color_dist(a, b):
    # TODO: Should it use real distance instead of Manhattan?
    return sum(abs(x - y) for x, y in zip(a, b))
    #return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])

# return true if nes palette index is valid
def is_valid(idx):
    v = sum(NES_COLORS[idx])
    return v >= (3 * 8) and v <= (3 * 252)

# return valid nes palette index from given index
def fix_idx(idx):
    v = sum(NES_COLORS[idx])
    if v <= (3 * 8):
        return 0x1D # fixes invalid black
    elif v >= (3 * 252):
        return 0x30 # fixes invalid white
    else:
        return idx

# return valid rgb color triplet from the given one
def fix_rgb(s):
    if sum(s) <= (3 * 8):
        # fix invalid black
        return NES_COLORS[0x1D]
    elif sum(s) >= (3 * 252):
        # fix invalid white
        return NES_COLORS[0x30]
    else:
        return s

# finds closest nes color index for input rgb triplet
def closest(s):
    if sum(s) <= (3 * 8):
        # Ensure that invalid blacks are not returned.
        return 0x1D
    elif sum(s) >= (3 * 252):
        # Same goes the whites.
        return 0x30
    min_i, min_d = min(enumerate(NES_COLORS), key=lambda x: _color_dist(x[1], s))
    return min_i

# finds closest nes color rgb for input rgb triplet
def closest_rgb(s):
    s = fix_rgb(s)
    return min(NES_COLORS, key=lambda x: _color_dist(x, s))
