#!/usr/bin/env python3
"""neskit image tool.

"Swiss army knife" to convert images to graphics data used by neskit.

Features:
- Create NES format pattern table data (.patt), containing tile pixels.
- Create NES format name table data (.name), indexing the pattern table.
- Create NES format attribute table data (.attr), defining the palettes.
- Compress the above using yalz (LZSS).
- Remove duplicate tiles in the pattern table.
- Map NES format palette colors from input images.
- Create detail sprites.

To use externally, 'import imgtool' and use the public functions.

"""
__version__ = "0.9.1"
__copyright__ = 'Copyright (c) 2013 Valtteri Heikkila'
__license__ = """
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""
import itertools, sys, os, argparse
from PIL import Image
import nespal, yalz

MAX_PALETTE_SIZE = 32
MIN_SPR_PAL = 4
MAX_SPRITES = 64
NO_PALETTE = -1
ANY_PALETTE = -2
NO_INDEX = -1

_info_quiet = False

_get_pal = lambda x: x // 4 #max(0, x-1) // 3
_get_idx = lambda x: x & 3 #max(0, x-1) % 3

class Error(Exception): pass

class Tile:

    def __init__(self, im, pos, is_8x16=False, is_spr_pal=False):
        px, py = pos
        h = 8 + is_8x16 * 8
        tile_im = im.crop((px, py, px + 8, py + h))
        self.pos = pos
        self.is_8x16 = is_8x16
        self.pixels = [0] * (8*h)
        self.palette = NO_PALETTE
        self.index = NO_INDEX
        self.dupe = False

        possible_palettes = (i for i in range(MIN_SPR_PAL))
        if is_spr_pal:
            possible_palettes = (MIN_SPR_PAL + i for i in possible_palettes)
        possible_palettes = set(possible_palettes)

        for y in range(h):
            for x in range(8):
                pixel = tile_im.getpixel((x, y))
                palette = _get_pal(pixel)
                color = _get_idx(pixel)

                if not is_spr_pal and palette >= MIN_SPR_PAL:
                    color = 0 # Skip invalid pixel, not in sprite mode but has a sprite palette
                elif is_spr_pal and palette < MIN_SPR_PAL:
                    color = 0 # Skip invalid pixel, in sprite mode but doesn't have sprite palette
                elif color != 0:
                    pixel_rgb = im.palette_rgb[pixel]
                    pixel_possible_palettes = set(_get_pal(i) for i in range(color, 32, 4) if pixel_rgb == im.palette_rgb[i])
                    new_possible_palettes = possible_palettes & pixel_possible_palettes
                    if not new_possible_palettes or len(new_possible_palettes) < 1:
                        #print '%X,%X: %X: %s vs. %s: %s' % (x, y, pixel, possible_palettes, pixel_possible_palettes, pixel_rgb)
                        #for s in enumerate(im.palette_rgb):
                        #    print s
                        raise Error("got pixel color %d (pal %d) with possible pals %s vs. %s" % (pixel, palette, pixel_possible_palettes, possible_palettes))
                    possible_palettes = new_possible_palettes
                    """
                    # take palette from first suitable pixel
                    if self.palette == NO_PALETTE:
                        self.palette = palette
                    elif self.palette != palette:
                        # try to find a suitable color from current palette.
                        # image may have duplicated palette entries for a reason.
                        raise Error, "got pixel with color %d (palette %d) when other pixels are from palette %d" % (pixel, palette, self.palette)
                    """
                self.pixels[y * 8 + x] = color

        self.palettes = possible_palettes

        if len(possible_palettes) == MIN_SPR_PAL and sum(self.pixels) == 0:
            # palette is not assigned and all pixel colors are 0
            # => set to have any palette
            self.palette = ANY_PALETTE
        else:
            # Get any palette in possible ones.
            self.palette = list(possible_palettes)[0]

    def _gen_patt_sub(self, sub_index):
        result1 = [0] * 8
        result2 = [0] * 8
        y_offs = 8 * sub_index
        for y in range(8):
            for x in range(8):
                pixel = self.pixels[(y + y_offs) * 8 + x]
                result1[y] |= (pixel & 1) << (7 - x)
                result2[y] |= ((pixel >> 1) & 1) << (7 - x)
        return result1 + result2

    def gen_patt(self):
        result = self._gen_patt_sub(0)
        if self.is_8x16:
            result += self._gen_patt_sub(1)
        return bytearray(result)


    def nequal_pal(self, t):
        return (self.palette != ANY_PALETTE) and (t.palette != ANY_PALETTE) and (self.palette != t.palette)


def _info(msg):
    if not _info_quiet:
        print(msg)

def set_quiet(quiet):
    global _info_quiet
    _info_quiet = quiet

def _write_encoded(filename, data, output_type, use_yalz):
    size_suffix = ''
    if use_yalz:
        if os.path.splitext(filename)[1] != '.yalz':
            filename += '.yalz'
        orig_len = len(data)
        data = yalz.encode(data)
        size_suffix = ' / %d = %d%%' % (orig_len, (100 * len(data)) // orig_len)

    _info("output %s: %s (%d%s bytes)" % (output_type, filename, len(data), size_suffix))
    f = open(filename, "wb")
    f.write(data)
    f.close()

def _get_attr(tw, th, tiles):
    # Extend tw,th to be multiple of 4 as each attr byte stores 4x4 tile (32x32 px) area.
    # TODO: should it warn if extending is done?
    aw = (tw + 3) // 4
    ah = (th + 3) // 4
    result = [0] * (aw * ah)

    # Fill result with | operator from each 2x2 tile area
    for y in range(0, th-1, 2):
        for x in range(0, tw-1, 2):
            i = y * tw + x
            pals = [ tiles[i].palettes, tiles[i + 1].palettes, tiles[i + tw].palettes, tiles[i + tw + 1].palettes ]
            isect = pals[0] & pals[1] & pals[2] & pals[3]
            if len(isect) == 0:
                raise Error("palettes don't match in 2x2 tile area at tile (%d,%d) (got palettes:%s)" % (x, y, [ list(p) for p in pals ]))

            k = (y // 4) * aw + (x // 4)
            shift = 2 * (y % 4) + (x % 4)
            result[k] = result[k] | (isect.pop() << shift) # Just pop any (all isect palettes are valid)

    return bytearray(result)

def _gen_tiles(im, is_sprites):
    h = 8 + is_sprites * 8 # For sprites tile size is 8x16.
    tw = im.size[0] // 8
    th = im.size[1] // h
    for y in range(0, h * th, h):
        for x in range(0, 8 * tw, 8):
            pos = (x, y)
            try:
                # Sprite tiles have is_8x16 and is_spr_pal flags set.
                t = Tile(im, pos, is_sprites, is_sprites)
                if t.palette != NO_PALETTE: # Discard tiles with no palette.
                    yield t
            except Error as e:
                raise Error("at tile %s, %s" % (pos, e))

def convert_spr(infile, patt_fn=None, s_fn=None, offs=(0,0), incr=0, verbose=False, use_yalz=True, quiet=True):
    set_quiet(quiet)
    im = open_image(infile)

    tiles = _gen_tiles(im, True)

    # filter out tiles without sprite information (incl. tiles with any palette)
    tiles = [t for t in tiles if t.palette >= MIN_SPR_PAL]
    _info("spr: found %d sprites" % (len(tiles)))
    # check there is not too many sprites
    if len(tiles) > MAX_SPRITES:
        raise Error("too many sprites %d (maximum: %d)" % (len(tiles), MAX_SPRITES))

    # write patterns
    if patt_fn:
        _write_encoded(patt_fn, bytearray('').join(t.gen_patt() for t in tiles), "sprite patterns", use_yalz)

    # write assembly
    if s_fn:
        data = []
        for i, t in enumerate(tiles):
            x = t.pos[0] + offs[0]
            y = t.pos[1] + offs[1]
            # bit0 set for index to use chrs in $1000
            data += max(0, y - 1), ((2 * i) + incr) | 1, t.palette - MIN_SPR_PAL, x

        name = os.path.splitext(os.path.basename(im.filename))[0]
        f = open(s_fn, "wb")
        f.write("%s_DETAIL_BYTES = %d\n" % (name.upper(), 4 * len(tiles)))
        f.write("%s_details:\n" % (name.lower()))
        f.write("    .BYTE   %s\n" % (repr(data)[1:-1])) # crude hack but works
        f.close()
        _info("output sprite assembly: %s" % (s_fn))

# strip: True=strip duplicate tiles, False=don't strip
def tiles_make_indexes(tiles, strip):
    k = 0
    for i in range(len(tiles)):
        if strip:
            for j in range(0, i):
                if tiles[i].pixels == tiles[j].pixels:
                    tiles[i].index = tiles[j].index
                    tiles[i].dupe = True
                    break
        if tiles[i].index == -1:
            tiles[i].index = k
            k += 1

# Write tile patterns in yalz excluding dupes.
def write_tiles_patt(patt_fn, tiles, msg, use_yalz):
    patt_tiles = [t.gen_patt() for t in tiles if not t.dupe]
    # Uncomment to print patt data hex dump.
    #for t in patt_tiles:
    #    print ''.join("%02X" % (c) for c in t)
    _write_encoded(patt_fn, bytearray().join(patt_tiles), msg, use_yalz)
    return len(patt_tiles)

# strip: True=strip duplicate tiles (default), False=don't strip
def convert_bg(infile, patt_fn=None, name_fn=None, attr_fn=None, incr=0, verbose=False, use_yalz=True, quiet=True, strip=True):
    set_quiet(quiet)
    im = open_image(infile)

    tiles = []

    tw = im.size[0] // 8
    th = im.size[1] // 8

    tiles = list(_gen_tiles(im, False))

    # check palettes match in 16x16 pixel area
    if attr_fn:
        for ty in range(0, th - 1, 2):
            for tx in range(0, tw - 1, 2):
                i = tw * ty + tx
                t = tiles[i]
                if t.nequal_pal(tiles[i + 1]) or t.nequal_pal(tiles[i + tw]) or t.nequal_pal(tiles[i + tw + 1]):
                    raise Error("palettes don't match in 2x2 tile area at (%d,%d) px" % (8 * tx, 8 * ty))

    _info("bg: size %dx%d tiles" % (tw, th))
    tiles_make_indexes(tiles, strip)

    if patt_fn:
        num_unique = write_tiles_patt(patt_fn, tiles, "bg patterns", use_yalz)
        _info("bg: wrote %d unique tiles" % (num_unique))

    # write name
    if name_fn:
        names = [t.index + incr for t in tiles]
        _write_encoded(name_fn, bytearray(names), "bg names", use_yalz)
        if verbose:
            _info("name table: " + repr(names))

    # write attr
    if attr_fn:
        _write_encoded(attr_fn, bytearray(_get_attr(tw, th, tiles)), "bg attributes", use_yalz)

def convert_pal(infile, pale_fn, use_yalz=True, quiet=True):
    set_quiet(quiet)
    im = open_image(infile)

    # strip NES pal 0-color to make palette a bit smaller
    palette_idx = [im.palette_idx[i] for i in range(32) if (i == 0) or (i & 3) != 0]

    if pale_fn:
        f = open(pale_fn, "wb")
        f.write(bytearray(palette_idx))
        f.close()
        _info("output palettes: %s (25 bytes)" % (pale_fn))

def _create_pal_im():
    pal = list(itertools.chain.from_iterable(nespal.NES_COLORS))
    pal += [0] * (768 - len(pal))
    pal_im = Image.new('P', (1, 1))
    pal_im.putpalette(pal)
    pal_im.load()
    return pal_im.im
_pal_im = _create_pal_im()

# convert Pillow.Image to nes palette.
# inputs:
#   max_colors: set to optimize palette (must be <=MAX_PALETTE_SIZE, raises if above this)
#   bg_color: set bg color index (NES pal color index)
# return:
#   new image with nes palette.
def convert_to_nes_palette(im, max_colors=None, bg_color=None):
    if im.mode != 'RGB' and im.mode != 'P':
        raise Error(f'image must be rgb or paletted (was {im.mode})')
    if max_colors != None and max_colors > MAX_PALETTE_SIZE:
        raise Error(f'max_colors parameter must be <={MAX_PALETTE_SIZE} (was {max_colors})')

    im.load()
    core = im.im.convert('P', 0, _pal_im)
    im = im._new(core)

    if max_colors == None:
        # just fix possible invalid colors
        im.putdata(tuple(nespal.fix_idx(c) for c in im.getdata()))
        return im
    else:
        # optimise by removing unused colors
        pal_lookup = [None] * len(nespal.NES_COLORS)
        pal = []
        if bg_color != None:
            pal_lookup[bg_color] = 0
            pal.append(nespal.NES_COLORS[bg_color])

        data = [0] * (im.width * im.height)
        for i, c in enumerate(nespal.fix_idx(c) for c in im.getdata()):
            if pal_lookup[c] == None:
                if len(pal) >= max_colors:
                    raise Error(f'more than {max_colors} colors in image')
                pal_lookup[c] = len(pal)
                pal.append(nespal.NES_COLORS[c])
            data[i] = pal_lookup[c]

        res = Image.frombytes('P', im.size, bytes(data))
        res.putpalette(tuple(itertools.chain(*pal)))
        return res

def open_image(filename):
    im = Image.open(filename)
    #_info("open_image: %s (%dx%d %s %s)" % (filename, im.size[0], im.size[1], im.format, im.mode))
    im.filename = filename
    if im.mode != 'P':
        im = convert_to_nes_palette(im, MAX_PALETTE_SIZE)

    # validate palette, all colors must have index <MAX_PALETTE_SIZE
    for c in im.getcolors():
        if c[1] >= MAX_PALETTE_SIZE:
            raise Error(f"found pixel with palette index {c[1]}, image must have <{MAX_PALETTE_SIZE} colors")

    # Create RGB-triplets from image palette.
    # Strip to 32 colors palette (bg & spr)
    # get colors from palette buffer
    palette_rgb = im.getpalette()[:3 * MAX_PALETTE_SIZE]
    # splice buffer to RGB triplets
    palette_rgb = [palette_rgb[i:i+3] for i in range(0, len(palette_rgb), 3)]
    # expand palette to full 32 colors by padding with 0-color
    palette_rgb += [palette_rgb[0]] * max(0, 32 - len(palette_rgb))
    im.palette_rgb = palette_rgb

    # Create final NES palette indexes.
    # convert RGB to NES palette index
    palette_idx = [nespal.closest(c) for c in palette_rgb]
    # TODO: remove, done already above
    # add padding color (grey) to get full 32 colors
    #palette_idx += [0] * max(0, 32 - len(palette_idx))
    im.palette_idx = palette_idx

    return im

def convert(args):
    if args.bg_patt or args.bg_name or args.bg_attr:
        convert_bg(args.infile, args.bg_patt, args.bg_name, args.bg_attr, args.bg_incr, args.verbose, args.yalz, args.quiet, not args.no_strip)
    if args.spr_patt or args.spr_s:
        convert_spr(args.infile, args.spr_patt, args.spr_s, (args.spr_x, args.spr_y), args.spr_incr, args.verbose, args.yalz, args.quiet)
    if args.pale:
        convert_pal(args.infile, args.pale, args.yalz, args.quiet)

def _main():
    parser = argparse.ArgumentParser(description='convert images to nes data')
    mutex = parser.add_mutually_exclusive_group()
    mutex.add_argument('-q', '--quiet', action='store_true', help='suppress shell output')
    mutex.add_argument('-v', '--verbose', action='store_true', help='show extra information')
    parser.add_argument('-m', '--max-tiles', type=int, default=256, help='maximum number of tiles for .patt (default: 256)')
    parser.add_argument('-bi', '--bg-incr', type=int, default=0, help='increment for each byte in bg .name (default: 0)')
    parser.add_argument('-bp', '--bg-patt', help='output .patt file name')
    parser.add_argument('-bn', '--bg-name', help='output .name file name')
    parser.add_argument('-ba', '--bg-attr', help='output .attr file name')
    parser.add_argument('-si', '--spr-incr', type=int, default=0, help='increment for each byte in spr indices (default: 0)')
    parser.add_argument('-sp', '--spr-patt', help='output sprite .patt file name')
    parser.add_argument('-ss', '--spr-s', help='output sprite .s file name')
    parser.add_argument('-sx', '--spr-x', type=int, default=0, help='offset to sprite x coordinate')
    parser.add_argument('-sy', '--spr-y', type=int, default=0, help='offset to sprite y coordinate')
    parser.add_argument('-p', '--pale', help='output .pale file name')
    parser.add_argument('-y', '--yalz', action='store_true', help='encode .patt, .name, .attr output. extension .yalz will be added')
    parser.add_argument('-n', '--no-strip', action='store_true', help="don't strip duplicate tiles")
    parser.add_argument('infile', help='input file')
    args = parser.parse_args()

    if args.pale or args.bg_patt or args.bg_name or args.bg_attr or args.spr_patt or args.spr_s:
        convert(args)
    else:
        raise Error("nothing to do, specify an option: -p, -n, -a, -sp, -ss")

if __name__ == '__main__':
    try:
        _main()
    except Error as e:
        print("ERROR:", e)
