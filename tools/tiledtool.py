#!/usr/bin/env python3
"""neskit tiled map tool.

Create scrollable tile map (name table and attribute table data)
for neskit from tiled .json tile map.

"""
__version__ = "0.9.1"
__copyright__ = 'Copyright (c) 2019 Valtteri Heikkila'
__license__ = """
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""
import argparse, json, os, sys
import imgtool

# strip: True=strip duplicate tiles (default), False=don't strip
# Returns list of tiles.
def convert_tileset(infile, patt_fn=None, incr=0, verbose=False, use_yalz=True, quiet=True, strip=True):
    imgtool.set_quiet(quiet)
    im = imgtool.open_image(infile)

    tiles = []

    tw = im.size[0] // 8
    th = im.size[1] // 8

    tiles = list(imgtool._gen_tiles(im, False))

    imgtool._info("tileset: size %dx%d tiles" % (tw, th))
    imgtool.tiles_make_indexes(tiles, strip)

    if patt_fn:
        num_unique = imgtool.write_tiles_patt(patt_fn, tiles, "bg patterns", use_yalz)
        imgtool._info("tileset: wrote %d unique tiles" % (num_unique))

    return tiles

def convert_map(map_json, tiles, scroll, name_fn, attr_fn, verbose=False, use_yalz=True, quiet=True):
    data = [ x - 1 for x in map_json["layers"][0]["data"] ]
    tw = map_json["layers"][0]["width"]
    th = map_json["layers"][0]["height"]
    if scroll == 'up':
        if name_fn or attr_fn:
            if tw < 32:
                raise imgtool.Error("scroll=%s: tile layer must be >=32 tiles wide" % (scroll))
            elif tw > 32:
                imgtool._info("scroll=%s: tile layer width %d > 32, only first 32 tiles per row will be used" % (scroll, tw))

            names = []
            for i in range(len(data) // tw - 1, -1, -1):
                names.extend(data[tw * i:tw * i + 32])

            if name_fn:
                imgtool._write_encoded(name_fn, bytearray(tiles[x].index for x in names), "bg names", use_yalz)
                if verbose:
                    imgtool._info("name table: " + repr(names))

            if attr_fn:
                screens = [ names[i:i + 32*30] for i in range(0, len(names), 32*30) ]
                name_tiles = []
                for s in screens:
                    name_tiles.extend([ tiles[0] ] * (32*32 - len(s)))
                    name_tiles.extend([ tiles[x] for x in s ])
                th = len(name_tiles) // 32

                attr_bytes = bytearray(imgtool._get_attr(32, th, name_tiles))
                for i in range(len(attr_bytes)):
                    attr_bytes[i] = (attr_bytes[i] >> 4) + ((attr_bytes[i] << 4) & 255)
                imgtool._write_encoded(attr_fn, attr_bytes, "bg attributes", use_yalz)

def convert(args):
    with open(args.infile, 'r') as map_f:
        map_json = json.load(map_f)
        tileset_json_fn = os.path.join(os.path.dirname(args.infile), map_json["tilesets"][0]["source"])
        with open(tileset_json_fn, 'r') as tileset_f:
            tileset = json.load(tileset_f)
            tileset_image_fn = os.path.join(os.path.dirname(args.infile), tileset["image"])
            tileset_image_f = open(tileset_image_fn, 'rb')

            if args.bg_patt or args.bg_name or args.bg_attr or args.pale:
                tiles = convert_tileset(tileset_image_f, args.bg_patt, args.bg_incr, args.verbose, args.yalz, args.quiet, not args.no_strip)
                if args.bg_name or args.bg_attr:
                    convert_map(map_json, tiles, args.scroll, args.bg_name, args.bg_attr, args.verbose, args.yalz, args.quiet)
                if args.pale:
                    imgtool.convert_pal(tileset_image_f, args.pale, args.yalz, args.quiet)

def _main():
    parser = argparse.ArgumentParser(description='convert tiled .json for neskit')
    mutex = parser.add_mutually_exclusive_group()
    mutex.add_argument('-q', '--quiet', action='store_true', help='suppress shell output')
    mutex.add_argument('-v', '--verbose', action='store_true', help='show extra information')
    parser.add_argument('-m', '--max-tiles', type=int, default=256, help='maximum number of tiles for .patt (default: 256)')
    parser.add_argument('-bi', '--bg-incr', type=int, default=0, help='increment for each byte in bg .name (default: 0)')
    parser.add_argument('-bp', '--bg-patt', help='output .patt file name')
    parser.add_argument('-bn', '--bg-name', help='output .name file name')
    parser.add_argument('-ba', '--bg-attr', help='output .attr file name')
    parser.add_argument('-ss', '--spr-s', help='output sprite .s file name')
    parser.add_argument('-p', '--pale', help='output .pale file name')
    parser.add_argument('-s', '--scroll', choices=['up', 'both'], default='up', help='scrolling type')
    parser.add_argument('-y', '--yalz', action='store_true', help='encode .patt, .name, .attr output. extension .yalz will be added')
    parser.add_argument('-n', '--no-strip', action='store_true', help="don't strip duplicate tiles")
    parser.add_argument('infile', help='input tiled .json file')
    args = parser.parse_args()

    if args.pale or args.bg_patt or args.bg_name or args.bg_attr:
        convert(args)
    else:
        raise imgtool.Error("nothing to do, specify an option: -p, -n, -a, -sp, -ss")

if __name__ == '__main__':
    try:
        _main()
    except imgtool.Error as e:
        print("ERROR:", e)
