#!/usr/bin/env python3
"""yalz (de)compression tool.

yalz is a modified LZSS algorithm (extension .yalz). The compression is
loosely based on the description of GameBoy Advance BIOS function
LZ77UnCompVram in the following document:
http://nocash.emubase.de/gbatek.htm#biosdecompressionfunctions

yalz uses a 256 byte lookback buffer because the NES system has very
limited RAM. Using 256 bytes has other benefits (=equal to 6502 page
size) so the tradeoff between performance, used memory and compression
is fairly good. Typical average compression ratio is around 60-70% for
graphics.

neskit uses yalz only to compress graphics. This is because NES RAM is
very limited, so there is generally no reason to decompress into RAM.

As with LZSS, compressed data is a stream of sequences of 8 blocks.
The sequence has an 8-bit header indicating which following blocks
are literal blocks and copy blocks. Description of a single sequence:

  Sequence header:
    1 byte    Type flags next 8 blocks (LSB for 0th block)
  8 blocks, each with following structure:
    If header bit == 0, literal block:
      1 byte  Single data byte to be copied to destination
    Otherwise header bit == 1, copy block (from lookback):
      1 byte  Copy start index in lookback buffer
      1 byte  Copy end index in lookback buffer

In case of copy block, all bytes between start and end index are copied
to destination. Decoding is terminated when a copy block has start index
equal to current lookback buffer index. As witl LZ77 algorithm, every
byte that is copied to destination is also copied to the lookback.

NOTE: To reduce NES system RAM usage, lookback buffer can be located
to OAM sprite mirror page during the decompression. This can be done
if NES PPU is disabled during the decompression.

"""
__version__ = "0.9.1"
__copyright__ = 'Copyright (c) 2014 Valtteri Heikkila'
__license__ = """
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""
import sys, argparse

LOOKBACK_SIZE = 256
MIN_COPY_LEN = 3   # 2 bytes encodes a copy

s_lookback = bytearray([0 for _ in range(LOOKBACK_SIZE)])
s_head = 0

class Error(Exception): pass

def _buf_scan(p, dst_idx, src_idx, max_len):
    while p[dst_idx] != p[src_idx]:
        dst_idx += 1
        if (dst_idx >= src_idx):
            return 0, 0

    orig_dst_idx = dst_idx
    match_len = 0
    while (src_idx < len(p)) and (match_len < max_len) and (p[dst_idx] == p[src_idx]):
        dst_idx += 1
        src_idx += 1
        match_len += 1
    
    return match_len, orig_dst_idx


# Returns 0,0 if no matches is found,
# otherwise length, index pair of best match.
def _find_best(b, curr_idx):
    if (curr_idx - LOOKBACK_SIZE) >= 0:
        scan_idx = curr_idx - LOOKBACK_SIZE
    else:
        scan_idx = 0
    final_len = 0
    final_idx = 0
    while scan_idx < curr_idx:
        length, match_idx = _buf_scan(b, scan_idx, curr_idx, LOOKBACK_SIZE - 1)
        if 0 == length:
            break
        if (length >= MIN_COPY_LEN) and (length > final_len):
            final_len = length
            final_idx = match_idx
        scan_idx = match_idx + 1
        
    return final_len, final_idx


def encode(b, quiet = True, verbose = False):
    # reserve enough to contain buffer in worst case scenario
    out = bytearray([0 for _ in range(MIN_COPY_LEN*len(b))])
    idx = 0
    
    len_hist = [0 for _ in range(LOOKBACK_SIZE)]
    curr_idx = 0

    flags_idx = 0
    block_counter = 0
    flags = 0
    while curr_idx < len(b):
        if 0 == block_counter:
            out[flags_idx] = flags
            flags = 0
            flags_idx = idx
            out[idx] = 0
            idx += 1

            if not quiet:
                print("\r  %2d%%" % ((curr_idx * 100) / len(b)), end=' ')

        best_len, best_idx = _find_best(b, curr_idx)
        if not best_len:
            # uncompressed (literal)
            out[idx] = b[curr_idx]
            idx += 1
            curr_idx += 1
        else:
            # compressed (copy)
            out[idx] = ((best_idx + best_len) % LOOKBACK_SIZE) # end offset
            out[idx+1] = (best_idx % LOOKBACK_SIZE) # start offset
            idx += 2
            curr_idx += best_len
            flags |= (1 << block_counter)
            
            len_hist[best_len] += 1
        
        block_counter = (block_counter + 1) % 8
    
    out[flags_idx] = flags

    # append terminator
    if 0 == block_counter:
        out[idx] = 0x01
        idx += 1
    else:
        out[flags_idx] |= (1 << block_counter)
    out[idx] = ((curr_idx) % LOOKBACK_SIZE)
    out[idx+1] = ((curr_idx) % LOOKBACK_SIZE)
    idx += 2
    
    if not quiet and verbose:
        print("\rmatch lengths histogram:\n");
        for block_counter in range(LOOKBACK_SIZE):
            c = len_hist[block_counter] / 2
            if c > 0:
                print(("  %-3d  %s" % (block_counter, c * "*")))
        print("")
    
    out = out[:idx]

    # if encoded size is larger than input (+raw header size), then store as raw
    if len(out) >= len(b) + 2:
        if not quiet:
            print("\rINFO: encoded file is larger than original, storing as raw")
        out = encode_raw(b)
        if not out:
            raise Error("ERROR: encoding failed, maximum encoded size is 32kb")

    return out

def encode_raw(in_bytes):
    length = len(in_bytes)
    if length > 0x7FFF:
        return None
    out = bytearray([ \
        ((length & 0x7F00) >> 7) | 1,
        (length & 0x00FF) \
    ])
    out += in_bytes
    return out

#def _push_byte(out, byte, idx):
def _push_byte(out, byte):
    global s_head
    #print idx
    out.append(byte)
    s_lookback[s_head] = byte
    s_head = (s_head + 1) % LOOKBACK_SIZE


# TODO: needs checks for overflow (b->data[curr_idx++])
def decode(in_bytes, args):
    global s_head
    
    curr_idx = 0
    out = bytearray()    

    s_head = 0

    run = True

    while run:
        flags = in_bytes[curr_idx]
        curr_idx += 1

        for i in range(8):
            if flags & (1 << i):
                # copy
                end_offs = in_bytes[curr_idx]
                start_offs = in_bytes[curr_idx + 1]
                curr_idx += 2
                if start_offs == end_offs:
                    #puts("terminator");
                    run = False
                    break

                # copy loop
                while start_offs != end_offs:
                    a = s_lookback[start_offs]
                    start_offs = (start_offs + 1) % LOOKBACK_SIZE
                    _push_byte(out, a)

            else:
                # literal
                _push_byte(out, in_bytes[curr_idx])
                curr_idx += 1
            
            if curr_idx >= len(in_bytes):
                #puts("eof");
                run = False
                break

    return out


def main():
    parser = argparse.ArgumentParser(description='yalz is a lempel-ziv (de)compressor')
    mutex = parser.add_mutually_exclusive_group()
    mutex.add_argument('-q', '--quiet', action='store_true', help='suppress shell output')
    mutex.add_argument('-v', '--verbose', action='store_true', help='show extra information')
    mutex = parser.add_mutually_exclusive_group(required=True)
    mutex.add_argument('-c', '--create', action='store_true', help='create (compress)')
    mutex.add_argument('-x', '--extract', action='store_true', help='extract (decompress)')
    parser.add_argument('infile', help='input file')
    parser.add_argument('outfile', help='output file')
    args = parser.parse_args()

    if not args.quiet:
        if args.create:
            print("\n  \\(O___o)/ -create!\n")
        else:
            print("\n  \\(O___o)/ -extract!\n")
        print(("infile: %s" % (args.infile)))
        print(("outfile: %s" % (args.outfile)))

    f = open(args.infile, "rb")
    src = bytearray(f.read())
    f.close()

    if args.create:    
        dst = encode(src, args.quiet, args.verbose)
    else:
        dst = decode(src, args) # never fails?
    

    if not args.quiet:
        print(("\rinput: %d bytes" % (len(src))))
        print(("output: %d bytes" % (len(dst))))
        if args.create:
            print(("ratio: %d%%\n" % ((100 * len(dst) / len(src)))))

    f = open(args.outfile, "wb")
    f.write(dst)
    f.close()


def test():
    print("testing by creating .yalz from yalz.py and then extracting it...")
    sys.argv = [sys.argv[0], '-c', 'yalz.py', 'yalz_test.yalz']
    main()
    sys.argv = [sys.argv[0], '-x', 'yalz_test.yalz', 'yalz_test.tmp']
    main()
    import filecmp
    print("\nresult:", end=' ')
    if filecmp.cmp('yalz.py', 'yalz_test.tmp'):
        print("pass")
    else:
        print("fail")


if __name__ == "__main__":
    main()
