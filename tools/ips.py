#!/usr/bin/env python3
"""A simplistic .ips patcher tool.

To use externally, use 'import ips' and then the 'ips.patch()' function.

This code follows the description of .ips found here:
http://www.zerosoft.zophar.net/ips.php

"""
__version__ = "0.9.1"
__copyright__ = 'Copyright (c) 2014 Valtteri Heikkila'
__license__ = """
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""
import argparse

class _FileDataBuffer:
    def __init__(self, filename):
        f = open(filename, "rb")
        self.data = bytearray(f.read())
        f.close()
        self.pos = 0

    def size(self):
        return len(self.data)

    def read(self, num_bytes):
        max_size = self.size()
        if self.pos >= max_size:
            return None
        else:
            old_pos = self.pos
            self.pos += num_bytes
            if self.pos > max_size:
                self.pos = max_size
            return self.data[old_pos:self.pos]

    def patch_at(self, pos, data):
        start = pos
        end = pos + len(data)
        self.data[start:end] = data

    def write_to_file(self, filename):
        f = open(filename, "wb")
        f.write(self.data)
        f.close()

def patch(args, widget):
    try:
        filebuf = _FileDataBuffer(args['infile'])
        widget.info("read %s (%d bytes)" % (args['infile'], filebuf.size()))
    except IOError:
        return widget.error("file %s not found" % args['infile'])

    try:
        ipsbuf = _FileDataBuffer(args['patchfile'])
        widget.info("read %s (%d bytes)" % (args['patchfile'], ipsbuf.size()))
    except IOError:
        return widget.error("file %s not found" % args['patchfile'])

    if ipsbuf.read(5) != "PATCH":
        return widget.error("unrecognized .ips file, no 'PATCH' signature found")

    widget.info('patching...')
    prev_progress_percentage = 0
    while True:
        data = ipsbuf.read(3)

        progress_percentage = (100 * ipsbuf.pos) // ipsbuf.size()
        if progress_percentage > prev_progress_percentage:
            widget.progress(progress_percentage)
            prev_progress_percentage = progress_percentage
        
        if data == 'EOF': # .ips format has 'EOF' marker at end of file
            break

        offs = (data[0]<<16) + (data[1]<<8) + data[2]
        if offs >= filebuf.size():
            return widget.error("patching offset %s is out of bounds" % (offs))

        data = ipsbuf.read(2)
        size = (data[0]<<8) + data[1]
        if size != 0:
            data = ipsbuf.read(size)
            filebuf.patch_at(offs, data)
        else:
            data = ipsbuf.read(2)
            size = (data[0]<<8) + data[1]
            data = ipsbuf.read(1)
            filebuf.patch_at(offs, data * size)
    widget.info("done")

    try:
        filebuf.write_to_file(args['outfile'])
    except IOError:
        return widget.error("unable to write file %s" % args['outfile'])
    widget.info("wrote %s (%d bytes)" % (args['outfile'], filebuf.size()))

def _parse_args():
    parser = argparse.ArgumentParser(description='apply .ips patch to a file')
    parser.add_argument('patchfile', help='input patch file (.ips)')
    parser.add_argument('infile', help='input file to patch')
    parser.add_argument('outfile', help='output file from patching')
    return vars(parser.parse_args())

if __name__ == '__main__':
    class _ConsoleWidget:
        def info(self, message):
            print(message)

        def error(self, message):
            print('error: %s' % (message))

        def progress(self, percentage):
            print("\r    \r%d%%" % (percentage), end=' ')
        
    patch(_parse_args(), _ConsoleWidget())
