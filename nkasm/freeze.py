import collections, re
import log, parse, state, spec, eva, encode, flow
from util import *

_re_unnamed_pos = re.compile(":\++")
_re_unnamed_neg = re.compile(":-+")
_stats = None
_parent_label = None

def _assert(params):
        msg = "" if len(params) < 2 else f": {params[1]}"
        if not eva.luate(params[0]):
                raise UserError(f"assertion hit{msg}")

def _byte(params):
        return (encode.cc_byte, params, len(params))

def _str(params):
        data = params[0].strip('"')
        return (encode.cc_str, data, len(data))

def _strz(params):
        data = params[0].strip('"')
        return (encode.cc_strz, data, len(data) + 1)

def _pstr(params):
        data = params[0].strip('"')
        return (encode.cc_pstr, data, len(data) + 1)

def _endstat(params):
        if not _stats:
                raise UserError("missing leading .STAT")
        bank = state.curr_segment().curr_bank()
        _stats[-1].end = bank.org()
        bank.stats.append(_stats.pop())

def _incbin(params):
        filename = params[0].strip('"')
        log.info("including binary file %s" % (filename))
        f = flow.open_file(filename, "rb")
        data = f.read()
        log.info("read $%X bytes from %s" % (len(data), filename))
        f.close
        return (encode.cc_incbin, data, len(data))

def _org(params):
        state.curr_segment().curr_bank().set_org(eva.luate(params[0]))

def _align(params):
        align = int(eva.luate(params[0]))
        bank = state.curr_segment().curr_bank()
        bank.set_org((bank.org() + align - 1) // align * align)

def _stat(params):
        _stats.append(state.Stat(params[0]))

def _res(params):
        bank = state.curr_segment().curr_bank()
        bank.set_org(bank.org() + eva.luate(params[0]))

def _word(params):
        return (encode.cc_word, params, len(params) * 2)

def _label(s):
        global _parent_label
        label, new_parent = parse.label(s, _parent_label)
        if label == None:
                raise UserError("failed parsing label")
        if state.add_label(label):
                _parent_label = new_parent

C = collections.namedtuple('C', 'func, min_params, max_params, is_rom')
_commands = {
'ASSERT'  : C(_assert,   1, 2, 0),
'BYTE'    : C(_byte,     1, -1, 1),
'STR'     : C(_str,      1, 1, 1),
'STRZ'    : C(_strz,     1, 1, 1),
'PSTR'    : C(_pstr,     1, 1, 1),
'ENDSTAT' : C(_endstat,  0, 0,  1),
'INCBIN'  : C(_incbin,   1, 1,  1),
'ORG'     : C(_org,      1, 1,  0),
'ALIGN'   : C(_align,    1, 1,  0),
'STAT'    : C(_stat,     1, 1,  1),
'RES'     : C(_res,      1, 1,  0),
'WORD'    : C(_word,     1, -1, 1),
# duplicates:
'DB'      : C(_byte,     1, -1, 1),
'DW'      : C(_word,     1, -1, 1),
}

# TODO: refactor, dupe: freeze, flow
def _check_cmd_params(cmd_name, cmd, params):
        if cmd.max_params > -1 and len(params) > cmd.max_params:
                raise UserError('too many parameters to .%s' % (cmd_name))
        if len(params) < cmd.min_params:
                raise UserError('too few parameters to .%s' % (cmd_name))
        if cmd.is_rom == 1 and not state.curr_segment().is_rom:
                raise UserError('segment %s doesnt allow .%s' % (state._segment_name, cmd_name))

def _process_cmd_freeze(cmd_name, params):
        try:
                cmd = _commands[cmd_name]
        except:
                raise UserError("unknown control command .%s" % (cmd_name))
        _check_cmd_params(cmd_name, cmd, params)
        return cmd.func(params)

def _control_command(line, s):
        cmd, params = parse.control_command(s)
        if cmd != None:
                data = _process_cmd_freeze(cmd, params)
                if data:
                        # got raw data, add it as line
                        line.set_encode_func(data[0])
                        line.tokens = data[1] # piggyback tokens for raw data
                        bank = state.curr_segment().curr_bank()
                        bank.set_org(bank.org() + data[2])
                        return line
                else:
                        return None
        else:
                raise UserError("on parse control command")

def _has_zeropage_variable(tokens):
        for t in tokens:
                if state.is_in_zeropage(t[2]):
                        return True
        return False

def _is_zeropage_value(tokens):
        try:
                # try to evaluate tokens. if eval works, check if result is in zeropage
                # TODO: if zeropage var is used, maybe intention is to have it in zeropage?
                # i.e. if eval fails, there may be some buggy code
                has_zeropage = _has_zeropage_variable(tokens)
                # TODO: store result for encode?
                result = eva.luate_tokens(tokens)
                if result > 0x00FF:
                        if has_zeropage:
                                log.warning("expression results address above zeropage (>$00FF)")
                        return False
                elif result < 0x0000:
                        if has_zeropage:
                                log.warning("expression results address below zeropage (<$0000)")
                        return False
                else:
                        return True
        except UserError:
                return False
        raise UserError("never reached")

def _fix_fmt(fmt, tokens):
        if fmt[0] != 'D':
                return fmt
        if _is_zeropage_value(tokens):
                # Signal value is zeropage (byte, not word).
                return 'Z' + fmt[1:]
        else:
                return 'R' + fmt[1:]

def _instruction(line, s):
        cmd, params = parse.command(s)
        if not cmd or cmd != None:
                try:
                        op = spec.instructions[cmd]
                        #line.parent_label = _parent_label
                        fmt, value = parse.instruction_params(params)
                        if value:
                                line.tokens = eva.tokenize(value)
                                fmt = _fix_fmt(fmt, line.tokens)

                        # Try to get op for assembly format.
                        try:
                                line.op = op[fmt]
                        except KeyError:
                                if fmt[0] == 'Z':
                                        # Was zeropage, try non-zeropage.
                                        fmt = 'R' + fmt[1:]
                                        line.op = op[fmt]
                                        log.warning("expanded a zeropage value to word width")
                                else:
                                        raise UserError(f"nonexistent or malformed instruction")

                        line.set_encode_func(line.op.encode_func)
                        bank = state.curr_segment().curr_bank()
                        bank.set_org(bank.org() + line.op.size_bytes)
                        return line
                except KeyError:
                        raise UserError("undefined instruction %s" % (cmd))
        return None

def _parse_line(line):
        s = line.text
        if s[0] != CONTROL_CMD_CHR:
                # calculate offset for unnamed label index
                unnamed_label_offs = [0]
                def repl_unnamed(match):
                        group = match.group(0)
                        if group[1] == '+':
                                unnamed_label_offs[0] = len(group) - 2
                        else:
                                unnamed_label_offs[0] = -(len(group) - 1)
                        return '__label%%ANON_IDX%%'
                s = _re_unnamed_pos.sub(repl_unnamed, s)
                s = _re_unnamed_neg.sub(repl_unnamed, s)

                sp = s.split(':')
                if len(sp) > 2:
                        raise UserError('line parse error')
                elif len(sp) == 2:
                        s = sp[0].rstrip() + ':'
                        # adds new label
                        _label(s)
                        s = sp[1].lstrip()
                        if not s:
                                # nothing on line except label, skip
                                return None

                # finalize unnamed label
                s = s.replace('%%ANON_IDX%%', str(state.num_labels() + unnamed_label_offs[0]))

                if s[0] != CONTROL_CMD_CHR:
                        return _instruction(line, s)

        # otherwise we have control command
        return _control_command(line, s)

def _init():
        global _stats, _parent_label

        _stats = []
        _parent_label = ''

def _process_bank(lines):
        for line in lines:
                state.set_curr_line(line)
                line.org = state.curr_segment().curr_bank().org()
                line.parent_label = _parent_label
                out = _parse_line(line)
                if out:
                        yield out

def _validate_bank(bank_idx):
        if _stats:
                raise UserError("missing .ENDSTAT at end of bank:%d, .STAT can't span across banks" % (bank_idx))

def process():
        _init()
        for segment_name in ['ZEROPAGE', 'BSS', 'CODE', 'CHR']: # zeropage must be first
                state.set_curr_segment(segment_name)
                segment = state.curr_segment()
                for bank_idx, bank in enumerate(segment.banks):
                        segment.set_curr_bank_idx(bank_idx)
                        state.set_out(tuple(_process_bank(bank.lines)))
                        _validate_bank(bank_idx)
