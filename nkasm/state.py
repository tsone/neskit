import collections
from types import MethodType
from util import *
import spec, flow

class Line(Locked):
        def __init__(self, filename, linenum, text):
                self.filename = filename
                self.linenum = linenum
                self.text = text
                self.op = None
                self.data = None

        def set_encode_func(self, func):
                self.encode_func = MethodType(func, self)
Line._allowed = ('filename', 'linenum', 'text', 'org', 'tokens', 'op' ,'parent_label', 'encode_func', 'data')

class Segment(Locked):
        def __init__(self, is_rom, banks):
                self.is_rom = is_rom
                self.banks = banks
                self._bank_idx = 0
                self.pads = [] # padding sections added by encode

        def num_banks(self):
                return len(self.banks)

        def curr_bank_idx(self):
                return self._bank_idx

        def set_curr_bank_idx(self, bank_idx):
                if bank_idx < 0 or bank_idx >= self.num_banks():
                        raise UserError("out of range: 0-%d" % (self.num_banks() - 1))
                self._bank_idx = bank_idx

        def pos2bank_idx(self, pos):
                return pos // self.bank_size()

        def pos2org(self, pos):
                bank_idx = self.pos2bank_idx(pos)
                bank = self.banks[bank_idx]
                return pos - bank_idx * bank.size() + bank.start

        def org2pos(self, bank_idx, org):
                bank = self.banks[bank_idx]
                # TODO: error is possible only at end of bank, also check after incrementing position
                if org < bank.start or org > bank.end:
                        raise UserError("org:$%04X out of range:$%04X-$%04X at bank:%d" % (org, bank.start, bank.end, bank_idx))
                return bank_idx * bank.size() + (org - bank.start)

        def curr_bank(self):
                return self.banks[self._bank_idx]

        def bank_size(self):
                return self.banks[0].size()
Segment._allowed = ('banks', '_bank_idx', 'pads', 'is_rom')

class Bank(Locked):
        def __init__(self, start, end):
                self.start = start
                self.end = end
                self._org = start
                self.labels = {}
                self.out = []
                self.lines = []
                self.stats = []

        def org(self):
                return self._org

        def set_org(self, org):
                self._org = org

        def size(self):
                return self.end - self.start + 1
Bank._allowed = ('start', 'end', '_org', 'labels', 'out', 'lines', 'stats')

class Stat(Locked):
        def __init__(self, name):
                self.name = name
                self.bank = curr_segment().curr_bank()
                self.start = self.bank.org()
Stat._allowed = ('name', 'bank', 'start', 'end')

_reserved_list = ['A', 'X', 'Y', '__BANK__', '__MAPPER__', '__NES__', '__NUM_BANKS__', '__NUM_CHR_BANKS__']
_segment_list = None
_segment_name = None
_num_labels = None
_curr_line = None
_chardict = "" # TODO: set default charmap
_charbase = 0

def curr_line():
        return _curr_line

def set_curr_line(line):
        global _curr_line
        _curr_line = line
        flow.set_define('__BANK__', str(curr_segment().curr_bank_idx()))

def curr_segment():
        return _segment_list[_segment_name]

def set_curr_segment(name):
        global _segment_name
        name = name.upper()
        if name in _segment_list:
                _segment_name = name
                return None
        else:
                raise UserError("invalid segment '%s'" % (name))

def curr_org():
        return curr_segment().curr_bank().org()

def set_curr_org(org):
        curr_segment().curr_bank().set_org(org)

def init(num_banks, num_chr_banks):
        global _segment_name, _num_labels, _curr_line, _segment_list

        _segment_name = 'CODE' # default segment: CODE
        _num_labels = 0
        _curr_line = None
        # create banks for segments
        _segment_list = {
        'ZEROPAGE' : Segment(0, [Bank(ZEROPAGE.start, ZEROPAGE.end)]),
        'BSS'      : Segment(0, [Bank(BSS.start, BSS.end)]),
        'CODE'     : Segment(1, [Bank(BANKED.start, BANKED.end) for _ in range(num_banks - 1)] \
                + [Bank(FIXED.start, FIXED.end)]),
        'CHR'      : Segment(1, [Bank(CHR.start, CHR.end) for _ in range(num_chr_banks)]),
        }

def num_labels():
        return _num_labels

def add_label(label_name):
        global _num_labels
        if is_reserved(label_name):
                raise UserError('reserved "' + label_name + '"')
        if get_label(label_name) != None:
                raise UserError('label "' + label_name + '" already exists')
        label_id = '__label' + str(_num_labels)
        _num_labels = _num_labels + 1
        bank = curr_segment().curr_bank()
        bank.labels[label_id] = bank.org()
        if len(label_name) > 0:
                bank.labels[label_name] = bank.org()
                return True
        return False

def get_label(label_name):
        # TODO: refactor this by adding tokenizer in pre (to generate proper sublabel tokes)
        if not label_name:
                return None
        if label_name[0] == SUBLABEL_CHR:
                label_name = curr_line().parent_label + label_name
        for segment in _segment_list.values():
                for bank in segment.banks:
                        if label_name in bank.labels:
                                return bank.labels[label_name]
        return None

def is_reserved(name):
        name = name.upper()
        return name in _reserved_list or name in spec.instructions

def set_out(out):
        curr_segment().curr_bank().out = out

def add_line(line):
        curr_segment().curr_bank().lines.append(line)

def is_in_zeropage(label):
        return label in _segment_list['ZEROPAGE'].banks[0].labels

def set_charmap(charmap):
        global _chardict
        _chardict = {}
        for i, c in enumerate(charmap):
                if c in _chardict:
                        raise UserError('duplicate char "' + c + '"')
                _chardict[c] = i

def set_charbase(charbase):
        global _charbase
        _charbase = charbase

def map_chars(chars):
        try:
                return bytes(_charbase + _chardict[c] for c in chars)
        except KeyError as e:
                raise UserError('unable to map character %s (see .CHARMAP)' % (str(e)))
