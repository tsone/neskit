import codecs, collections, re, copy, os.path
from util import *
import state, log, parse, spec, eva, freeze

class If(Locked):
        def __init__(self, running, done, had_else):
                self.running = running   # are we running the level?
                self.done = done         # ?
                self.had_else = had_else # did level have ELSE already?
If._allowed = ('running', 'done', 'had_else')

_macro_construct_count = None # Global count for expanding "\@" in macros.

class Macro(Locked):
        def __init__(self, name, param_names):
                self.name = name
                self.lines = []
                self.param_names = param_names
                self.replaces = [(re.compile(r'(\W|^)%s(\W|$)' % (p)), r'\1\\%d\2' % (i + 1)) for i, p in enumerate(param_names)]

        def append(self, s):
                for r in self.replaces:
                        s = r[0].sub(r[1], s)
                self.lines.append(s)

        def construct_lines(self, params, parent_line):
                global _macro_construct_count
                if len(params) != len(self.param_names):
                        log.error("invalid number of parameters for macro (requires %d, got %d)" \
                                % (len(self.param_names), len(params)))
                result = []
                for s in self.lines:
                        s = s.replace(r'\@', str(_macro_construct_count))
                        # TODO: join for loops, or first loop param_names then remaining params
                        for i, p in enumerate(params):
                                s = s.replace('\\%d' % (i + 1), p)
                        # TODO: optimize this to [.. for . in .]
                        result.append(state.Line(parent_line.filename, parent_line.linenum, s))
                _macro_construct_count = _macro_construct_count + 1
                return result

        def construct_lines_repeat(self, parent_line, repeat_count):
                result = []
                for i in range(repeat_count):
                        for s in self.lines:
                                s = s.replace(r'\@', str(i))
                                result.append(state.Line(parent_line.filename, parent_line.linenum, s))
                return result
Macro._allowed = ('name', 'lines', 'param_names', 'replaces')

_iferror = None
_ifstack = None
_macros = None
_curr_macro = None
_curr_repeat = None
_curr_repeat_count = 0 # HACK: Uses _curr_macro for the repeat.
_defines = None
_include_paths = None
_included_files = None
_lines = None
_re_comment_remover = re.compile(r'(?:[^;"]|"(?:\\.|[^"])*")+')

def _eval_if(params):

        def _is_valid_split(sp):
                return len(sp) != 2 or len(sp[1]) <= 1 or sp[1][-1] != ')'

        if params[0].upper().startswith('.DEFINED('):
                sp = params[0].split('(')
                if _is_valid_split(sp):
                        raise UserError('invalid .DEFINED(<define>) syntax')
                return get_define(sp[1][:-1])
        elif params[0].upper().startswith('.NDEFINED('):
                sp = params[0].split('(')
                if _is_valid_split(sp):
                        raise UserError('invalid .NDEFINED(<define>) syntax')
                return not get_define(sp[1][:-1])
        else:
                return eva.luate(params[0])

def _bank(params):
        state.curr_segment().set_curr_bank_idx(eva.luate(params[0]))

def _charbase(params):
        state.set_charbase(eva.luate(params[0]))

def _charmap(params):
        charmap = codecs.decode(params[0].strip('"'), 'unicode_escape')
        state.set_charmap(charmap)

def _define(params):
        _check_set_define(*params[0].split())

def _else(params):
        if len(_ifstack) <= 1:
                raise UserError('should be preceded by .IF')
        if _ifstack[-1].had_else:
                raise UserError('incorrectly preceded by .ELSE')
        _ifstack[-1].had_else = True
        if _ifstack[-1].running:
                _ifstack[-1].running = False
        elif not _ifstack[-1].done:
                _ifstack[-1].running = _ifstack[-2].running
                _ifstack[-1].done = _ifstack[-2].running

def _elseif(params):
        if len(_ifstack) <= 1:
                raise UserError('should be preceded by .IF')
        if _ifstack[-1].had_else:
                raise UserError('incorrectly preceded by .ELSE')
        if _ifstack[-1].running:
                _ifstack[-1].running = False
        elif not _ifstack[-1].done and _eval_if(params): # NOTE: eval if running=0 AND done=0
                _ifstack[-1].running = _ifstack[-2].running
                _ifstack[-1].done = _ifstack[-2].running

def _endif(params):
        if len(_ifstack) <= 1:
                raise UserError('should be preceded by .IF')
        _ifstack.pop()

def _endmacro(params):
        global _curr_macro
        if not _is_recording_macro():
                raise UserError('.ENDMACRO encountered, but no .MACRO set')
        else:
                _macros[_curr_macro.name] = _curr_macro
                _curr_macro = None

# NOTE: Special case: param[0] contains current line.
def _endrepeat(params):
        global _curr_repeat, _curr_repeat_count
        if not _is_recording_repeat():
                raise UserError('.ENDREPEAT without preceding .REPEAT')
        else:
                new_lines =_curr_repeat.construct_lines_repeat(params[0], _curr_repeat_count)
                _curr_repeat_count = 0
                _curr_repeat = None
                return new_lines

def _if(params):
        run = _ifstack[-1].running and _eval_if(params) # NOTE: eval if running=1
        _ifstack.append(If(run, run, False))

def _ifdef(params):
        _if(['.DEFINED(' + params[0] + ')'])

def _ifndef(params):
        _if(['.NDEFINED(' + params[0] + ')'])

def _remove_comment(line):
        s = _re_comment_remover.findall(' '+line)
        return s[0].strip()

def _convert_to_lines(filename, source):
        result = [state.Line(filename, linenum + 1, _remove_comment(text)) for linenum, text in enumerate(source.splitlines())]
        return [line for line in result if len(line.text) >= 1] # remove empty lines

def open_file(filename, mode):
        for p in _include_paths:
                new_path = os.path.join(p, filename)
                try:
                        return open(new_path, mode)
                except IOError:
                        log.info("failed to open %s, trying next path (if any)" % (new_path))
        raise UserError("cannot open file %s" % (filename))

def _load_lines(filename):
        log.info("loading lines from file %s" % (filename))
        f = open_file(filename, "rt")
        source = f.read()
        f.close()
        return _convert_to_lines(filename, source)

def _include(params):
        includefile = params[0].strip('"')
        if not includefile in _included_files:
                _included_files.append(includefile)
                new_lines = _load_lines(includefile)
                _prepend_lines(new_lines)
        else:
                log.warning('file ' + includefile + ' already included, will not include again')

def _macro(params):
        global _curr_macro
        if _is_recording_macro():
                raise UserError(".MACRO inside .MACRO is not allowed")
        sp = re.split('\s+', params[0], 1)
        if len(sp) == 2:
                params.insert(1, sp[1])
        _curr_macro = Macro(sp[0].upper(), params[1:])

def _repeat(params):
        global _curr_repeat, _curr_repeat_count
        if _is_recording_repeat():
                raise UserError(".REPEAT inside .REPEAT is not allowed")
        _curr_repeat_count = eva.luate(params[0])
        _curr_repeat = Macro("__repeat_temp", [])

def _segment(params):
        name = params[0].strip('"')
        return state.set_curr_segment(name)

def _inesmap(params):
        return log.warning('.INESMAP is non-functional: use command line to specify mapper')

def _inesprg(params):
        return log.warning('.INESPRG is non-functional: use command line to specify number of PRG-ROM banks')

def _ineschr(params):
        return log.warning('.INESCHR is non-functional: use command line to specify number of CHR-ROM banks')

def _inesmir(params):
        return log.warning('.INESMIR is non-functional: use command line to specify mirroring')

C = collections.namedtuple('C', 'func, min_params, max_params, is_cond, is_rom')
_commands = {
'BANK'      : C(_bank,      1, 1,  0, 1),
'CHARBASE'  : C(_charbase,  1, 1,  0, 0),
'CHARMAP'   : C(_charmap,   1, 1,  0, 0),
'DEFINE'    : C(_define,    1, 1,  0, 0),
'ELSE'      : C(_else,      0, 0,  1, 0),
'ELSEIF'    : C(_elseif,    1, 1,  1, 0),
'ENDIF'     : C(_endif,     0, 0,  1, 0),
'ENDMACRO'  : C(_endmacro,  0, 0,  0, 0),
'ENDREPEAT' : C(_endrepeat, 0, 0,  0, 0),
'IF'        : C(_if,        1, 1,  1, 0),
'IFDEF'     : C(_ifdef,     1, 1,  1, 0),
'IFNDEF'    : C(_ifndef,    1, 1,  1, 0),
'INCLUDE'   : C(_include,   1, 1,  0, 0),
'MACRO'     : C(_macro,     1, -1, 0, 0),
'REPEAT'    : C(_repeat,    1, 1,  0, 0),
'SEGMENT'   : C(_segment,   1, 1,  0, 0),
# non-functional:
'INESMAP'   : C(_inesmap,   1, 1,  0, 0),
'INESPRG'   : C(_inesprg,   1, 1,  0, 0),
'INESCHR'   : C(_ineschr,   1, 1,  0, 0),
'INESMIR'   : C(_inesmir,   1, 1,  0, 0),
}

def _is_skipping():
        return not _ifstack[-1].running

# TODO: refactor, dupe: freeze, flow
def _check_cmd_params(cmd_name, cmd, params):
        if cmd.max_params > -1 and len(params) > cmd.max_params:
                raise UserError('too many parameters to .%s' % (cmd_name))
        if len(params) < cmd.min_params:
                raise UserError('too few parameters to .%s' % (cmd_name))
        if cmd.is_rom == 1 and not state.curr_segment().is_rom:
                raise UserError('segment %s doesnt allow .%s' % (state._segment_name, cmd_name))

def _process_control_cmd(line, cmd_name, params):
        try:
                cmd = _commands[cmd_name]
        except KeyError:
                if not _is_skipping():
                        return True
                else:
                        return False
        if _is_skipping() and cmd.is_cond == 0:
                return False
        _check_cmd_params(cmd_name, cmd, params)

        # .ENDREPEAT is special by having line as param.
        if cmd_name == "ENDREPEAT":
                params = [line]

        generated_lines = cmd.func(params)
        if generated_lines:
                _prepend_lines(generated_lines)
        return False

def _record_macro_line(s):
        _curr_macro.append(s)

def _is_recording_macro():
        return _curr_macro

def _record_repeat_line(s):
        _curr_repeat.append(s)

def _is_recording_repeat():
        return _curr_repeat

def _prepend_lines(new_lines):
        global _lines
        _lines = new_lines + _lines

def set_define(name, value):
        _defines[name] = value

def _check_set_define(name, value):
        if name != None:
                if state.is_reserved(name):
                        raise UserError('reserved "' + name + '"')
                elif name in _defines:
                        raise UserError('define "' + name + '" already exists')
                else:
                        #log.info("define added: '%s'='%s'" % (name, value))
                        set_define(name, value)
        else:
                raise UserError("define has no name (parse failed)")
        return None

def _control_command(line, s):
        cmd, params = parse.control_command(s)
        if cmd != None:
                if _is_recording_macro() and cmd != "ENDMACRO":
                        # TODO: recorded in two places, ok?
                        _record_macro_line(s)
                        line = None
                if _is_recording_repeat() and cmd != "ENDREPEAT":
                        # TODO: recorded in two places, ok?
                        _record_repeat_line(s)
                        line = None

                if line and _process_control_cmd(line, cmd, params):
                        return line
                else:
                        return None
        else:
                raise UserError("on parse control command")

def _parse_line(line):
        s = line.text
        if s[0] == CONTROL_CMD_CHR:
                return _control_command(line, s)

        if _is_recording_macro():
                # TODO: recorded in two places, ok?
                _record_macro_line(s)
                line = None
        if _is_recording_repeat():
                # TODO: recorded in two places, ok?
                _record_repeat_line(s)
                line = None

        if line and not _is_skipping():
                if s.count('=') == 1:
                        _check_set_define(*parse.define(s))
                        return None
                else:
                        # check for macro command and expand
                        cmd, params = parse.command(s)
                        # -> not instruction.. do we have a macro?
                        try:
                                new_lines = _macros[cmd].construct_lines(params, line)
                                _prepend_lines(new_lines)
                                return None
                        except KeyError:
                                return line
        else:
                # skipping due conditionals, skip
                return None

def _init(startfile, include_paths, mapper):
        global _iferror, _ifstack, _macros, _curr_macro, _defines
        global _curr_repeat, _curr_repeat_count
        global _include_paths, _included_files
        global _lines, _macro_construct_count

        _iferror = False
        _ifstack = [If(True, False, False)]
        _macros = {}
        _curr_macro = None
        _curr_repeat = None
        _curr_repeat_count = 0
        _defines = {}
        _include_paths = None
        _included_files = []
        _lines = []
        _macro_construct_count = 0
        _include_paths = include_paths

        state.set_curr_segment('CHR')
        set_define('__NUM_CHR_BANKS__', str(state.curr_segment().num_banks()))

        state.set_curr_segment('CODE')
        set_define('__NES__', '1')
        set_define('__NUM_BANKS__', str(state.curr_segment().num_banks()))
        set_define('__MAPPER__', str(mapper))

        _include([startfile])

def _check_eof():
        if _is_recording_repeat():
                raise UserError("missing .ENDREPEAT for .REPEAT")
        if _is_recording_macro():
                raise UserError("missing .ENDMACRO for .MACRO %s" % (_curr_macro.name))
        if not len(_ifstack) == 1:
                raise UserError("missing %d .ENDIF(s)" % (len(_ifstack) - 1))

def process(startfile, include_paths, mapper):
        _init(startfile, include_paths, mapper)
        while len(_lines) > 0:
                line = _lines.pop(0)
                state.set_curr_line(line)
                line = _parse_line(line)
                if line:
                        state.add_line(line)
        _check_eof()

def get_define(name):
        return _defines.get(name, None)

