__version__ = "0.9.1"
__copyright__ = 'Copyright (c) 2013 Valtteri Heikkila'
__license__ = """
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""
import sys, argparse, os.path
from util import *
import state, flow, freeze, encode, log

# iNES header parameters
BANKS_MIN = 1
BANKS_MAX = 8 # max 128 KB = 8 * 16 KB
CHR_BANKS_MIN = 0
CHR_BANKS_MAX = 8 # max 64 KB = 8 * 8 KB

_args = None
_disclaimer = """\
nkasm %s - neskit assembler
""" % (__version__)
_epilog = """\
copyright (c) 2013 valtteri heikkila

"""

def _parse_args():
        global _args
        parser = argparse.ArgumentParser(description=_disclaimer, epilog=_epilog, formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument("-v", "--verbose", help="verbose output messages", action="store_true")
        parser.add_argument("-s", "--strip-header", help="strip ines header from output", action="store_true")
        parser.add_argument("-d", "--debug", help="output debug symbol files (for fceux in .nl format)", action="store_true")
        parser.add_argument("-M", "--mapper", type=int, default=2, choices=[0,2,4,119], help=f"ines 1.0 mapper, options: 0,2,4,119 (default: 2)")
        parser.add_argument("-m", "--horizontal", help="set horizontal nametable mirroring (otherwise vertical)", action="store_true")
        parser.add_argument("-b", "--banks", type=int, default=BANKS_MIN, choices=list(range(BANKS_MIN, BANKS_MAX+1)), metavar="NUM", help=f"number of PRG-ROM 16 KB banks in range {BANKS_MIN}-{BANKS_MAX} (default: {BANKS_MIN})")
        parser.add_argument("-c", "--chr-banks", type=int, default=CHR_BANKS_MIN, choices=list(range(CHR_BANKS_MIN, CHR_BANKS_MAX+1)), metavar="NUM", help=f"number of CHR-ROM 8 KB banks in range {CHR_BANKS_MIN}-{CHR_BANKS_MAX} (0=CHR-RAM, default: {CHR_BANKS_MIN})")
        parser.add_argument("-I", "--include", action="append", default=[''], help="add include path")
        parser.add_argument("-o", "--outfile", default="out.nes", help="output rom file")
        parser.add_argument("-l", "--listing", help="output a combined listing of all generated code")
        parser.add_argument("-D", "--define", help="set define, ex. -DLIFF=42")
        parser.add_argument("infile", help="input assembler source code file (single)")
        _args = parser.parse_args()
        _args.include = [ os.path.normpath(i) for i in _args.include ]
        # TODO: implement missing params
        if _args.mapper == 4 and _args.horizontal:
                raise UserError("horizontal mirroring not working with mapper #4")
        if _args.define:
                log.info("not implemented " + str(_args.define))
        if _args.verbose:
                log.set_verbose()

def _build_ines_header():
        # iNES 1.0 header:
        # http://wiki.nesdev.com/w/index.php/INES
        mapper = _args.mapper
        mirroring = _args.horizontal
        return b"NES\x1A" + bytes([
                _args.banks,    # 4: PRG-ROM size (16 KB units).
                _args.chr_banks, # 5: 0 = CHR-RAM, otherwise CHR-ROM size (8 KB units).
                ((mapper << 4) & 0xF0) + mirroring, # 6: Mapper low nybble, mirroring etc.
                (mapper & 0xF0), # 7: Mapper high nybble etc.
                0,              # 8: PRG-RAM size (8 KB units).
                0,              # 9: TV system etc.
                0,              # 10: TV system etc.
                0, 0, 0, 0, 0]) # 11-15: Unused/zeroes.

def _write_outfile(output):
        f = open(_args.outfile, "wb")
        if not _args.strip_header:
                f.write(_build_ines_header())
        f.write(output)
        f.close()
        log.info("wrote rom file %s" % (_args.outfile))

def _format_listing_line(org_str, data_str, filepos_str, text, cycles_str=""):
        if org_str or data_str or filepos_str:
                return "%-78s ; %5s  %-11s  %12s %s\n" % (text, org_str, data_str, filepos_str, cycles_str)
        else:
                return text + "\n"

def _gen_listing_lines():
        for segment_name in ['ZEROPAGE', 'BSS', 'CODE', 'CHR']:
                for bank_idx, bank in enumerate(state._segment_list[segment_name].banks):
                        if segment_name == "CODE" or segment_name == "CHR":
                                yield "\n" \
                                        + _format_listing_line("", "", "", f".SEGMENT \"{segment_name}\"") \
                                        + _format_listing_line("", "", "", ".BANK %d" % (bank_idx)) \
                                        + "\n"
                        else:
                                yield "\n" \
                                        + _format_listing_line("", "", "", f".SEGMENT \"{segment_name}\"") \
                                        + "\n"
                        for line in sorted(bank.lines, key=lambda x: x.org):
                                data_str = " ".join(["%02X" % (c) for c in line.data[:4]]) if line.data else ""
                                cycles_str = "*" * line.op.max_cycles if line.op else ""
                                text = line.text
                                idx = text.find(':')
                                if idx == -1:
                                        text = "\t" + text
                                else:
                                        if idx + 1 < len(text) and (text[idx + 1] == '+' or text[idx + 1] == '-'):
                                                text = "\t" + text
                                text = text.expandtabs(8)

                                filepos = os.path.basename(line.filename) + ':' + str(line.linenum)
                                yield _format_listing_line("%05X" % (line.org), data_str, filepos, text, cycles_str)

def _write_listing():
        if not _args.listing:
                return
        f = open(_args.listing, "w")
        f.write("; %s %s\n; assembly: %s\n\n" % (sys.argv[0], __version__, _args.infile))
        defines = sorted(i for i in flow._defines.items() if not i[0].startswith("__"))
        f.writelines(_format_listing_line("", "", "", "%s = %s" % (i[0], i[1])) for i in defines)
        f.writelines(_gen_listing_lines())
        f.close()
        log.info("wrote listing %s" % (_args.listing))

def _write_debug_bank(filename, labels):
        merge = {}
        for key, value in labels.items():
                if value in merge and not merge[value].startswith('__label'):
                        continue
                merge[value] = key
        f = open(filename, "w")
        f.writelines("$%04X#%s#\n" % (org, name) for org, name in sorted(merge.items()))
        f.close()

def _write_debug():
        if not _args.debug:
                return
        for bank_idx, bank in enumerate(state._segment_list['CODE'].banks):
                _write_debug_bank(_args.outfile + (".%x.nl" % bank_idx), bank.labels)
        ram_labels = state._segment_list['ZEROPAGE'].banks[0].labels
        ram_labels.update(state._segment_list['BSS'].banks[0].labels)
        _write_debug_bank(_args.outfile + ".ram.nl", ram_labels)

def _gen_pads_sort_split(segment):
        for start, end in sorted(segment.pads):
                start_b, end_b = segment.pos2bank_idx(start), segment.pos2bank_idx(end)
                while start_b < end_b:
                        start_b = start_b + 1
                        new_start = start_b * segment.bank_size()
                        yield (start, new_start - 1)
                        start = new_start
                yield (start, end)

def _print_pads(segment_name, rom_name):
        segment = state._segment_list[segment_name]
        # split and sort pads, then store to array by bank index
        bank_pads = [[] for _ in range(segment.num_banks())]
        for p in _gen_pads_sort_split(segment):
                bank_idx = segment.pos2bank_idx(p[0])
                bank_pads[bank_idx].append((segment.pos2org(p[0]), segment.pos2org(p[1])))

        # count and log unused area
        rom_size = segment.bank_size() * segment.num_banks()
        total_unused = 0
        log.info(f"unused {rom_name}-ROM areas:")
        for bank_idx, pads in enumerate(bank_pads):
                strs = []
                bank_unused = 0
                for start, end in pads:
                        size = end - start + 1
                        strs.append("$%04X-$%04X(%d)" % (start, end, size))
                        bank_unused += size
                if bank_unused != segment.bank_size():
                        strs.append("unused: %d bytes" % (bank_unused))
                else:
                        strs = ["completely unused: %d bytes" % (bank_unused)]
                log.info("bank %d: %s" % (bank_idx, ', '. join(strs)))
                total_unused += bank_unused
        log.info(f"total {rom_name}-ROM unused: {total_unused} bytes ({rom_size - total_unused} used)")

def _print_stats():
        for bank in state._segment_list['CODE'].banks:
                for stat in bank.stats:
                        code_bytes = 0
                        data_bytes = 0
                        min_cycles = 0
                        max_cycles = 0
                        lines = [ line for line in bank.lines if line.org >= stat.start and line.org < stat.end ]
                        for line in lines:
                                if line.op:
                                        code_bytes += line.op.size_bytes
                                        min_cycles += line.op.min_cycles
                                        max_cycles += line.op.max_cycles
                                elif line.data:
                                        data_bytes += len(line.data)

                        s = "stat:%s: " % (stat.name)
                        if data_bytes == 0:
                                s += "cycles: %d-%d code_bytes: %d" \
                                        % (min_cycles, max_cycles, code_bytes)
                        elif code_bytes == 0:
                                s += "data_bytes: %d" % (data_bytes)
                        else:
                                s += "cycles: %d-%d total_bytes: %d code_bytes: %d data_bytes: %d" \
                                        % (min_cycles, max_cycles, code_bytes+data_bytes, code_bytes, data_bytes)
                        log.info(s)

def _init():
        state.init(_args.banks, _args.chr_banks)

def _doit():
        _parse_args()
        if _args.infile:
                _init()
                flow.process(_args.infile, _args.include, _args.mapper)
                freeze.process()
                output = b''.join(encode.process('CODE')) # evaluate generator
                if _args.chr_banks > 0:
                        output += b''.join(encode.process('CHR'))
                state.set_curr_line(None)
                _write_outfile(output)
                _write_listing()
                _write_debug()
                _print_pads('CODE', 'PRG')
                if _args.chr_banks > 0:
                        _print_pads('CHR', 'CHR')
                log.set_verbose() # note!
                _print_stats()
                return 0
        else:
                raise UserError("no input file")

def run():
        try:
                _doit()
        except UserError as e:
                log.error(e)

if __name__ == '__main__':
        run()
