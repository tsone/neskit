import re, collections
from util import *
import state, log, flow, parse

# regex pattern to evaluate defines. explanation:
# - previous char must NOT be a letter, underscore or digit
# - current char must be a letter or underscore (not digit)
# - following chars must be letters, underscores or digits
_define_patt = r'(?<!\w)[\w^\d]\w*'
_re_define = re.compile(_define_patt)

# regexp pattern for tokenizer. explanation:
# - input for regex must have whitespace sequences replaced with a single space char
#   - ex. a sequence of two '\t's is converted to one ' '
# - operation evaluation comes first in the regex because
#   % is shared by modulo operation and binary literal
#   - operation (modulo) evaluation checks if previous
#     is literal, and if so, we have a modulo
#   - otherwise we have binary literal
# - the regex outputs each token as four-value group/tuple
# - explanation of the four values:
#   [0]: operation symbol (if token is operation)
#   [1]: parenthesis symbol (if token is parenthesis)
#   [2]: literal symbol (if token is literal)
#   [3]: parse remainder (if error was encountered)
# - only one of the four values is set at given time
#   - thus, to check if token is operation:
#     if token[0]: do_stuff_token_is_op()
#   - the same goes for parenthesis and literals
_tokenize_ops = r'\+|-|\*|/|(?<=\w |.\w)%|<<|>>|<=|>=|<|>|==|!=|~|!|&&|&|\^|\|\||\|'
_tokenize_parenthesis = r'\(|\)'
_tokenize_literals = r'(?:\$|%|' + SUBLABEL_CHR + ')?[\w]+'
_tokenize_patt = r'(%s)|(%s)|(%s)|(\s|.+)' % (_tokenize_ops, _tokenize_parenthesis, _tokenize_literals)
_re_tokenize = re.compile(_tokenize_patt)

# truncate float to integer
def _int(value):
        if isinstance(value, float):
                return int(value)
        elif not isinstance(value, int):
                raise UserError(f"encountered invalid type:{type(value)}")
        return value

# main operators from c language:
# http://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#Operator_precedence
# (note: precedence is negated for our use)
# additional low byte (<) and high byte (>) operators from:
# http://www.cc65.org/doc/ca65-5.html
O = collections.namedtuple('O', 'precedence, num_params, func')
_ops = {
'~'  : O(-3, 1, lambda p: ~_int(p[0])),
'!'  : O(-3, 1, lambda p: int(not p[0])),
'_'  : O(-3, 1, lambda p: -p[0]), # unary negation '-'
'{'  : O(-3, 1, lambda p: (_int(p[0]) & 0x00FF)), # extract low byte '<'
'}'  : O(-3, 1, lambda p: (_int(p[0]) & 0xFF00) >> 8), # extract high byte '>'
'*'  : O(-5, 2, lambda p: p[1] * p[0]),
'/'  : O(-5, 2, lambda p: p[1] / p[0]),
'%'  : O(-5, 2, lambda p: p[1] % p[0]),
'+'  : O(-6, 2, lambda p: p[1] + p[0]),
'-'  : O(-6, 2, lambda p: p[1] - p[0]),
'<<' : O(-7, 2, lambda p: _int(p[1]) << _int(p[0])),
'>>' : O(-7, 2, lambda p: _int(p[1]) >> _int(p[0])),
'<'  : O(-8, 2, lambda p: p[1] < p[0]),
'<=' : O(-8, 2, lambda p: p[1] <= p[0]),
'>'  : O(-8, 2, lambda p: p[1] > p[0]),
'>=' : O(-8, 2, lambda p: p[1] >= p[0]),
'==' : O(-9, 2, lambda p: p[1] == p[0]),
'!=' : O(-9, 2, lambda p: p[1] != p[0]),
'&'  : O(-10, 2, lambda p: _int(p[1]) & _int(p[0])),
'^'  : O(-11, 2, lambda p: _int(p[1]) ^ _int(p[0])),
'|'  : O(-12, 2, lambda p: _int(p[1]) | _int(p[0])),
'&&' : O(-13, 2, lambda p: p[1] and p[0]),
'||' : O(-14, 2, lambda p: p[1] or p[0]),
}

def _eval_defines(s):
        def sub(match, parent_subs):
                name = match.group(0)
                value = flow.get_define(name)
                if value:
                        if name in parent_subs:
                                # evaluation is cyclic
                                raise UserError("cyclic depedency for define '%s'" % (name))
                        else:
                                return _re_define.sub( \
                                        curry(sub, parent_subs = parent_subs + [name]), \
                                        "(" + value + ")") # parentheses required 
                else:
                        # not define, possibly label
                        return name
        return _re_define.sub(curry(sub, parent_subs = []), s)

def _tokenize_filter(tokens):
        # handle unary -
        lvalue = False
        for t in tokens:
                if t[3]: continue
                if not lvalue:
                        if t[0] == '-':
                                # convert to unary negation operation
                                t = ('_', t[1], t[2])
                        elif t[0] == '+':
                                # filter out possible '+' prefix
                                continue
                        elif t[0] == '*':
                                # convert to line org and store as literal
                                t = (None, t[1], str(state.curr_line().org))
                        elif t[0] == '<':
                                # convert to low byte operation
                                t = ('{', t[1], t[2])
                        elif t[0] == '>':
                                # convert to high byte operation
                                t = ('}', t[1], t[2])
                lvalue = t[2] or t[1] == ')'
                yield t

def _tokenize(input):
        input = ' '.join(input.split())
        tokens = _re_tokenize.findall(input)
        if tokens[-1][-1] and tokens[-1][-1] != ' ':
                raise UserError("cannot parse '%s' or '%s'" % (tokens[-1][-1], input))
        else:
                return _tokenize_filter(tokens)

# shunting-yard algorithm:
# http://en.wikipedia.org/wiki/Shunting-yard_algorithm
def _infix_to_postfix(tokens):
        output = []
        op_stack = []
        for t in tokens:
                if t[1] == '(':
                        op_stack.append(t)
                elif t[1] == ')':
                        while True:
                                t = op_stack.pop()
                                if t[1] == '(':
                                        break
                                # TODO: test mismatching parentheses
                                output.append(t)
                elif t[0]:
                        o1 = _ops[t[0]]
                        while len(op_stack) and op_stack[-1][0]:
                                o2 = _ops[op_stack[-1][0]]
                                if (o1.precedence < o2.precedence) or (o1.num_params == 2 and o2.precedence == o1.precedence):
                                        output.append(op_stack.pop())
                                else:
                                        break
                        op_stack.append(t)
                else:
                        output.append(t)
        while len(op_stack):
                t = op_stack.pop()
                if t[1] == '(':
                        raise UserError("mismatching parentheses")
                output.append(t)
        
        return output

def _eval_literal(token):
        s = token[2]
        if parse.is_number(s):
                return parse.number_no_clamp(s)
        elif parse.is_label(s):
                org = state.get_label(s)
                if org == None:
                        raise UserError("no label with name '%s' found" % (s))
                else:
                        return org
        else:
                raise UserError("invalid literal '%s'" % (s))

# evaluate postfix:
# http://en.wikipedia.org/wiki/Reverse_Polish_notation#Postfix_algorithm
def _eval_postfix(tokens):
        stack = []
        for t in tokens:
                if t[0]:
                        op = _ops[t[0]]
                        if len(stack) < op.num_params:
                                raise UserError("operand '%s' requires %d parameters, %d given" % (t[0], op.num_params, len(stack)))
                        params = [stack.pop() for i in range(op.num_params)]
                        value = op.func(params)
                else:
                        value = _eval_literal(t)
                stack.append(value)
        if len(stack) != 1:
                raise UserError("too many literals")
        else:
                return stack[0] # result is the top of stack

def luate(input):
        return luate_tokens(tokenize(input))

def tokenize(input):
        input = _eval_defines(input)
        if input:
                return _infix_to_postfix(_tokenize(input))
        else:
                return None
        

def luate_tokens(tokens):
        if tokens:
                return _int(_eval_postfix(tokens))
        else:
                return None

