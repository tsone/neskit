import re
import log
from util import *

_re_is_alnum = re.compile(r'\W')
_re_is_label = re.compile(r'[\w\.^\d][\w\.]*')
_re_instruction_type = re.compile("\
(?P<MY>\(\s*([^\)]+)\s*\)\s*,\s*(y|Y))\
|(?P<MX>\(\s*([^,]+)\s*,\s*(x|X)\s*\))\
|(?P<M>\(\s*([^\)]+)\s*\))\
|(?P<I>#(.+))\
|(?P<DY>([^,]+)\s*,\s*(y|Y))\
|(?P<DX>([^,]+)\s*,\s*(x|X))\
|(?P<D>([^Aa][^,]*|[^,]{2,}))\
|(?P<A>[Aa])\
|(?P<N>\s*)\
")
_re_comma_splitter = re.compile(r'(?:[^,"]|"(?:\\.|[^"])*")+')

def is_alnum(s):
        return not _re_is_alnum.search(s)

def is_number(s):
        return s[0].isdigit() or s[0] == '$' or s[0] == '%'

def is_label(s):
        return _re_is_label.search(s)

def number_no_clamp(s):
        try:
                if s[0] == '$':
                        return int(s[1:], 16)
                elif s.startswith('0x'):
                        return int(s[2:], 16)
                elif s[0] == '%':
                        if len(s) != 1+8:
                                log.warning("binary number %s is not exactly 8 bits long" % (s))
                        return int(s[1:], 2)
                else:
                        return int(s)
        except:
                return None

def number(s, minimum, maximum):
        result = number_no_clamp(s)
        if result:
                if result > maximum:
                        log.warning('overflow, clamped to maximum ' + str(maximum))
                        result = maximum
                elif result < minimum:
                        log.warning('underflow, clamped to minimum ' + str(minimum))
                        result = minimum
        return result

def command(s):
        sp = re.split('\s+', s, 1)
        name = sp[0].strip() # TODO: is strip necessary?
        if not is_alnum(name):
                return None, None
        try:
                params = [p.strip() for p in _re_comma_splitter.findall(sp[1])]
        except:
                params = []
        return name.upper(), params

def define(s):
        sp = s.split('=', 1)
        return sp[0].rstrip(), sp[1].lstrip()

def instruction_params(p):
        p = ','.join(p)
        match = _re_instruction_type.match(p)
        fmt = match.lastgroup
        if match.end(fmt) != len(p):
                # length of matched instruction is incorrect
                raise UserError("syntax error on instruction")
        value = [m for m in match.groups() if m]
        if len(value) >= 2:
                return fmt, value[1]
        else:
                return fmt, None

def label(s, parent):
        if s[0] == SUBLABEL_CHR:
                if not is_alnum(s[1:-1]):
                        raise UserError('sublabel "%s" is not alphanumeric' % (s[1:-1]))
                result = parent + s[:-1]
        else:
                if not is_alnum(s[:-1]):
                        raise UserError('label "%s" is not alphanumeric' % (s[:-1]))
                result = s[:-1]
                parent = result
        return result, parent

def control_command(s):
        name, params = command(s[1:])
        if not name:
                return None, None
        return name, params
