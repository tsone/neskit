# classes
class Locked:
        def __setattr__(self, name, value):
                if not name in self._allowed:
                        raise AttributeError("attribute '%s' is not allowed" % (name))
                self.__dict__[name] = value

class Meta:
        def __init__(self, **vargs):
                self.__dict__ = vargs

class UserError(Exception):
        pass

# functions
def curry(f, *a, **kw):
        def curried(*more_a, **more_kw):
                return f(*(a+more_a), **dict(kw, **more_kw))
        return curried

# constants
ZEROPAGE = Meta(start=0x0000, end=0x00FF)
BSS      = Meta(start=0x0200, end=0x07FF)
BANKED   = Meta(start=0x8000, end=0xBFFF) # PRG-ROM low area (switchable)
FIXED    = Meta(start=0xC000, end=0xFFFF) # PRG-ROM high area (non-switchable)
CHR      = Meta(start=0x0000, end=0x1FFF) # CHR-ROM physical area (switchable)
CONTROL_CMD_CHR = '.'
SUBLABEL_CHR = '@'
PADDING_BYTE = b'\xFF'
