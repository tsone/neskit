import logging
import state

logging.basicConfig(format="%(levelname)s:%(message)s")
_logger = logging.getLogger("nkasm")
_exit_on_error = True

def _log(level, message):
        line = state.curr_line()
        try:
                _logger.log(level, "%s:%d:%s:%s" % \
                        (line.filename, line.linenum, line.text, message))
        except:
                _logger.log(level, message)

def info(msg):
        _log(logging.INFO, msg)

def warning(msg):
        _log(logging.WARNING, msg)

# TODO: change errors to exceptions? (logging to a file should be still possible)
def error(msg):
        _log(logging.ERROR, msg)
        if _exit_on_error:
                exit(1)
        return -1
        
def set_verbose():
        _logger.setLevel(logging.INFO)
