
.segment "zeropage"
foo:    .RES    1

.segment "bss"
bar:    .RES    2

.segment "code"
    LDA     foo
    STA     bar
    LDA     #$20
    STA     bar+1