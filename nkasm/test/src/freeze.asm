; comment 1
.BANK 1 ; comment 2
        .org $d000
        .include "src/simple.asm"
.byte 128, 15
ror $1111
.bank 0
.org $8010
ror $2222
.INCLUDE "src/freeze.asm" ; test for infinite recursion of includes
.incbin "src/test.bin"
   .byte $AB, $CD, $EF
ror a
.align 32
.word 12, 13, 14
