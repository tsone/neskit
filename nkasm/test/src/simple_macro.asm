
.MACRO  NEG
        EOR     #$FF
        CLC
        ADC     #1
.ENDMACRO

.MACRO  ABS
        BPL     @skip\@
        NEG
@skip\@:
.ENDMACRO

base:
        LDA     #1
        NEG
        ABS
        SBC     #3
        ABS

