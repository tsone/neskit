
.segment "CODE"

MAGIC_IDX_BASE = $0300

; 0. Dummy macro invocations to increase the invocation counter.
.MACRO DUMMY_INVOCATION
.ENDMACRO
DUMMY_INVOCATION
DUMMY_INVOCATION
DUMMY_INVOCATION

; 1. Macro with macro invocation counter evaluated inside repeat.
.macro MAGIC_IDX_LO value
.byte   <((value) + \@)
.endmacro
magic_idx_lo:
.REPEAT 8
MAGIC_IDX_LO MAGIC_IDX_BASE + 32*(1+\@)
.ENDREPEAT

; 2. Macro with invocation counter, defined and evaluated inside repeat.
magic_idx_hi:
.REPEAT 8
.macro MAGIC_IDX_HI value
.byte   >((value) + \@)
.endmacro
MAGIC_IDX_HI MAGIC_IDX_BASE + 32*(1+\@)
.ENDREPEAT
