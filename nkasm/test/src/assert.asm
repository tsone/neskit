
FOO = 1

.ASSERT FOO == 1, "assert 1"
.ASSERT FOO + FOO == 2, "assert 2"

; no assert message
.assert * == $c000

.IF 0
.ASSERT 0, "assert 4"
.ENDIF