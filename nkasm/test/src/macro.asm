#SWAP_BYTES = 1
TEST_IF = 1

.segment "ZEROPAGE"
temp:    .res 2

.segment "CODE"
.if TEST_IF

.macro load_word addr
.ifdef SWAP_BYTES
        LDA     addr+1
        STA     temp+0
        LDA     addr+0
        STA     temp+1
.else
        LDA     addr+0
        STA     temp+0
        LDA     addr+1
        STA     temp+1
.endif
.endmacro

.else

.macro load_word addr
        NOP
.endmacro

.endif
        load_word data
        JMP     (temp)
        
data:
        .word $ACDC
