#!/usr/bin/env python3
import argparse, glob, filecmp, os, subprocess

_args = None

# key: { testname: ( type, prg_banks ) }
_tests = {
    "assert":       ( "noref",  1 ),
    "assert_err":   ( "error",  1 ),
    "bank_err":     ( "error",  1 ),
    "constants":    ( "normal", 1 ),
    "dot_err":      ( "error",  1 ),
    "empty":        ( "normal", 1 ),
    "freeze":       ( "normal", 2 ),
    "if_err":       ( "error",  1 ),
    "if":           ( "normal", 1 ),
    "label":        ( "normal", 1 ),
    "macro":        ( "normal", 1 ),
    "repeat":       ( "normal", 1 ),
    "segment":      ( "normal", 1 ),
    "simple_macro": ( "normal", 1 ),
    "simple":       ( "normal", 1 ),
    "stat":         ( "normal", 2 ),
}

def run_test(testname):
    print(f"running test: {testname}")
    infile = os.path.join("src", testname + ".asm")
    outfile = os.path.join("generated", testname + ".nes")
    lstfile = os.path.join("generated", testname + ".lst")
    reffile = os.path.join("ref", testname + ".nes")

    test_type = _tests[testname][0]
    prg_banks = _tests[testname][1]

    # build command and assemble
    cmd = ["python3", "../../nkasm", "-v", "-b", str(prg_banks), "-o", outfile, infile]
    if _args.lst:
        cmd += ["-l", lstfile]
    ret = subprocess.run(cmd, capture_output=not _args.verbose)

    # verify retcode
    assert ret.returncode == (test_type == "error")

    # verify reference (compare output file)
    if test_type == "normal" and not _args.no_verify:
        assert filecmp.cmp(outfile, reffile, shallow=False)

def main():
    global _args

    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    parser = argparse.ArgumentParser(description="nkasm unit test runner", formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-v", "--verbose", help="verbose output messages", action="store_true")
    parser.add_argument("-n", "--no-verify", help="don't verify against reference", action="store_true")
    parser.add_argument("-l", "--lst", help="also generate .lst files", action="store_true")
    parser.add_argument("tests", help="tests to run (optional)", default=_tests.keys(), metavar="testname", nargs="*")
    _args = parser.parse_args()

    try:
        os.mkdir("generated")
    except:
        pass

    for test in _args.tests:
        run_test(test)

if __name__ == '__main__':
    main()
