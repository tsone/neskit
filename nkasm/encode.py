from util import *
import state, log, eva

# TODO: refactor, just weird
def _op_base(line):
        if line.tokens:
                data = eva.luate_tokens(line.tokens)
                if data != None:
                        return data
                else:
                        raise UserError("evaluating '%s'" % (line.value))
        else:
                return None
 
def op_branch(self):
        delta = _op_base(self) - (self.org + self.op.size_bytes)
        if delta > 127 or delta < -128:
                raise UserError("branch to delta %d is out of range" % (delta))
        return self.op.opcode + bytes([delta & 0xFF])

def op_byte(self):
        data = _op_base(self)
        # TODO: not sure if the warning is possible
        if data > 0xFF:
                log.warning("value 0x%X does not fit into a byte (8 bits)" % (data))
        return self.op.opcode + bytes([data & 0xFF])
        
def op_word(self):
        data = _op_base(self)
        # TODO: fix warning message in case of JMP and others
        if data <= 0xFF:
                log.warning("value 0x%X fits into a byte (8 bits)" % (data))
        elif data > 0xFFFF:
                log.warning("value 0x%X does not fit into a word (16 bits)" % (data))
        return self.op.opcode + bytes([data & 0xFF, (data >> 8) & 0xFF])
        
def op_plain(self):
        return self.op.opcode

def cc_byte(self):
        # TODO: add warning for byte overflow
        return bytes([d for d in (eva.luate(p) for p in self.tokens)])

def cc_str(self):
        return state.map_chars(self.tokens) # tokens piggybacked for raw data

def cc_strz(self):
        return cc_str(self) + bytes([0])

def cc_pstr(self):
        s = cc_str(self)
        return bytes([len(s)]) + s

def cc_word(self):
        # TODO: add warning for word overflow
        return b''.join(bytes([d & 0xFF, (d & 0xFF00) >> 8]) for d in (eva.luate(p) for p in self.tokens)) # tokens piggybacked for raw data

def cc_incbin(self):
        return self.tokens # tokens piggybacked for raw data

# shortcuts for spec module
D = op_branch
B = op_byte
W = op_word
N = op_plain

def _pad_count(pos, new_pos):
        if new_pos < pos:
                raise UserError("new position is less than previous...?")
        else:
                return new_pos - pos

def process(segment_name):
        # set segment so subsequent curr_segment() will point to that
        state.set_curr_segment(segment_name)

        pos = 0
        segment = state.curr_segment()
        segment.pads = [] # clear pads

        for i, bank in enumerate(segment.banks):
                for line in sorted(bank.out, key=lambda x: x.org):
                        state.set_curr_line(line)
                        new_pos = segment.org2pos(i, line.org)

                        pad_count = _pad_count(pos, new_pos)
                        if pad_count > 0:
                                segment.pads.append((pos, new_pos-1))
                                yield PADDING_BYTE * pad_count
                        
                        data = line.encode_func()
                        line.data = data
                        pos += pad_count + len(data)
                        yield data

        new_pos = segment.num_banks() * segment.bank_size()
        pad_count = _pad_count(pos, new_pos)
        if pad_count > 0:
                segment.pads.append((pos, new_pos-1))
                yield PADDING_BYTE * pad_count
